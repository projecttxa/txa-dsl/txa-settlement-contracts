// SPDX-License-Identifier: BSD-3-Clause
// Copyright © 2022 TXA PTE. LTD.
pragma solidity ^0.8.9;

import "./Interfaces/IdentityRegistryInterface.sol";
import "./Interfaces/CollateralInterface.sol";
import "./SettlementUtilities.sol";

/**
 * MerkleState stores all merkle roots for the state of all exchange signed trades.
 *
 * The contract allows SDP operators to periodically update the state by submitting
 * new merkle roots with new trades appended to them.
 *
 * Note: The very first trade Id submitted must be 1.
 */

contract MerkleState {
    address internal constant _za = address(0);
    /**
     * Data structure containing relevant information for a merkle root submission
     */
    struct RootSubmission {
        bytes32 merkleRoot;
        address submitter;
        uint256 lastTradeID;
    }

    /**
     * Contains important data about each individual root challenge
     * @param submitter address of challenge submitter
     * @param challengeSubmissionBlock block number of challenge submission. Determines reply period timeout.
     * @param unresolved challenge resolution state. "resolved" state is active low so uninstantiated profiles are default "resolved"
     */
    struct DisputeProfile {
        address submitter;
        uint256 challengeSubmissionBlock;
        bool unresolved;
    }

    struct ChallengeData {
        bytes32 challengeRoot;
        bytes challengeLeaf;
        SettlementUtilities.Signature challengeSig;
        bytes32 reportedHash;
    }

    enum RootState {
        awaitingRoot,
        awaitingChallengeOrTimeout,
        processingChallenge,
        challengeSuccess
    }

    /**
     * Instance of IdentityRegistry interface.
     */
    IdentityRegistryInterface internal _identity;

    /**
     * Maps all trade merkle root updates to an incremental ID
     */
    mapping(uint256 => RootSubmission) public tradeRoots;

    /**
     * Incremental ID for all trade merkle roots
     * First ID is 1 to prevent underflow when checking last trade block
     * upon submission of first block
     */
    uint256 public currentRootID = 1;

    /**
     * Total number of active disputes on the current trade root
     * Incremented with every challenge that is initiated.
     * Only one is needed because a Root submission will never be moved on
     * from while this number is not equal to 0
     */
    uint256 public disputeCounter = 0;

    /**
     * Dispute profiles stored for every dispute against a trade merkle root.
     * The challenger's trade root is mapped to the profile
     */
    mapping(bytes32 => DisputeProfile) public disputeProfiles;

    /**
     * State indicating whether the contract is awaiting a new merkle root submission, awaiting challenge/timeout
     * or processing a challenge.
     */
    RootState public submissionState = RootState.awaitingRoot;

    /**
     * The block number of the most recent trade root submission, this is used for the
     * challenge period timer
     */
    uint256 public rootSubmissionBlock = 0;

    /**
     * The time period measured by block number wherein a new merkle root can be challenged
     */
    uint256 public rootApprovalTime;

    constructor(address identity) {
        _identity = IdentityRegistryInterface(identity);
        rootApprovalTime = 50;
    }

    /**
     * Called by an SDP Operators to submit an update to the trade merkle root state
     *
     * Operators are required to submit the last trade in the tree along with a merkle proof
     * for that leaf
     *
     * The Merkle Tree is an ordered tree that is ordered by trade ID so that all trades will have
     * an index in the tree according to their trade ID
     *
     * The ID of the last trade under a trade merkle root will be saved along with the root itself
     *
     * The last trade ID is submitted with the settlement acknowledgement (ack) from the exchange.
     * This ID will have its tree index and the ack will have its merkle proof checked.
     *
     * The ID of the last trade must be greater than the the last trade
     * of the previous root submission by the value
     * of its tree index to preserve index to tree index matching across all trade merkle trees
     *
     * All trades in tree must be signed by the exchange's trade signing key to be considered valid.
     * Exchange signature on submitted ack is checked on trade root submission
     *
     * @param tradeRoot The new merkle root of trades
     * @param leaves leaf hashes for every leaf under the trade merkle root
     * @param ack settlement acknowledgement
     * @param sig from the exchange of the settlement acknowledgement
     */
    function reportTradeRoot(
        bytes32 tradeRoot,
        bytes32[] calldata leaves,
        SettlementUtilities.ACK calldata ack,
        SettlementUtilities.Signature calldata sig
    ) public onlyOperator {
        require(ack.chainID == block.chainid, "INCORRECT_CHAIN_ID");
        require(submissionState == RootState.awaitingRoot, "TRADE_ROOT_ALREADY_SUBMITTED");
        bytes32 hashedLeaf = SettlementUtilities.hashACK(ack);

        tradeRoots[currentRootID].merkleRoot = tradeRoot;
        tradeRoots[currentRootID].submitter = msg.sender;
        tradeRoots[currentRootID].lastTradeID = ack.tradeID;
        require(
            _identity.isTradeSigner(SettlementUtilities.verifySig(hashedLeaf, sig)),
            "INVALID_EXCHANGE_SIGNATURE"
        );
        require(
            ack.tradeID - tradeRoots[currentRootID - 1].lastTradeID == leaves.length,
            "END_LEAF_INDEX_INVALID"
        );
        require(tradeRoot == SettlementUtilities.getOrderedRoot(leaves), "LEAVES_DO_NOT_HASH_TO_TRADE_ROOT");
        require(
            keccak256(abi.encodePacked(sig.v, sig.r, sig.s, hashedLeaf)) == leaves[leaves.length - 1],
            "ACK_DOES_NOT_MATCH_LEAF"
        );

        // TODO: Remove proof verification and just use the position of the ack in the
        // leaf array to verify its position.

        submissionState = RootState.awaitingChallengeOrTimeout;
        rootSubmissionBlock = block.number;

        CollateralInterface(_identity.getLatestCollateralCustody()).lockCollateral(
            msg.sender,
            _identity.getLatestProtocolToken(),
            _identity.protocolStakeAmount()
        );
    }

    function verifyTrade(
        uint256 tradeRootID,
        bytes32[] calldata merkleProof,
        bool[] calldata rightProofTrue,
        SettlementUtilities.Trade calldata trade,
        SettlementUtilities.Signature calldata sig
    ) public view {
        require(tradeRoots[tradeRootID].merkleRoot != 0, "TRADE_ROOT_DOES_NOT_EXIST");
        bytes32 hashedLeaf = keccak256(abi.encodePacked(trade.tradeID));
        verifyLeaf(
            tradeRoots[tradeRootID].merkleRoot,
            merkleProof,
            rightProofTrue,
            hashedLeaf,
            tradeRootID,
            trade.tradeID,
            sig
        );
    }

    function verifyACK(
        uint256 tradeRootID,
        bytes32[] calldata merkleProof,
        bool[] calldata rightProofTrue,
        SettlementUtilities.ACK calldata ack,
        SettlementUtilities.Signature calldata sig
    ) public view {
        require(tradeRoots[tradeRootID].merkleRoot != 0, "TRADE_ROOT_DOES_NOT_EXIST");
        bytes32 hashedLeaf = SettlementUtilities.hashACK(ack);
        verifyLeaf(
            tradeRoots[tradeRootID].merkleRoot,
            merkleProof,
            rightProofTrue,
            hashedLeaf,
            tradeRootID,
            ack.tradeID,
            sig
        );
    }

    /**
     * SDP Operator initiates a challenge to a trade root reported by another SDP operator using reportTradeRoot().
     * Challenger submits an alternate trade merkle root.
     *
     * Can be called by an SDP Operator after another operator has submitted a trade root using the reportTradeRoot()
     * function. Can only be called during the timed challenge submission period.
     *
     * Challenger must submit the signed ACK for themselves as well as the reporter.
     * Challenger must submit the merkle proof for both themselves and the reporter.
     *
     * @param challengeData  Signed acks from challener and reporter, and the challenger's merkle root
     * @param challengeProof Merkle proof of the challenger
     * @param hashingOrder Hashing order for the challenger's and reporter's proof. Must be the same for Both
     * @param reportedProof  Merkle proof of the reporter
     */

    function challengeTradeRoot(
        ChallengeData calldata challengeData,
        bytes32[] calldata challengeProof,
        bool[] calldata hashingOrder,
        bytes32[] calldata reportedProof
    ) public onlyOperator {
        require(
            submissionState == RootState.awaitingChallengeOrTimeout ||
                submissionState == RootState.processingChallenge,
            "CHALLENGE_PERIOD_NOT_OPEN"
        );
        require(block.number < rootSubmissionBlock + rootApprovalTime, "CHALLENGE_PERIOD_TIMED_OUT");
        require(
            disputeProfiles[challengeData.challengeRoot].submitter == _za,
            "DISPUTE_ROOT_ALREADY_HAS_ACTIVE_CHALLENGE"
        );
        require(
            challengeData.challengeRoot != tradeRoots[currentRootID].merkleRoot,
            "CHALLENGE_ROOT_DOES_NOT_DIFFER"
        );
        disputeProfiles[challengeData.challengeRoot].submitter = msg.sender;
        disputeProfiles[challengeData.challengeRoot].challengeSubmissionBlock = block.number;
        //TODO: determine the right stage to set this dispute profile to resolved state.
        disputeProfiles[challengeData.challengeRoot].unresolved = true;
        disputeCounter += 1;
        //TODO: Verify that ACKS are the same or find exchange at fault. New Function also Possible

        SettlementUtilities.Signature calldata sig = challengeData.challengeSig;
        (bytes32 hashedLeaf, uint256 tradeID) = SettlementUtilities.decodeAndHash(
            challengeData.challengeLeaf
        );
        require(
            keccak256(abi.encodePacked(sig.v, sig.r, sig.s, hashedLeaf)) != challengeData.reportedHash,
            "CHALLENGE_LEAF_DOES_NOT_DIFFER"
        );

        //check challenge
        verifyLeaf(
            challengeData.challengeRoot,
            challengeProof,
            hashingOrder,
            hashedLeaf,
            currentRootID,
            tradeID,
            sig
        );

        hashedLeaf = challengeData.reportedHash;
        (bool verify, uint256 leafIndex) = SettlementUtilities.verifyOrderedProof(
            reportedProof,
            hashingOrder,
            tradeRoots[currentRootID].merkleRoot,
            hashedLeaf
        );

        require(verify, "MERKLE_PROOF_INVALID");
        submissionState = RootState.processingChallenge;
    }

    function challengeSuccessTimeout(bytes32 challengeRoot) public onlyOperator {
        require(disputeProfiles[challengeRoot].unresolved == true, "DISPUTE_PROFILE_NOT_ACTIVE");
        require(
            block.number >= disputeProfiles[challengeRoot].challengeSubmissionBlock + rootApprovalTime,
            "REPLY_PERIOD_STILL_OPEN"
        );
        //TODO: handle closure of multiple challenges
        //TODO: ensure this dispute profile is set to resolved and dispute counter is decremented at the right time.
        submissionState = RootState.challengeSuccess;
    }

    /**
     * Checks the validity of a leaf in a submitted merkle tree
     *
     * It verifies the following:
     * -merkle proof
     * -correct positioning index in the ordered merkle tree
     * -exchange signature
     *
     * @param merkleRoot merkle root
     * @param merkleProof merkle Proof
     * @param rightProofTrue hashing order for the ordered proof
     * @param hashedLeaf hash of the merkle leaf contents prior to signature. Allows for different leaf types to be checked
     * @param tradeRootID trade root ID, this allows the last trade of the previous trade root to be checked to allow leaf index verif
     * @param tradeID trade ID of the leaf to allow leaf index verification
     * @param sig exchange signature on leaf hash.
     */
    function verifyLeaf(
        bytes32 merkleRoot,
        bytes32[] calldata merkleProof,
        bool[] calldata rightProofTrue,
        bytes32 hashedLeaf,
        uint256 tradeRootID,
        uint256 tradeID,
        SettlementUtilities.Signature calldata sig
    ) public view {
        require(
            _identity.isTradeSigner(SettlementUtilities.verifySig(hashedLeaf, sig)),
            "INVALID_EXCHANGE_SIGNATURE"
        );
        hashedLeaf = keccak256(abi.encodePacked(sig.v, sig.r, sig.s, hashedLeaf));

        bytes32 root = merkleRoot; // variable created to prevent "Stack too deep" error
        (bool verify, uint256 leafIndex) = SettlementUtilities.verifyOrderedProof(
            merkleProof,
            rightProofTrue,
            root,
            hashedLeaf
        );
        require(verify, "MERKLE_PROOF_INVALID");
        //Some failures of the following require block revert with an underflow error rather than the error
        //message "END_LEAF_INDEX_INVALID". This does not interfere with functionality.
        require(tradeID - tradeRoots[tradeRootID - 1].lastTradeID - 1 == leafIndex, "END_LEAF_INDEX_INVALID");
    }

    /**
     * Called by an SDP Operator to finalize a trade merkle root as accepted
     * after the challenge period is complete
     */
    function finalizeTradeRoot() public onlyOperator {
        require(submissionState == RootState.awaitingChallengeOrTimeout, "WRONG_ROOT_SUBMISSION_STATE");
        require(block.number >= rootSubmissionBlock + rootApprovalTime, "ROOT_APPROVAL_TIME_NOT_PASSED");
        submissionState = RootState.awaitingRoot;

        CollateralInterface(_identity.getLatestCollateralCustody()).unlockCollateral(
            tradeRoots[currentRootID].submitter,
            _identity.getLatestProtocolToken(),
            _identity.protocolStakeAmount()
        );
        currentRootID++;
    }

    modifier onlyOperator() {
        require(
            CollateralInterface(_identity.getLatestCollateralCustody()).adminFor(msg.sender) != address(0),
            "NOT_APPROVED_OPERATOR"
        );
        _;
    }
}
