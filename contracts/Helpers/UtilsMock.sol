// SPDX-License-Identifier: BSD-3-Clause
// Copyright © 2022 TXA PTE. LTD.
pragma solidity ^0.8.9;

import "../SettlementUtilities.sol";

contract UtilsMock {
    function verifySig(bytes32 message, SettlementUtilities.Signature calldata sig)
        public
        pure
        returns (address signer)
    {
        return SettlementUtilities.verifySig(message, sig);
    }

    function compareObligationAmounts(
        SettlementLib.Obligation[] memory correct,
        SettlementLib.Obligation[] memory reported
    ) public pure returns (SettlementUtilities.CollateralTransfer[] memory compensations) {
        return SettlementUtilities.compareObligationAmounts(correct, reported);
    }

    function generateTransferObligations(
        SettlementUtilities.CollateralTransfer[] memory senders,
        SettlementUtilities.CollateralTransfer[] memory recipients
    ) public pure returns (SettlementUtilities.TransferObligation[] memory slashObligations) {
        return SettlementUtilities.generateTransferObligations(senders, recipients);
    }

    function compareObligations(
        SettlementLib.Obligation[] memory reported,
        SettlementLib.Obligation[] memory existing
    ) public pure returns (bool) {
        return SettlementUtilities.compareObligations(reported, existing);
    }

    function hashACK(SettlementUtilities.ACK calldata ack) public pure returns (bytes32) {
        return SettlementUtilities.hashACK(ack);
    }
}
