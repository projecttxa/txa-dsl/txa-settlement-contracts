// SPDX-License-Identifier: BSD-3-Clause
// Copyright © 2022 TXA PTE. LTD.
pragma solidity ^0.8.9;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

contract DummyCoin is ERC20 {
    constructor(
        address[] memory airdrop,
        string memory name,
        string memory symbol
    ) ERC20(name, symbol) {
        _mint(msg.sender, 100000000000000000000);
        for (uint256 i = 0; i < airdrop.length; i++) {
            _mint(airdrop[i], 100000000000000000000);
        }
    }

    function mint(address recipient, uint256 amount) external {
        _mint(recipient, amount);
    }
}
