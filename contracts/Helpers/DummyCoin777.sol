// SPDX-License-Identifier: BSD-3-Clause
// Copyright © 2022 TXA PTE. LTD.
pragma solidity ^0.8.9;

import "@openzeppelin/contracts/token/ERC777/ERC777.sol";

contract DummyCoin777 is ERC777 {
    constructor(address[] memory airdrop, address[] memory operators) ERC777("DummyCoin", "DMC", operators) {
        _mint(msg.sender, 100000000000000000000, "", "");
        for (uint256 i = 0; i < airdrop.length; i++) {
            _mint(airdrop[i], 1000000000000000000, "", "");
        }
    }

    function mint(address recipient, uint256 amount) external {
        _mint(recipient, amount, "", "");
    }
}
