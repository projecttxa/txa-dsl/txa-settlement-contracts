// SPDX-License-Identifier: BSD-3-Clause
// Copyright © 2022 TXA PTE. LTD.
pragma solidity ^0.8.9;

/**
 * WalletRegistry allows a trader to associate a signature with a wallet address.
 * This allows the trader to authorize a single key for signing orders on participating exchanges
 * while accessing assets across many chains and wallets.
 * The signature must be verified off-chain by the SDPs and the participating exchanges.
 */
contract WalletRegistry {
    enum SignatureType {
        ECDSA
    }

    struct Signature {
        uint8 v;
        bytes32 r;
        bytes32 s;
        bytes publicKey;
        SignatureType signatureType;
        SignatureData sigData;
    }

    struct SignatureData {
        address wallet;
        uint256 chainId;
        bool signPermission;
    }

    /**
     * Maps trader addresses to signatures.
     */
    mapping(address => Signature) public signatures;

    /**
     * Emitted when a trader sets a signature.
     */
    event KeyApproved(
        address indexed trader,
        uint8 v,
        bytes32 r,
        bytes32 s,
        bytes publicKey,
        SignatureType signatureType,
        bool signPermission
    );

    /**
     * Emitted when a trader revokes a signature.
     */
    event KeyRevoked(address indexed trader, uint8 v, bytes32 r, bytes32 s, bytes publicKey);

    /**
     * Called by traders to connect a key with an address on this chain by associating
     * a signature with it.
     *
     * @param v                 v parameter in signature
     * @param r                 r parameter in signature
     * @param s                 s parameter in signature
     * @param publicKey         public key of signing address
     */
    function approveSignature(
        uint8 v,
        bytes32 r,
        bytes32 s,
        bool signPermission,
        bytes calldata publicKey
    ) external {
        require(
            signatures[msg.sender].v == 0 && signatures[msg.sender].r == 0 && signatures[msg.sender].s == 0,
            "SIGNATURE_ALREADY_SET"
        );
        require(v != 0 || r != 0 || s != 0, "INVALID_SIGNATURE");
        signatures[msg.sender] = Signature(
            v,
            r,
            s,
            publicKey,
            SignatureType.ECDSA,
            SignatureData(msg.sender, block.chainid, signPermission)
        );
        emit KeyApproved(msg.sender, v, r, s, publicKey, SignatureType.ECDSA, signPermission);
    }

    /**
     * Called by traders to remove a key associated with an address on this chain.
     */
    function revokeSignature() external {
        require(
            signatures[msg.sender].v != 0 || signatures[msg.sender].r != 0 || signatures[msg.sender].s != 0,
            "SIGNATURE_NOT_SET"
        );
        emit KeyRevoked(
            msg.sender,
            signatures[msg.sender].v,
            signatures[msg.sender].r,
            signatures[msg.sender].s,
            signatures[msg.sender].publicKey
        );
        delete signatures[msg.sender];
    }
}
