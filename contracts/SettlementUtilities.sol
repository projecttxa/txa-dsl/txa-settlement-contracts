// SPDX-License-Identifier: BSD-3-Clause
// Copyright © 2022 TXA PTE. LTD.

pragma solidity ^0.8.9;
import "./SettlementLib.sol";

library SettlementUtilities {
    struct CollateralTransfer {
        address party;
        uint256 amount;
    }

    /**
     * A single obligation for collateral transfer between a sender and a recipient
     */
    struct TransferObligation {
        address sender;
        address recipient;
        uint256 amount;
    }

    /**
     * Ethereum signature
     */
    struct Signature {
        uint8 v;
        bytes32 r;
        bytes32 s;
    }

    /**
     * Settlement ACK, will have other values filled out in a later ticket
     */
    struct ACK {
        uint256 tradeID;
        uint256 settlementID;
        uint256 chainID;
        address tokenID;
        //address coordAddress; leave out for now
    }

    /**
     * Trade struct, currently only has Trade ID
     */
    struct Trade {
        uint256 tradeID;
    }

    /**
     * Compares two sets of obligations.
     */
    function compareObligations(
        SettlementLib.Obligation[] memory reported,
        SettlementLib.Obligation[] memory existing
    ) public pure returns (bool) {
        return keccak256(abi.encode(reported)) == keccak256(abi.encode(existing));
    }

    /**
     * A General Function for segmenting a set of slash obligations for a group
     * This function accounts for the fact that when a collateral pool is to be divided,
     * integer division may create rounding errors.
     * It is worth noting that these errors will always round down so that
     * it is not possible to end up accounting for more tokens than actually exist.
     *
     * This algorithm simply iterates through the collateral requirements of recipients and makes
     * obligations from the senders to fill these requirements, moving from sender to sender successively
     *
     * @param slashObligations struct aray of slash obligations (sender => recipient by amount)
     *
     * Variables:
     *
     * senderOut: the array of senders for the output obligations
     * recipientOut: the array of recipients for the output obligations
     * transferAmounts: array of amounts for the output obligations
     *
     * senderIndex: index value for which sender is currently being slashed in the iterative loop
     * outIndex: index for the output obligation arrays as it is being filled
     * ** recipient is indexed by for loop
     *
     * amountOwed: amount of tokens owed to the particular recipient being serviced in that cycle of the main for loop
     * senderAmount: Amount of tokens still available to be allocated by the sender who is currently having their tokens allocated
     */

    function generateTransferObligations(
        CollateralTransfer[] memory senders,
        CollateralTransfer[] memory recipients
    ) public pure returns (TransferObligation[] memory slashObligations) {
        uint256 senderIndex = 0;
        uint256 outIndex = 0;

        slashObligations = new TransferObligation[](recipients.length + senders.length);
        // senderOut = new address[](recipients.length + senders.length);
        // recipientOut = new address[](recipients.length + senders.length);
        // transferAmounts = new uint256[](recipients.length + senders.length);

        for (uint256 i = 0; i < recipients.length; i++) {
            uint256 amountOwed = recipients[i].amount;
            // Do not move on to next recipient until amount owed to
            // this recipient is accounted for in output arrays.
            // this value is accounted for within a rounding error
            // tollerance of slashObligations.length
            while (amountOwed > slashObligations.length) {
                uint256 senderAmount = senders[senderIndex].amount;
                // Determine if sender can cover the remaining amount owed
                if (senderAmount >= amountOwed) {
                    // Transfer amount is remaining amount owed to this recipient.
                    slashObligations[outIndex].amount = amountOwed;
                    senders[senderIndex].amount -= amountOwed;
                    amountOwed = 0;
                } else {
                    // Transfer amount is remaining amount available from sender.
                    slashObligations[outIndex].amount = senders[senderIndex].amount;
                    amountOwed -= senders[senderIndex].amount;
                    senders[senderIndex].amount = 0;
                }

                // Record obligation from sender to recipient
                slashObligations[outIndex].sender = senders[senderIndex].party;
                slashObligations[outIndex].recipient = recipients[i].party;

                // Move on to next sender if this one has been emptied.
                if (senders[senderIndex].amount == 0) {
                    senderIndex++;
                }

                // Increment index of obligations.
                outIndex++;
            }
        }

        return (slashObligations);
    }

    //-----------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------

    /**
     * Accepts two sets of obligations and generates an array of unique addresses and any deficit
     * of funds that would have resulted from the difference between the correct and reported
     * obligations for each address.
     *
     * First a unique list of addresses is compiled from the reporters and deliverers in the correct
     * and reported obligations.
     * The correct and reported obligations are looped through and any user in the deliv or recip field
     * of the obligation that is not already in the compensations array is added to it.
     *
     * After this any loss due to discrepancies between the correct and reported obligations for
     * each user is found and added to their entry in the compensations array.
     * The specifics of the summing algorithm that accomplishes this is explained above it.
     *
     * Compensations are returned at the end of the function
     *
     * @param correct The Obligations that are true and should have been written in reality
     * @param reported The incorrect obligations that were written in reality. mistakes will need to be compensated
     * @param compensations array of unique addresses accompanied by the total tokens they will be owed
     *                      due to the disparity between reported obligations and correct obligations
     */
    function compareObligationAmounts(
        SettlementLib.Obligation[] memory correct,
        SettlementLib.Obligation[] memory reported
    ) public pure returns (CollateralTransfer[] memory compensations) {
        compensations = new CollateralTransfer[]((reported.length + correct.length) * 2);
        bool exists;
        uint256 compIndex = 0;

        // Find unique deliverers and recipients in reported obligations
        for (uint256 ii = 0; ii < reported.length; ii++) {
            // loop through the array of reported obligations
            exists = false;
            for (uint256 jj = 0; jj < compIndex; jj++) {
                if (reported[ii].deliverer == compensations[jj].party) {
                    exists = true;
                    break;
                }
            }
            if (!exists) {
                compensations[compIndex].party = reported[ii].deliverer;
                compIndex++;
            }
            exists = false;
            for (uint256 jj = 0; jj < compIndex; jj++) {
                if (reported[ii].recipient == compensations[jj].party) {
                    exists = true;
                    break;
                }
            }
            if (!exists) {
                compensations[compIndex].party = reported[ii].recipient;
                compIndex++;
            }
        }
        // Find unique deliverers and recipients in correct obligations not found in reported obs
        for (uint256 ii = 0; ii < correct.length; ii++) {
            // find deliverers
            exists = false;
            for (uint256 jj = 0; jj < compIndex; jj++) {
                if (correct[ii].deliverer == compensations[jj].party) {
                    exists = true;
                    break;
                }
            }
            if (!exists) {
                compensations[compIndex].party = correct[ii].deliverer;
                compIndex++;
            }
            //find recipients
            exists = false;
            for (uint256 jj = 0; jj < compIndex; jj++) {
                if (correct[ii].recipient == compensations[jj].party) {
                    exists = true;
                    break;
                }
            }
            if (!exists) {
                compensations[compIndex].party = correct[ii].recipient;
                compIndex++;
            }
        }
        /**
         * Sum up total amount of tokens delivered and received by users in the correct obligations
         * vs the reported obligations.
         *
         * Each user will end up with a resulting sum showing the amount of tokens they need to be
         * given to compensate for a net loss. The value will be zero if the user is not effected
         * or ends up with a net gain.
         *
         * Symbolically according to this summing system:
         *
         * The user is owed tokens they delivered in reality and would have received in correct obligations
         * Positive: reported.deliverer, correct.recipient
         *
         * The user owes tokens they received in reality and would have delivered in correct obligations
         * Negative: reported.recipient, correct.deliverer
         *
         * The negative and positive values summed will cancel eachother out to give an actual
         * sum of the values owed to the user by the settlement system
         */

        // Start by adding up what user is owed before cancelling these values out
        for (uint256 ii = 0; ii < compIndex; ii++) {
            //for each member of compensations, search for all reported deliveries
            for (uint256 jj = 0; jj < reported.length; jj++) {
                if (compensations[ii].party == reported[jj].deliverer) {
                    compensations[ii].amount += reported[jj].amount;
                }
            }
            //for each member of compensations, search for all recipient entries in correct obligations
            for (uint256 jj = 0; jj < correct.length; jj++) {
                if (compensations[ii].party == correct[jj].recipient) {
                    compensations[ii].amount += correct[jj].amount;
                }
            }
        }

        // cancel out any amount shown to be owed to user with any correct deliveries or reported recievals
        for (uint256 ii = 0; ii < compIndex; ii++) {
            //for each member of compensations, search for all reported recievals
            for (uint256 jj = 0; jj < reported.length; jj++) {
                if (compensations[ii].party == reported[jj].recipient) {
                    if (compensations[ii].amount <= reported[jj].amount) {
                        compensations[ii].amount = 0;
                    } else {
                        compensations[ii].amount -= reported[jj].amount;
                    }
                }
            }
            //for each member of compensations, deliverer entries in correct obligations
            for (uint256 jj = 0; jj < correct.length; jj++) {
                if (compensations[ii].party == correct[jj].deliverer) {
                    if (compensations[ii].amount <= correct[jj].amount) {
                        compensations[ii].amount = 0;
                    } else {
                        compensations[ii].amount -= correct[jj].amount;
                    }
                }
            }
        }
        return compensations;
    }

    //-----------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------

    function getMerkleRoot(bytes32[] memory leaves) public pure returns (bytes32) {
        return recursiveMerkleTree(leaves, 0, leaves.length);
    }

    function recursiveMerkleTree(
        bytes32[] memory leaves,
        uint256 start,
        uint256 end
    ) internal pure returns (bytes32) {
        if (end - start > 2) {
            uint256 byTwo = 2;
            while (end - start - byTwo > byTwo) {
                byTwo = byTwo * 2;
            }
            //TODO: check to see if gas costs or stack depth can be reduced by getting rid of
            // these intermediate variables
            uint256 middle = start + byTwo;
            bytes32 node1 = recursiveMerkleTree(leaves, start, middle);
            bytes32 node2 = recursiveMerkleTree(leaves, middle, end);

            return _hashPair(node1, node2);
        } else if (end - start == 1) {
            return leaves[start];
        } else if (end - start == 2) {
            return _hashPair(leaves[start], leaves[end - 1]);
        } else {
            revert("Recursive State: indexes start = end. State should not be reached");
        }
    }

    //-----------------------------------------------------------------------------------------------------

    /**
     * The following two functions combine a set of nodes in an ordered tree that are all at the same level
     * to give the hash of the common parent node of all of them.
     * This Function is to be used only for sets of nodes that are a power of two.
     *
     * function will not work for empty leaves[] array, or array with only 1 element
     */

    function getOrderedNode(bytes32[] memory leaves) public pure returns (bytes32) {
        return recursiveOrderedTree(leaves, 0, leaves.length);
    }

    function recursiveOrderedTree(
        bytes32[] memory leaves,
        uint256 start,
        uint256 end
    ) internal pure returns (bytes32) {
        if (end - start > 2) {
            //TODO: check to see if gas costs or stack depth can be reduced by getting rid of
            // these intermediate variables
            uint256 middle = start + (end - start) / 2;
            bytes32 node1 = recursiveOrderedTree(leaves, start, middle);
            bytes32 node2 = recursiveOrderedTree(leaves, middle, end);

            return _hashPairNoSort(node1, node2);
        } else if (end - start == 1) {
            revert("NODES_NOT_EVEN_POWER_OF_TWO");
            // TODO: replace revert with a return statement returning a null layer hash
            // This would remove the need to add balancing leaves and may save gas.
        } else if (end - start == 2) {
            return _hashPairNoSort(leaves[start], leaves[end - 1]);
        } else {
            revert("Recursive State: indexes start = end. State should not be reached");
        }
    }

    /**
     * Balances a merkle tree in order to get a merkle root from the getOrderedNode function
     *
     * function will not work for empty leaves[] array, or array with only 1 element
     */

    function getOrderedRoot(bytes32[] memory leaves) public pure returns (bytes32) {
        uint256 powerOfTwo = 1;
        while (powerOfTwo < leaves.length) {
            powerOfTwo = powerOfTwo * 2;
        }
        bytes32[] memory inputs = new bytes32[](powerOfTwo);
        for (uint256 ii = 0; ii < leaves.length; ii++) {
            inputs[ii] = leaves[ii];
        }
        return getOrderedNode(inputs);
    }

    //-----------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------

    function hashACK(ACK calldata ack) public pure returns (bytes32) {
        return keccak256(abi.encodePacked(ack.tradeID, ack.settlementID, ack.chainID, ack.tokenID));
    }

    // Ensure that this function is always an exact copy of the calldata version
    function hashACKMemory(ACK memory ack) public pure returns (bytes32) {
        return keccak256(abi.encodePacked(ack.tradeID, ack.settlementID, ack.chainID, ack.tokenID));
    }

    //-----------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------

    function decodeACK(bytes calldata data) public pure returns (ACK memory) {
        ACK memory myACK = abi.decode(data, (ACK));
        return myACK;
    }

    function decodeTrade(bytes calldata data) public pure returns (Trade memory) {
        Trade memory myTrade = abi.decode(data, (Trade));
        return myTrade;
    }

    function decodeAndHash(bytes calldata data) public pure returns (bytes32,uint256) {
        if(data.length == 32){
            Trade memory myTrade = decodeTrade(data);
            // data is a Trade
            return (keccak256(data),myTrade.tradeID);
        } else if(data.length == 128){
            ACK memory myACK = decodeACK(data);
            return(hashACKMemory(myACK),myACK.tradeID);
        } else {
            revert("INCORRECT_LEAF_DATA_LENGTH");
        }
    }

    /**
     * Internal function for verifying an Ethereum signature where the message body is a 32 bit hash
     *
     * @param message           Message body of the signature
     * @param sig               Cryptographic signature
     */
    function verifySig(bytes32 message, SettlementUtilities.Signature calldata sig)
        public
        pure
        returns (address signer)
    {
        return ecrecover(message, sig.v, sig.r, sig.s);
    }

    /*
    The functions below are based on code from OpenZeppelin: 
    https://github.com/OpenZeppelin/openzeppelin-contracts/blob/6ab8d6a67e3281ab062bdbe4df32b95c6409ee6d/contracts/utils/cryptography/MerkleProof.sol#L200
    The code below is subject to the following license

    The MIT License (MIT)

    Copyright (c) 2016-2022 zOS Global Limited and contributors

    Permission is hereby granted, free of charge, to any person obtaining
    a copy of this software and associated documentation files (the
    "Software"), to deal in the Software without restriction, including
    without limitation the rights to use, copy, modify, merge, publish,
    distribute, sublicense, and/or sell copies of the Software, and to
    permit persons to whom the Software is furnished to do so, subject to
    the following conditions:

    The above Copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
    OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
    MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
    IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
    CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
    TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
    SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    */

    /**
     * This functions verifies a merkle proof for a leaf in a merkle tree with a specific order
     *
     * returns true if the proof is correct
     *
     * @param proof array of merkle proof leaves
     * @param rightProofTrue boolean array to indicate hashing order. Proof is on right if it is set to true
     * @param root merkle root
     * @param leaf leaf of tree with accompanying proof
     */
    function verifyOrderedProof(
        bytes32[] calldata proof,
        bool[] calldata rightProofTrue,
        bytes32 root,
        bytes32 leaf
    ) public pure returns (bool, uint256) {
        (bytes32 processedRoot, uint256 index) = processOrderedProof(proof, rightProofTrue, leaf);
        return (processedRoot == root, index);
    }

    /**
     * This functions processes a merkle proof for a leaf in a merkle tree with a specific order
     *
     * returns the merkle root calculated from the merkle proof
     *
     * @param proof array of merkle proof leaves
     * @param rightProofTrue boolean array to indicate hashing order. Proof is on right if it is set to true
     * @param leaf leaf of tree with accompanying proof
     *
     * @param computedHash merkle root calculated by function
     * @param leafIndex index of proven leaf in merkle tree determined by hashing order
     */
    function processOrderedProof(
        bytes32[] calldata proof,
        bool[] calldata rightProofTrue,
        bytes32 leaf
    ) public pure returns (bytes32 computedHash, uint256 leafIndex) {
        computedHash = leaf;
        leafIndex = 0;
        uint256 indexBit;

        for (uint256 i = 0; i < proof.length; i++) {
            indexBit = (rightProofTrue[i] ? 0 : 1);
            leafIndex += indexBit << i;
            computedHash = _hashOrderedPair(computedHash, proof[i], rightProofTrue[i]);
        }
        return (computedHash, leafIndex);
    }

    /**
     * Copy of the _hashPair function that uses a boolean to set the hashing order.
     * Used to calculate merkle proofs.
     *
     * @param rightProofTrue boolean to indicate hashing order. Proof is on right if it is set to true
     * @param node  the node hash to be combined with a new proof node
     * @param proof the proof hash to be proccessed as part of the proof
     */
    function _hashOrderedPair(
        bytes32 node,
        bytes32 proof,
        bool rightProofTrue
    ) private pure returns (bytes32) {
        return rightProofTrue ? _efficientHash(node, proof) : _efficientHash(proof, node);
    }

    /**
     * Copy of the _hashPair that does not alter the hashing order based on any criteria
     * This funciton is used for verifying that a set of nodes all at the same level combine to the
     * same parent node.
     *
     * @param a node hash a
     * @param b node hash b
     */
    function _hashPairNoSort(bytes32 a, bytes32 b) private pure returns (bytes32) {
        return _efficientHash(a, b);
    }

    function _hashPair(bytes32 a, bytes32 b) private pure returns (bytes32) {
        return a < b ? _efficientHash(a, b) : _efficientHash(b, a);
    }

    function _efficientHash(bytes32 a, bytes32 b) private pure returns (bytes32 value) {
        /// @solidity memory-safe-assembly
        assembly {
            mstore(0x00, a)
            mstore(0x20, b)
            value := keccak256(0x00, 0x40)
        }
    }
}
