// SPDX-License-Identifier: BSD-3-Clause
// Copyright © 2022 TXA PTE. LTD.
pragma solidity ^0.8.9;

contract ExchangeEIP712 {
    struct EIP712Domain {
        string name;
        string version;
        uint256 chainId;
        address verifyingContract;
        bytes32 salt;
    }

    enum OrderType {
        ORDER_TYPE_NONE,
        ORDER_TYPE_LMT,
        ORDER_TYPE_MKT,
        ORDER_TYPE_STOP,
        ORDER_TYPE_STOP_LMT,
        ORDER_TYPE_TRAILING_STOP,
        ORDER_TYPE_TRAILING_STOP_LMT,
        ORDER_TYPE_CONTINGENT,
        ORDER_TYPE_MULTI_CONTINGENT,
        ORDER_TYPE_OTO,
        ORDER_TYPE_OCO,
        ORDER_TYPE_OTOCO
    }

    enum Side {
        ORDER_SIDE_BUY,
        ORDER_SIDE_SELL
    }

    enum FillType {
        ORDER_FILL_TYPE_AON,
        ORDER_FILL_TYPE_FOK,
        ORDER_FILL_TYPE_NORM
    }

    enum NetworkType {
        ASSET_NETWORK_NONE,
        ASSET_NETWORK_ETH,
        ASSET_NETWORK_ETC,
        ASSET_NETWORK_RSK,
        ASSET_NETWORK_POL
    }

    struct AssetNetworkInput {
        //   ID of the Chain on which the Asset exists.
        //   資産があるチェーンのID。
        uint256 chainId;
        //   Address of the Escrow Account containing the Asset.
        //   資産が含まれるエスクロー口座のアドレス。
        address assetCustody;
        //   Address of beneficiary account for the Asset.
        //   資産の受取口座のアドレス。
        address beneficiaryAddress;
        //   Type of network to which Asset belongs.
        //   資産が属するネットワークの種類。
        NetworkType networkName;
    }

    struct SubmitMarketOrderRequestParamsInput {
        //   "Base Asset information for given Order."
        AssetNetworkInput baseNetwork;
        //   "Counter Asset information for given Order."
        AssetNetworkInput counterNetwork;
        //   "ID of the Product associated with Order"
        string productId;
        //   "Describes if Order is `ORDER_SIDE_BUY` or `ORDER_SIDE_SELL` side."
        Side side;
        //   "Selected fill method of Order."
        FillType fillType;
        //   "Quantity of Product requested in Order. Value must be a valid number greater than zero."
        string size;
    }

    struct SubmitLimitOrderRequestParamsInput {
        //   Base Asset information for given Order.
        //   指定した注文のベースアセット（基盤資産）情報。
        AssetNetworkInput baseNetwork;
        //   Counter Asset information for given Order.
        //   指定した注文のカウンターアセット（対抗資産）情報。
        AssetNetworkInput counterNetwork;
        //   "ID of the Product associated with Order"
        string productId;
        //   Describes if Order is `ORDER_SIDE_BUY` or `ORDER_SIDE_SELL` side.
        //   注文が`ORDER_SIDE_BUY`か`ORDER_SIDE_SELL`かを表す。
        Side side;
        //   "Selected fill method of Order."
        FillType fillType;
        //   Quantity of Product requested in Order. Value must be a valid number greater than zero.
        //   注文で求める商品の数量。値はゼロより大きい有効な数字でなければならない。
        string size;
        //   Price at which the Order was submitted. Value must be a valid number greater than zero.
        //   注文が出されたときの価格。値はゼロより大きい有効な数字でなければならない。
        string price;
    }

    struct UpdateOrderRequestParamsInput {
        string id;
        //   Quantity of Product requested in Order. Value must be a valid number greater than zero.
        //   注文で求める商品の数量。値はゼロより大きい有効な数字でなければならない。
        string size;
        //   Price at which the Order was submitted. Value must be a valid number greater than zero.
        //   注文が出されたときの価格。値はゼロより大きい有効な数字でなければならない。
        string price;
        OrderType orderType;
    }

    struct CancelOrderRequestParamsInput {
        string id;
    }

    bytes32 public constant salt = 0xf2d857f4a3edcb9b78b4d503bfe733db1e3f6cdc2b7971ee739626c97e86a558;
    string public constant domainName = "TXA";

    bytes32 private constant EIP712DOMAIN_TYPEHASH =
        keccak256(
            abi.encodePacked(
                "EIP712Domain(string name,string version,uint256 chainId,address verifyingContract,bytes32 salt)"
            )
        );
    bytes32 private constant IDENTITY_TYPEHASH =
        keccak256(
            abi.encodePacked(
                "Identity(string baseAssetEscrow,string baseAssetNetworkType,string baseAssetChainId,string counterAssetEscrow,string counterAssetNetworkType,string counterAssetChainId)"
            )
        );
    bytes32 private constant ORDER_TYPEHASH =
        keccak256(
            abi.encodePacked(
                "Order(Identity identity,string product,string side,string orderType,string fillType,string size,string price)",
                "Identity(string baseAssetEscrow,string baseAssetNetworkType,string baseAssetChainId,string counterAssetEscrow,string counterAssetNetworkType,string counterAssetChainId)"
            )
        );

    bytes32 public constant SubmitMarketOrderRequestParamsInput_TYPEHASH =
        keccak256(
            abi.encodePacked(
                "SubmitMarketOrderRequestParamsInput(AssetNetworkInput baseNetwork,AssetNetworkInput counterNetwork,string productId,uint256 side,uint256 fillType,string size)",
                "AssetNetworkInput(uint256 chainId,address assetCustody,address beneficiaryAddress,uint256 networkName)"
            )
        );
    bytes32 public constant SubmitLimitOrderRequestParamsInput_TYPEHASH =
        keccak256(
            abi.encodePacked(
                "SubmitLimitOrderRequestParamsInput(AssetNetworkInput baseNetwork,AssetNetworkInput counterNetwork,string productId,uint256 side,uint256 fillType,string size,string price)",
                "AssetNetworkInput(uint256 chainId,address assetCustody,address beneficiaryAddress,uint256 networkName)"
            )
        );
    bytes32 public constant UpdateOrderRequestParamsInput_TYPEHASH =
        keccak256(
            abi.encodePacked(
                "UpdateOrderRequestParamsInput(string id,string size,string price,uint256 orderType)"
            )
        );
    bytes32 public constant CancelOrderRequestParamsInput_TYPEHASH =
        keccak256(abi.encodePacked("CancelOrderRequestParamsInput(string id)"));
    bytes32 public constant AssetNetworkInput_TYPEHASH =
        keccak256(
            abi.encodePacked(
                "AssetNetworkInput(uint256 chainId,address assetCustody,address beneficiaryAddress,uint256 networkName)"
            )
        );

    bytes32 private DOMAIN_SEPARATOR;

    constructor(string memory version) {
        DOMAIN_SEPARATOR = hash(
            EIP712Domain({
                name: domainName,
                version: version,
                chainId: block.chainid,
                verifyingContract: address(this),
                salt: salt
            })
        );
    }

    function hash(EIP712Domain memory eip712Domain) internal view returns (bytes32) {
        return
            keccak256(
                abi.encode(
                    EIP712DOMAIN_TYPEHASH,
                    keccak256(bytes(eip712Domain.name)),
                    keccak256(bytes(eip712Domain.version)),
                    eip712Domain.chainId,
                    address(this),
                    eip712Domain.salt
                )
            );
    }

    function hashAssetNetworkInput(AssetNetworkInput memory assetNetworkInput) public pure returns (bytes32) {
        return
            keccak256(
                abi.encode(
                    AssetNetworkInput_TYPEHASH,
                    assetNetworkInput.chainId,
                    assetNetworkInput.assetCustody,
                    assetNetworkInput.beneficiaryAddress,
                    assetNetworkInput.networkName
                )
            );
    }

    function hashSubmitMarketOrderRequestParamsInput(
        SubmitMarketOrderRequestParamsInput memory submitMarketOrderRequestParamsInput
    ) public view returns (bytes32) {
        return
            keccak256(
                abi.encodePacked(
                    "\x19\x01",
                    DOMAIN_SEPARATOR,
                    keccak256(
                        abi.encode(
                            SubmitMarketOrderRequestParamsInput_TYPEHASH,
                            hashAssetNetworkInput(submitMarketOrderRequestParamsInput.baseNetwork),
                            hashAssetNetworkInput(submitMarketOrderRequestParamsInput.counterNetwork),
                            keccak256(bytes(submitMarketOrderRequestParamsInput.productId)),
                            submitMarketOrderRequestParamsInput.side,
                            submitMarketOrderRequestParamsInput.fillType,
                            keccak256(bytes(submitMarketOrderRequestParamsInput.size))
                        )
                    )
                )
            );
    }

    function hashSubmitLimitOrderRequestParamsInput(
        SubmitLimitOrderRequestParamsInput memory submitLimitOrderRequestParamsInput
    ) public view returns (bytes32) {
        return
            keccak256(
                abi.encodePacked(
                    "\x19\x01",
                    DOMAIN_SEPARATOR,
                    keccak256(
                        abi.encode(
                            SubmitLimitOrderRequestParamsInput_TYPEHASH,
                            hashAssetNetworkInput(submitLimitOrderRequestParamsInput.baseNetwork),
                            hashAssetNetworkInput(submitLimitOrderRequestParamsInput.counterNetwork),
                            keccak256(bytes(submitLimitOrderRequestParamsInput.productId)),
                            submitLimitOrderRequestParamsInput.side,
                            submitLimitOrderRequestParamsInput.fillType,
                            keccak256(bytes(submitLimitOrderRequestParamsInput.size)),
                            keccak256(bytes(submitLimitOrderRequestParamsInput.price))
                        )
                    )
                )
            );
    }

    function hashUpdateOrderRequestParamsInput(
        UpdateOrderRequestParamsInput memory updateOrderRequestParamsInput
    ) public view returns (bytes32) {
        return
            keccak256(
                abi.encodePacked(
                    "\x19\x01",
                    DOMAIN_SEPARATOR,
                    keccak256(
                        abi.encode(
                            UpdateOrderRequestParamsInput_TYPEHASH,
                            keccak256(bytes(updateOrderRequestParamsInput.id)),
                            keccak256(bytes(updateOrderRequestParamsInput.size)),
                            keccak256(bytes(updateOrderRequestParamsInput.price)),
                            updateOrderRequestParamsInput.orderType
                        )
                    )
                )
            );
    }

    function hashCancelOrderRequestParamsInput(
        CancelOrderRequestParamsInput memory cancelOrderRequestParamsInput
    ) public view returns (bytes32) {
        return
            keccak256(
                abi.encodePacked(
                    "\x19\x01",
                    DOMAIN_SEPARATOR,
                    keccak256(
                        abi.encode(
                            CancelOrderRequestParamsInput_TYPEHASH,
                            keccak256(bytes(cancelOrderRequestParamsInput.id))
                        )
                    )
                )
            );
    }

    function verifySubmitLimitOrderRequestParamsInput(
        address signer,
        SubmitLimitOrderRequestParamsInput memory input,
        uint8 v,
        bytes32 r,
        bytes32 s
    ) public view returns (bool) {
        return ecrecover(hashSubmitLimitOrderRequestParamsInput(input), v, r, s) == signer;
    }

    function verifySubmitMarketOrderRequestParamsInput(
        address signer,
        SubmitMarketOrderRequestParamsInput memory input,
        uint8 v,
        bytes32 r,
        bytes32 s
    ) public view returns (bool) {
        return ecrecover(hashSubmitMarketOrderRequestParamsInput(input), v, r, s) == signer;
    }

    function verifyUpdateOrderRequestParamsInput(
        address signer,
        UpdateOrderRequestParamsInput memory input,
        uint8 v,
        bytes32 r,
        bytes32 s
    ) public view returns (bool) {
        return ecrecover(hashUpdateOrderRequestParamsInput(input), v, r, s) == signer;
    }

    function verifyCancelOrderRequestParamsInput(
        address signer,
        CancelOrderRequestParamsInput memory input,
        uint8 v,
        bytes32 r,
        bytes32 s
    ) public view returns (bool) {
        return ecrecover(hashCancelOrderRequestParamsInput(input), v, r, s) == signer;
    }
}
