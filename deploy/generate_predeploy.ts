// Copyright © 2022 TXA PTE. LTD.
import { HardhatRuntimeEnvironment, Libraries } from "hardhat/types"
import { DeployFunction } from "hardhat-deploy/types"
import {
  IdentityRegistry,
  SettlementLib,
  SettlementUtilities,
} from "../test/utils"
import { PredeployConstants } from "../typechain/PredeployConstants"
import fs from "fs"
import { getUnnamedAccounts } from "hardhat"

const {
  ERC1820_REGISTRY_DEPLOY_TX,
  ERC1820_REGISTRY_ADDRESS,
} = require("@openzeppelin/test-helpers/src/data")

/**
 * Runs deployments of contracts that are instrumented for adding to a geth genesis block.
 * For each contract, grabs the deployed bytecode and appends it to the genesis block file
 * so that the specified addresses will have the same bytecode when running on test chain.
 */
const func: DeployFunction = async function (hre: HardhatRuntimeEnvironment) {
  const { deployments, getNamedAccounts, ethers } = hre
  const { deploy } = deployments
  const { exchange, alice, bob } = await getNamedAccounts()
  const accounts = await getUnnamedAccounts()

  const deployFromExchange = async (
    contractName: string,
    args: any[] = [],
    libraries?: Libraries
  ) => {
    const factory = await ethers.getContractFactory(contractName, {
      signer: await ethers.getSigner(exchange),
      libraries: libraries,
    })
    console.log("Deploying " + contractName)
    return await factory.deploy(...args)
  }

  // Object to write to geth genesis block
  interface DeployedBytecodes {
    [address: string]: {
      balance: string
      code?: string
    }
  }
  let deployedBytecodes: DeployedBytecodes = {}
  const addBytecode = async (contractAddress: string, byteCode: string) => {
    deployedBytecodes[contractAddress] = {
      balance: "0",
      code: byteCode,
    }
  }

  // Deploy libraries
  let chainId = await ethers.provider.send("eth_chainId", [])
  let eip712 = await deployFromExchange("ExchangeEIP712", ["1"])
  let library = await deployFromExchange("SettlementLib")
  let percentLib = await deployFromExchange("FullMath")
  let predeployConstants = (await deployFromExchange(
    "PredeployConstants"
  )) as PredeployConstants

  // Deploy ERC1820Registry
  if (
    (await ethers.provider.getCode(ERC1820_REGISTRY_ADDRESS)).length <=
    "0x0".length
  ) {
    console.log("Deploying ERC1820 Registry ...")
    await (
      await (
        await ethers.getSigners()
      )[0].sendTransaction({
        to: "0xa990077c3205cbDf861e17Fa532eeB069cE9fF96",
        value: ethers.utils.parseEther("1.08"),
      })
    ).wait()
    await (
      await ethers.provider.sendTransaction(ERC1820_REGISTRY_DEPLOY_TX)
    ).wait()
  }
  await addBytecode(
    ERC1820_REGISTRY_ADDRESS,
    await ethers.provider.getCode(ERC1820_REGISTRY_ADDRESS)
  )

  const withdrawInterval = hre.network.name.startsWith("test") ? 241 : 6000
  // Identity
  let identity = await deployFromExchange("IdentityRegistryPreDeploy", [
    exchange,
    withdrawInterval,
  ])
  await addBytecode(
    await predeployConstants.callStatic.IDENTITY_REGISTRY(),
    await ethers.provider.getCode(identity.address)
  )

  await addBytecode(
    await predeployConstants.callStatic.LIBRARY(),
    await ethers.provider.getCode(library.address)
  )

  // SettlementUtilities
  let utils = await deployFromExchange("SettlementUtilities")

  await addBytecode(
    await predeployConstants.callStatic.UTILS(),
    await ethers.provider.getCode(utils.address)
  )

  // Asset Custody Contract
  let assetCustody = await deployFromExchange("AssetCustodyPreDeploy", [])
  await addBytecode(
    await predeployConstants.callStatic.SINGLETON(),
    await ethers.provider.getCode(assetCustody.address)
  )

  // Collateral Custody Contract
  let collateralCustody = await deployFromExchange(
    "CollateralCustodyPreDeploy",
    []
  )
  await addBytecode(
    await predeployConstants.callStatic.COLLATERAL_CUSTODY(),
    await ethers.provider.getCode(collateralCustody.address)
  )

  // Coordinator
  let coord = await deployFromExchange("SettlementCoordinationPreDeploy", [])
  await addBytecode(
    await predeployConstants.callStatic.COORDINATOR(),
    await ethers.provider.getCode(coord.address)
  )

  const withSettlementUtilities = { ["SettlementUtilities"]: utils.address }

  // Consensus
  let consensus = await deployFromExchange(
    "SettlementDataConsensusPreDeploy",
    [],
    withSettlementUtilities
  )
  await addBytecode(
    await predeployConstants.callStatic.CONSENSUS(),
    await ethers.provider.getCode(consensus.address)
  )

  // Dummy Coin ERC20
  let dummyCoin = await deployFromExchange("DummyCoinPreDeploy", [[alice, bob]])
  await addBytecode(
    await predeployConstants.callStatic.DUMMY_COIN(),
    await ethers.provider.getCode(dummyCoin.address)
  )

  // USDT
  let USDT = await deployFromExchange("USDTPreDeploy", [[alice, bob]])
  await addBytecode(
    await predeployConstants.callStatic.USDT_COIN(),
    await ethers.provider.getCode(USDT.address)
  )

  // USDC
  let USDC = await deployFromExchange("USDCPreDeploy", [[alice, bob]])
  await addBytecode(
    await predeployConstants.callStatic.USDC_COIN(),
    await ethers.provider.getCode(USDC.address)
  )

  // Mint 2,500 units of native asset to 25 accounts for testing
  const amount = ethers.utils.parseEther("2500")
  for (let i = accounts.length - 1; i > accounts.length - 25; i--) {
    deployedBytecodes[accounts[i]] = {
      balance: amount.toHexString(),
    }
  }

  // append addresses with pre-deployed bytecode to cliquebait genesis block
  let data = await fs.promises.readFile("base_cliquebait.json")
  let genesisBlock = await JSON.parse(data.toString())
  genesisBlock.alloc = Object.assign(genesisBlock.alloc, deployedBytecodes)
  await fs.promises.writeFile("cliquebait.json", JSON.stringify(genesisBlock))
}
func.tags = ["PreDeploy"]
export default func
