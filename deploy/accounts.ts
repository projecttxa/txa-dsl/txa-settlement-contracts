// Copyright © 2022 TXA PTE. LTD.
import { HardhatRuntimeEnvironment } from "hardhat/types"
import { getSigner } from "../test/utils"

const getAccounts = async function (
  hre: HardhatRuntimeEnvironment
): Promise<string[]> {
  const { getNamedAccounts } = hre
  const {
    alice,
    bob,
    charlie,
    sdpAdmin1,
    sdpAdmin2,
    sdpAdmin3,
    sdpAdmin4,
    sdpOperator1,
    sdpOperator2,
    sdpOperator3,
    sdpOperator4,
  } = await getNamedAccounts()
  return [
    alice,
    bob,
    charlie,
    sdpAdmin1,
    sdpAdmin2,
    sdpAdmin3,
    sdpAdmin4,
    sdpOperator1,
    sdpOperator2,
    sdpOperator3,
    sdpOperator4,
  ]
}

export const getSigners = async function (hre: HardhatRuntimeEnvironment) {
  const accounts = await getAccounts(hre)
  const signers = []
  for (const account of accounts) {
    signers.push({
      name: account,
      account,
      signer: await getSigner(account),
    })
  }

  return signers
}

export default getAccounts
