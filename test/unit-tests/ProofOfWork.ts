// Copyright © 2022 TXA PTE. LTD.
import {ProofOfWork} from "../../typechain/ProofOfWork"
import {ProofOfWork__factory} from "../../typechain/factories/ProofOfWork__factory"
import {ethers} from "hardhat"
import {expect} from "chai"

describe("Proof of Work", function () {
    let workFactory: ProofOfWork__factory

    before(async () => {
        workFactory = (await ethers.getContractFactory("ProofOfWork")) as ProofOfWork__factory
    })

    beforeEach(async () => {})

    it("validates a basic nonce", async () => {
        // Deploying with max blocks set to 0, so difficulty will always be minimum (2 bits of 0s)
        const maxBlocks = 0
        const workContract = await workFactory.deploy(maxBlocks)
        expect(
            await workContract.callStatic.validateNonce(
                2,
                ethers.constants.HashZero,
                ethers.constants.AddressZero,
                0,
                ethers.constants.AddressZero,
                0
            )
        ).to.be.true
    })

    it("rejects a nonce without enough zeros", async () => {
        // Difficulty should be greater than 2, so this particular nonce will fail
        const maxBlocks = 100
        const workContract = await workFactory.deploy(maxBlocks)
        const blockNum = await ethers.provider.getBlockNumber()
        expect(
            await workContract.callStatic.validateNonce(
                2,
                ethers.constants.HashZero,
                ethers.constants.AddressZero,
                0,
                ethers.constants.AddressZero,
                blockNum
            )
        ).to.be.false
    })

    it("difficulty scales proportionally to number of blocks that passed", async () => {
        const maxBlocks = 100
        const workContract = await workFactory.deploy(maxBlocks)
        const minDifficulty = await workContract.callStatic.minDifficulty()
        const maxDifficulty = await workContract.callStatic.maxDifficulty()
        const startBlock = await ethers.provider.getBlockNumber()
        let currentBlockNumber = await ethers.provider.getBlockNumber()

        const difficulty = await workContract.callStatic.calculateDifficulty(startBlock)
        expect(difficulty).to.be.eq(maxDifficulty)

        while (currentBlockNumber < startBlock + maxBlocks) {
            await ethers.provider.send("evm_mine", [])
            currentBlockNumber = await ethers.provider.getBlockNumber()
            const difficulty = await workContract.callStatic.calculateDifficulty(startBlock)
            expect(difficulty).to.be.eq(
                maxDifficulty.sub(
                    maxDifficulty
                        .sub(minDifficulty)
                        .mul(currentBlockNumber - startBlock)
                        .div(maxBlocks)
                )
            )
        }
    })
})
