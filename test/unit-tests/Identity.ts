// Copyright © 2022 TXA PTE. LTD.
import {expect} from "chai"
import {Contract, Signer} from "ethers"
import {getNamedAccounts, ethers, getUnnamedAccounts} from "hardhat"
import {Address} from "hardhat-deploy/types"
import {DummyCoin} from "../../typechain/DummyCoin"
import {
    Contracts,
    deploy,
    deployWithLibraries,
    getDeployed,
    getSigner,
    IdentityRegistry,
    setupTest,
    SettlementLib,
} from "../utils"

describe("IdentityRegistry", () => {
    let contracts: Contracts
    let library: SettlementLib
    let percentLib: Contract
    let identity: IdentityRegistry
    let token: DummyCoin

    let exchange: Signer
    let auditor: string

    let unnamedAccounts: Address[]

    let bob: Signer

    let bobAddress: Address

    let feeRecipient: Address
    let noRole: Address

    let assetCustodyRole: string
    let collateralCustodyRole: string
    let coordRole: string
    let exchangeRole: string
    let tokenRole: string
    let auditorRole: string
    let tradeSignerRole: string

    before(async () => {
        const namedAccounts = await getNamedAccounts()
        unnamedAccounts = await getUnnamedAccounts()
        exchange = await getSigner(namedAccounts.exchange)
        auditor = namedAccounts.auditor

        feeRecipient = namedAccounts.feeRecipient
        noRole = namedAccounts.noRole

        bob = await ethers.provider.getSigner(namedAccounts.bob)
        bobAddress = ethers.utils.getAddress(await bob.getAddress())
    })

    beforeEach(async () => {
        const contracts = await setupTest()
        token = contracts.dummyCoin
        library = (await getDeployed("SettlementLib")) as SettlementLib
        percentLib = await getDeployed("FullMath")
        identity = (await deployWithLibraries(
            "IdentityRegistry",
            {SettlementLib: library.address, FullMath: percentLib.address},
            [await exchange.getAddress(), 6000]
        )) as IdentityRegistry
        assetCustodyRole = await library.callStatic.ROLE_ASSET_CUSTODY()
        collateralCustodyRole = await library.callStatic.ROLE_COLLATERAL_CUSTODY()
        coordRole = await library.callStatic.ROLE_LOCALCOORD()
        exchangeRole = await library.callStatic.ROLE_GOVERNANCE()
        tokenRole = await library.callStatic.TRADEABLE_TOKEN()
        auditorRole = await library.callStatic.ROLE_AUDITOR()
        tradeSignerRole = await library.callStatic.ROLE_TRADE_SIGNER()
    })

    it("starts with exchange as ROLE_GOVERNANCE", async () => {
        expect(await identity.hasRole(await library.ROLE_GOVERNANCE(), await exchange.getAddress())).to.be
            .true
    })

    it("returns governance address when calling getGovernanceAddress", async () => {
        expect(await identity.getGovernanceAddress()).to.be.eq(await exchange.getAddress())
    })

    it("stores fee recipient after setting", async () => {
        await identity.setFeeRecipient(feeRecipient)
        expect(await identity.feeRecipient()).to.be.eq(feeRecipient)
    })

    it("only exchange can set fee recipient", async () => {
        for (let i = 1; i < 4; i++) {
            await expect(
                identity.connect(await getSigner(unnamedAccounts[i])).setFeeRecipient(feeRecipient)
            ).to.be.revertedWith("SENDER_NOT_GOVERNANCE")
        }
    })

    it("calculates fee correctly", async () => {
        const {n, d} = await identity.fee()
        const amount = 1000
        const expectedFee = 1000 * 0.001

        expect((await identity.calculateFee(amount)).eq(expectedFee)).to.be.true
    })

    describe("Managing ROLE_LOCALCOORD", async () => {
        it("only exchange can initialize coordinator", async () => {
            for (let i = 1; i < 4; i++) {
                await expect(
                    identity.connect(await getSigner(unnamedAccounts[i])).initializeRole(coordRole, noRole)
                ).to.be.revertedWith("SENDER_NOT_GOVERNANCE")
            }
        })

        it("initializing coordinator gives ROLE_LOCALCOORD", async () => {
            await identity.initializeRole(coordRole, noRole)
            expect(await identity.callStatic.hasRole(await library.ROLE_LOCALCOORD(), noRole)).to.be.true
        })

        it("forbids initializing coordinator more than once", async () => {
            await identity.initializeRole(coordRole, noRole)
            await expect(identity.initializeRole(coordRole, noRole)).to.be.revertedWith("ALREADY_INITIALIZED")
        })

        it("cannot retire coordinator before initializing", async () => {
            await expect(identity.updateRole(coordRole, unnamedAccounts[15])).to.be.revertedWith(
                "NOT_INITIALIZED"
            )
        })

        it("only exchange can retire coordinator", async () => {
            await identity.initializeRole(coordRole, noRole)
            for (let i = 1; i < 4; i++) {
                await expect(
                    identity
                        .connect(await getSigner(unnamedAccounts[i]))
                        .updateRole(coordRole, unnamedAccounts[15])
                ).to.be.revertedWith("SENDER_NOT_GOVERNANCE")
            }
        })

        it("retiring coordinator gives new coordinator ROLE_LOCALCOORD", async () => {
            await identity.initializeRole(coordRole, noRole)
            await identity.updateRole(coordRole, unnamedAccounts[15])
            expect(await identity.callStatic.hasRole(await library.ROLE_LOCALCOORD(), unnamedAccounts[15])).to
                .be.true
        })

        it("coordinator view functions return proper values after initializing coordinator", async () => {
            await identity.initializeRole(coordRole, noRole)

            expect((await identity.getLatestCoordinatorVersion()).eq(1)).to.be.true

            expect(await identity.getLatestCoordinator()).to.be.eq(noRole)

            expect(await identity["isUnexpiredCoordinator(address)"](noRole)).to.be.true
        })
    })

    describe("Managing tradeable tokens", () => {
        it("allows exchange to add a tradeable token address", async () => {
            await identity.connect(exchange).grantRole(tokenRole, token.address)

            expect(await identity.callStatic.hasRole(tokenRole, token.address)).to.be.true
        })

        it("forbids non-exchange from adding a tradeable token address", async () => {
            for (let address of [feeRecipient, noRole]) {
                const signer = await ethers.getSigner(address)
                await expect(identity.connect(signer).grantRole(tokenRole, token.address)).to.be.revertedWith(
                    `AccessControl: account ${address.toLowerCase()} is missing role ${exchangeRole}`
                )
            }

            expect(await identity.callStatic.hasRole(tokenRole, token.address)).to.be.false
        })

        it("allows exchange to revoke a tradeable token address", async () => {
            await identity.connect(exchange).grantRole(tokenRole, token.address)
            await identity.connect(exchange).revokeRole(tokenRole, token.address)

            expect(await identity.callStatic.hasRole(tokenRole, token.address)).to.be.false
        })

        it("forbids non-exchange from revoking a tradeable token address", async () => {
            await identity.connect(exchange).grantRole(tokenRole, token.address)
            for (let address of [feeRecipient, noRole]) {
                const signer = await ethers.getSigner(address)
                await expect(
                    identity.connect(signer).revokeRole(tokenRole, token.address)
                ).to.be.revertedWith(
                    `AccessControl: account ${address.toLowerCase()} is missing role ${exchangeRole}`
                )
            }

            expect(await identity.callStatic.hasRole(tokenRole, token.address)).to.be.true
        })

        it("emits an event when adding a tradeable token", async () => {
            const exchangeAddress = await exchange.getAddress()
            const tx = await (await identity.connect(exchange).grantRole(tokenRole, token.address)).wait()
            expect(
                tx.logs
                    .map(log => identity.interface.parseLog(log))
                    .some(log => {
                        if (
                            log.eventFragment.name ==
                            identity.interface.events["RoleGranted(bytes32,address,address)"].name
                        ) {
                            return (
                                tokenRole == log.args[0] &&
                                token.address == log.args[1] &&
                                exchangeAddress == log.args[2]
                            )
                        } else {
                            return false
                        }
                    })
            ).to.be.true
        })

        it("emits an event when revoking a tradeable token", async () => {
            const exchangeAddress = await exchange.getAddress()
            await identity.connect(exchange).grantRole(tokenRole, token.address)
            const tx = await (await identity.connect(exchange).revokeRole(tokenRole, token.address)).wait()
            expect(
                tx.logs
                    .map(log => identity.interface.parseLog(log))
                    .some(log => {
                        if (
                            log.eventFragment.name ==
                            identity.interface.events["RoleRevoked(bytes32,address,address)"].name
                        ) {
                            return (
                                tokenRole == log.args[0] &&
                                token.address == log.args[1] &&
                                exchangeAddress == log.args[2]
                            )
                        } else {
                            return false
                        }
                    })
            ).to.be.true
        })
        it("Function isTradeableToken returns true for tradable token address", async () => {
            await identity.connect(exchange).grantRole(tokenRole, token.address)
            expect(await identity.callStatic.isTradeableToken(token.address)).to.be.true
        })
        it("Function isTradeableToken returns false for non - tradable token address", async () => {
            expect(await identity.callStatic.isTradeableToken(bobAddress)).to.be.false
        })
    })

    describe("Managing auditor", () => {
        it("allows exchange to add an auditor", async () => {
            await identity.connect(exchange).grantRole(auditorRole, auditor)

            expect(await identity.callStatic.hasRole(auditorRole, auditor)).to.be.true
        })

        it("forbids non-exchange from adding an auditor", async () => {
            for (let address of [feeRecipient, noRole]) {
                const signer = await ethers.getSigner(address)
                await expect(identity.connect(signer).grantRole(auditorRole, auditor)).to.be.revertedWith(
                    `AccessControl: account ${address.toLowerCase()} is missing role ${exchangeRole}`
                )
            }

            expect(await identity.callStatic.hasRole(auditorRole, auditor)).to.be.false
        })

        it("allows exchange to revoke an auditor", async () => {
            await identity.connect(exchange).grantRole(auditorRole, auditor)
            await identity.connect(exchange).revokeRole(auditorRole, auditor)

            expect(await identity.callStatic.hasRole(auditorRole, auditor)).to.be.false
        })

        it("forbids non-exchange from revoking an auditor", async () => {
            await identity.connect(exchange).grantRole(auditorRole, auditor)
            for (let address of [feeRecipient, noRole]) {
                const signer = await ethers.getSigner(address)
                await expect(identity.connect(signer).revokeRole(auditorRole, auditor)).to.be.revertedWith(
                    `AccessControl: account ${address.toLowerCase()} is missing role ${exchangeRole}`
                )
            }

            expect(await identity.callStatic.hasRole(auditorRole, auditor)).to.be.true
        })

        it("emits an event when adding an auditor", async () => {
            const exchangeAddress = await exchange.getAddress()
            const tx = await (await identity.connect(exchange).grantRole(auditorRole, auditor)).wait()
            expect(
                tx.logs
                    .map(log => identity.interface.parseLog(log))
                    .some(log => {
                        if (
                            log.eventFragment.name ==
                            identity.interface.events["RoleGranted(bytes32,address,address)"].name
                        ) {
                            return (
                                auditorRole == log.args[0] &&
                                auditor == log.args[1] &&
                                exchangeAddress == log.args[2]
                            )
                        } else {
                            return false
                        }
                    })
            ).to.be.true
        })

        it("emits an event when revoking an auditor", async () => {
            const exchangeAddress = await exchange.getAddress()
            await identity.connect(exchange).grantRole(auditorRole, auditor)
            const tx = await (await identity.connect(exchange).revokeRole(auditorRole, auditor)).wait()
            expect(
                tx.logs
                    .map(log => identity.interface.parseLog(log))
                    .some(log => {
                        if (
                            log.eventFragment.name ==
                            identity.interface.events["RoleRevoked(bytes32,address,address)"].name
                        ) {
                            return (
                                auditorRole == log.args[0] &&
                                auditor == log.args[1] &&
                                exchangeAddress == log.args[2]
                            )
                        } else {
                            return false
                        }
                    })
            ).to.be.true
        })

        it("Function isAuditor returns true for auditor address", async () => {
            await identity.connect(exchange).grantRole(auditorRole, auditor)
            expect(await identity.callStatic.isAuditor(auditor)).to.be.true
        })

        it("Function isAuditor returns false for non-auditor address", async () => {
            expect(await identity.callStatic.isAuditor(bobAddress)).to.be.false
        })
    })

    describe("managing ROLE_ASSET_CUSTODY", async () => {
        it("only exchange can initialize asset custody contract role", async () => {
            for (let i = 1; i < 4; i++) {
                await expect(
                    identity
                        .connect(await getSigner(unnamedAccounts[i]))
                        .initializeRole(assetCustodyRole, noRole)
                ).to.be.revertedWith("SENDER_NOT_GOVERNANCE")
            }
        })

        it("initializing asset custody contract gives ROLE_ASSET_CUSTODY", async () => {
            await identity.initializeRole(assetCustodyRole, noRole)
            expect(await identity.callStatic.hasRole(await library.ROLE_ASSET_CUSTODY(), noRole)).to.be.true
        })
    })

    describe("managing ROLE_COLLATERAL_CUSTODY", async () => {
        it("only exchange can initialize collateral custody contract role", async () => {
            for (let i = 1; i < 4; i++) {
                await expect(
                    identity
                        .connect(await getSigner(unnamedAccounts[i]))
                        .initializeRole(collateralCustodyRole, noRole)
                ).to.be.revertedWith("SENDER_NOT_GOVERNANCE")
            }
        })

        it("initializing collateral custody contract gives ROLE_COLLATERAL_CUSTODY", async () => {
            await identity.initializeRole(collateralCustodyRole, noRole)
            expect(await identity.callStatic.hasRole(await library.ROLE_COLLATERAL_CUSTODY(), noRole)).to.be
                .true
        })
    })

    describe("managing ROLE_TRADE_SIGNER", async () => {
        it("only exchange can initialize collateral custody contract role", async () => {
            for (let i = 1; i < 4; i++) {
                await expect(
                    identity
                        .connect(await getSigner(unnamedAccounts[i]))
                        .initializeRole(tradeSignerRole, noRole)
                ).to.be.revertedWith("SENDER_NOT_GOVERNANCE")
            }
        })

        it("initializing collateral custody contract gives ROLE_COLLATERAL_CUSTODY", async () => {
            await identity.initializeRole(tradeSignerRole, noRole)
            expect(await identity.callStatic.hasRole(await library.ROLE_TRADE_SIGNER(), noRole)).to.be.true
        })

        it("Function isTradeSigner returns true for tradeSigner address", async () => {
            await identity.connect(exchange).initializeRole(tradeSignerRole, noRole)
            expect(await identity.callStatic.isTradeSigner(noRole)).to.be.true
        })

        it("Function isTradeSigner returns false for non-tradeSigner address", async () => {
            expect(await identity.callStatic.isTradeSigner(bobAddress)).to.be.false
        })
    })
})
