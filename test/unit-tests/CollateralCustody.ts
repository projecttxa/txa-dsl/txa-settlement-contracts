// Copyright © 2022 TXA PTE. LTD.
import {expect} from "chai"
import {ethers, getNamedAccounts, getUnnamedAccounts} from "hardhat"
import {Address} from "hardhat-deploy/dist/types"
import {Contract, Signer} from "ethers"

import {
    AssetCustody,
    Contracts,
    setupTest,
    _za,
    DummyCoin,
    ProtocolToken,
    getSigner,
    IdentityRegistry,
    SettlementLib,
    getDeployed,
    CoordinationMock,
    ConsensusMock,
    CollateralCustody,
    Obligation,
    SettlementDataConsensus,
    approveAndDeposit,
    operatorInit,
    Signature,
} from "../utils"

describe("Collateral Custody Contract", function () {
    const hre = require("hardhat")
    this.timeout(200000)

    let identity: IdentityRegistry
    let library: SettlementLib
    let coordination: CoordinationMock
    let consensus: SettlementDataConsensus
    let consensusMock: ConsensusMock
    let unnamedAccounts: Address[]
    let exchange: Signer

    let sdpAdmin1: Signer
    let sdpAdmin2: Signer
    let sdpAdmin3: Signer
    let sdpOperator1: Signer
    let sdpOperator2: Signer
    let sdpOperator3: Signer
    let bob: Signer
    let alice: Signer

    let sdpAdmin1Address: Address
    let sdpAdmin2Address: Address
    let sdpAdmin3Address: Address
    let sdpOperator1Address: Address
    let sdpOperator2Address: Address
    let sdpOperator3Address: Address
    let collateralAddress: Address

    let bobAddress: Address
    let aliceAddress: Address
    let feeRecipientAddress: Address

    let sdpAdmin1Custody: CollateralCustody
    let sdpAdmin2Custody: CollateralCustody
    let sdpAdmin3Custody: CollateralCustody
    let sdpOperator1Custody: CollateralCustody
    let sdpOperator2Custody: CollateralCustody
    let sdpOperator3Custody: CollateralCustody
    let collateralCustody: CollateralCustody
    let zeroEth = ethers.utils.parseEther("0")
    let merkleRootZero = "0x0000000000000000000000000000000000000000000000000000000000000000"

    let contracts: Contracts
    let dummyCoin: DummyCoin
    let protocolToken: ProtocolToken

    let coordRole: string
    let tokenRole: string
    let exchangeRole: string
    let consensusRole: string
    let sdpAdminRole: string

    let contractBalanceBefore = zeroEth
    let sdpAdmin1CustodyBalanceBefore = zeroEth

    const ethTransfer = ethers.utils.parseEther("0.7")
    let tokenDeposit = 1450
    const sdpAdmin1Deposit = 4900
    const sdpAdmin2Deposit = 2400
    const totalDeposits = sdpAdmin1Deposit + sdpAdmin2Deposit
    const sdpAdmin1DepositEth = ethers.utils.parseEther("4.9")
    const sdpAdmin2DepositEth = ethers.utils.parseEther("2.4")
    const totalDepositsEth = sdpAdmin1DepositEth.add(sdpAdmin2DepositEth)

    let obs: Obligation[]
    let settlementId = zeroEth

    let sdpLockedBefore = zeroEth
    let sdp2LockedBefore = zeroEth
    let bobLockedBefore = zeroEth
    let sdpBalanceBefore = zeroEth
    let sdp2BalanceBefore = zeroEth
    let bobBalanceBefore = zeroEth
    let sdpLockedAfter = zeroEth
    let sdp2LockedAfter = zeroEth
    let bobLockedAfter = zeroEth
    let sdpBalanceAfter = zeroEth
    let sdp2BalanceAfter = zeroEth
    let bobBalanceAfter = zeroEth

    let sdpAdmin1AddressBytes: Uint8Array
    let sdpAdmin2AddressBytes: Uint8Array
    let sdpAdmin3AddressBytes: Uint8Array
    let signedMessage: string
    let signature: string
    let r: string
    let s: string
    let v: number
    let sig: Signature

    const sdpAdmin1EthDeposits = [
        ethers.utils.parseEther("1.3"),
        ethers.utils.parseEther("0.4"),
        ethers.utils.parseEther("2.9"),
    ]
    const sdpAdmin2EthDeposits = [
        ethers.utils.parseEther("2.2"),
        ethers.utils.parseEther("0.3"),
        ethers.utils.parseEther("2.8"),
    ]
    const sdpAdmin1Deposits = [1300, 400, 2900]
    const sdpAdmin2Deposits = [2200, 300, 2800]
    let sdpAdmin1DepositSum = zeroEth
    let sdpAdmin2DepositSum = zeroEth

    const sigMethod = "eth_signTypedData_v4"

    before(async () => {
        unnamedAccounts = await getUnnamedAccounts()
        const namedAccounts = await getNamedAccounts()

        /**
         * TODO: replace bob alice charlie with sdpAdmin1 sdpAdmin2 sdpAdmin3 named accounts after figuring out how to
         * start the SDP signers off with full token balance set
         */
        sdpAdmin1 = await ethers.provider.getSigner(namedAccounts.sdpAdmin1)
        sdpAdmin2 = await ethers.provider.getSigner(namedAccounts.sdpAdmin2)
        sdpAdmin3 = await ethers.provider.getSigner(namedAccounts.sdpAdmin3)
        sdpOperator1 = await ethers.provider.getSigner(namedAccounts.sdpOperator1)
        sdpOperator2 = await ethers.provider.getSigner(namedAccounts.sdpOperator2)
        sdpOperator3 = await ethers.provider.getSigner(namedAccounts.sdpOperator3)

        bob = await ethers.provider.getSigner(namedAccounts.bob)
        alice = await ethers.provider.getSigner(namedAccounts.alice)
        aliceAddress = ethers.utils.getAddress(await alice.getAddress())
        bobAddress = ethers.utils.getAddress(await bob.getAddress())

        sdpAdmin2Address = ethers.utils.getAddress(await sdpAdmin2.getAddress())
        sdpAdmin1Address = ethers.utils.getAddress(await sdpAdmin1.getAddress())
        sdpAdmin3Address = ethers.utils.getAddress(await sdpAdmin3.getAddress())
        sdpOperator2Address = ethers.utils.getAddress(await sdpOperator2.getAddress())
        sdpOperator1Address = ethers.utils.getAddress(await sdpOperator1.getAddress())
        sdpOperator3Address = ethers.utils.getAddress(await sdpOperator3.getAddress())
        feeRecipientAddress = namedAccounts.feeRecipient
        exchange = await getSigner(namedAccounts.exchange)
    })

    beforeEach(async function () {
        contracts = await setupTest()
        identity = contracts.identity
        coordination = contracts.coordinationMock
        consensus = contracts.consensus
        consensusMock = contracts.consensusMock

        collateralCustody = contracts.collateralCustody
        sdpAdmin1Custody = collateralCustody.connect(sdpAdmin1) // could also use a factory
        sdpAdmin2Custody = collateralCustody.connect(sdpAdmin2)
        sdpAdmin3Custody = collateralCustody.connect(sdpAdmin3)
        sdpOperator1Custody = collateralCustody.connect(sdpOperator1) // could also use a factory
        sdpOperator2Custody = collateralCustody.connect(sdpOperator2)
        sdpOperator3Custody = collateralCustody.connect(sdpOperator3)
        dummyCoin = contracts.dummyCoin
        protocolToken = contracts.protocolToken
        library = (await getDeployed("SettlementLib")) as SettlementLib

        collateralAddress = collateralCustody.address

        coordRole = await library.callStatic.ROLE_LOCALCOORD()
        tokenRole = await library.callStatic.TRADEABLE_TOKEN()
        consensusRole = await library.callStatic.ROLE_CONSENSUS()
        sdpAdminRole = await library.callStatic.ROLE_SDP_ADMIN()
    })

    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    describe("Test Native Token Deposits", async () => {
        beforeEach(async () => {
            contractBalanceBefore = await ethers.provider.getBalance(collateralCustody.address)
            sdpAdmin1CustodyBalanceBefore = await collateralCustody.collateralBalances(sdpAdmin1Address, _za)
            await expect(sdpAdmin1CustodyBalanceBefore).to.equal(zeroEth)
            await expect(contractBalanceBefore).to.equal(zeroEth)
            sdpAdmin1DepositSum = zeroEth
            sdpAdmin2DepositSum = zeroEth
        })
        it("Accepts native token deposits and recieves tokens to contract", async () => {
            /**
             * User Deposits native token using deposit function part 1:
             * The smart contract receives the token to contract address
             * sent using the deposit function.
             */

            //sdpAdmin1 deposits eth to the contract using the deposit function
            await sdpAdmin1Custody.deposit({value: sdpAdmin1DepositEth})
            let contractBalanceAfter = await ethers.provider.getBalance(collateralCustody.address)
            await expect(contractBalanceAfter).to.equal(sdpAdmin1DepositEth)
        })

        it("Accepts native token deposits to user custody account in contract", async () => {
            /**
             * User Deposits native token using deposit function part 2:
             * Upon receipt of the native token using the deposit function the
             * smart contract records the ownership of the exact token quantity
             * to the mapping at the index: [user address, token address:_za]
             */
            //sdpAdmin1 deposits eth to the contract using the deposit function
            await sdpAdmin1Custody.deposit({value: sdpAdmin1DepositEth})
            let sdpAdmin1CustodyBalanceAfter = await collateralCustody.collateralBalances(
                sdpAdmin1Address,
                _za
            )
            await expect(sdpAdmin1CustodyBalanceAfter).to.equal(sdpAdmin1DepositEth)
        })

        it("Accepts multiple native token deposits to collateral custody account ", async () => {
            /**
             * Smart Contract accepts multiple deposits from the same address
             * using the deposit function. Will pass if both contract balance
             * and user collateral account map location is updated properly
             *
             * Native Token
             */

            for (let ii = 0; ii < sdpAdmin1EthDeposits.length; ii++) {
                await sdpAdmin1Custody.deposit({value: sdpAdmin1EthDeposits[ii]})
                sdpAdmin1DepositSum = sdpAdmin1DepositSum.add(sdpAdmin1EthDeposits[ii])
            }
            let sdpAdmin1CustodyBalanceAfter = await collateralCustody.collateralBalances(
                sdpAdmin1Address,
                _za
            )
            let contractBalanceAfter = await ethers.provider.getBalance(collateralCustody.address)
            await expect(sdpAdmin1CustodyBalanceAfter).to.equal(sdpAdmin1DepositSum)
            await expect(contractBalanceAfter).to.equal(sdpAdmin1DepositSum)
        })

        it("Accepts multiple native token deposits to multiple collateral custody addresses", async () => {
            /**
             * Smart Contract accepts multiple deposits from multiple addresses
             * using the deposit function. Will pass if both contract balance
             * and both user collateral account map locations are updated properly
             *
             * Native Token
             */

            //interleaved deposits from sdpAdmin1 & sdpAdmin2 to asset custody contract

            for (let ii = 0; ii < sdpAdmin1EthDeposits.length; ii++) {
                await sdpAdmin1Custody.deposit({value: sdpAdmin1EthDeposits[ii]})
                await sdpAdmin2Custody.deposit({value: sdpAdmin2EthDeposits[ii]})
                sdpAdmin1DepositSum = sdpAdmin1DepositSum.add(sdpAdmin1EthDeposits[ii])
                sdpAdmin2DepositSum = sdpAdmin2DepositSum.add(sdpAdmin2EthDeposits[ii])
            }

            let sdpAdmin1CustodyBalanceAfter = await collateralCustody.collateralBalances(
                sdpAdmin1Address,
                _za
            )
            let sdpAdmin2CustodyBalanceAfter = await collateralCustody.collateralBalances(
                sdpAdmin2Address,
                _za
            )
            let contractBalanceAfter = await ethers.provider.getBalance(collateralCustody.address)
            await expect(sdpAdmin1CustodyBalanceAfter).to.equal(sdpAdmin1DepositSum)
            await expect(sdpAdmin2CustodyBalanceAfter).to.equal(sdpAdmin2DepositSum)
            await expect(contractBalanceAfter).to.equal(sdpAdmin1DepositSum.add(sdpAdmin2DepositSum))
        })

        it("Rejects native token sent to contract without using the deposit fuction", async () => {
            /**
             * Smart contract rejects a generic deposit.
             * This means a deposit sent as a generic blockchain transaction with
             * a non-zero value field will be rejected.
             * mySigner.sendTransaction({value: 10, to:myContract.address,}) in ethers
             */
            await expect(
                sdpAdmin1.sendTransaction({
                    value: sdpAdmin1DepositEth,
                    to: collateralCustody.address,
                })
            ).to.be.revertedWith(
                "Transaction reverted: function selector was not recognized and there's no fallback nor receive function"
            )
        })

        it("Rejects native token depostis not sent by approved SDP Admin", async () => {
            //sdpAdmin1 deposits eth to the contract using the deposit function
            await expect(
                collateralCustody.connect(sdpOperator1).deposit({value: sdpAdmin1DepositEth})
            ).to.be.revertedWith("NOT_SDP_ADMIN")
            await expect(
                collateralCustody.connect(bob).deposit({value: sdpAdmin1DepositEth})
            ).to.be.revertedWith("NOT_SDP_ADMIN")
        })

        it("Native token deposit to collateral custody emits an event", async () => {
            /**
             * Makes a native token deposit and checks to see that the contract emits an event
             * in the form CollateralDeposit(user address, token amount, token zero address) as expected
             */
            await expect(sdpAdmin1Custody.deposit({value: sdpAdmin1DepositEth}))
                .to.emit(collateralCustody, "CollateralDeposit")
                .withArgs(sdpAdmin1Address, sdpAdmin1DepositEth, _za)
        })
    })
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------

    describe("Tests ERC20 Token Deposits", async () => {
        it("Accepts ERC20 token deposits and recieves tokens to contract", async () => {
            await dummyCoin.connect(sdpAdmin1).approve(collateralCustody.address, sdpAdmin1Deposit)
            await sdpAdmin1Custody.depositToken(sdpAdmin1Deposit, dummyCoin.address)
            const contractBalance = await dummyCoin.balanceOf(collateralCustody.address)

            expect(contractBalance).equal(sdpAdmin1Deposit)
        })

        it("Accepts ERC20 token deposits and updates user collateral custody account", async () => {
            await dummyCoin.connect(sdpAdmin1).approve(collateralCustody.address, sdpAdmin1Deposit)
            await sdpAdmin1Custody.depositToken(sdpAdmin1Deposit, dummyCoin.address)
            let sdpAdmin1CustodyBalanceAfter = await collateralCustody.collateralBalances(
                sdpAdmin1Address,
                dummyCoin.address
            )

            expect(sdpAdmin1CustodyBalanceAfter).equal(sdpAdmin1Deposit)
        })

        it("Accepts multiple ERC20 token deposits to same collateral custody account ", async () => {
            /**
             * Smart Contract accepts multiple deposits from the same address
             * using the deposit function. Will pass if both contract balance
             * and user collateral account map location is updated properly
             * ERC20
             */

            let sdpAdmin1DepositSum = 0
            for (let ii = 0; ii < sdpAdmin1Deposits.length; ii++) {
                await approveAndDeposit(dummyCoin, sdpAdmin1, collateralCustody, sdpAdmin1Deposits[ii])
                sdpAdmin1DepositSum += sdpAdmin1Deposits[ii]
            }
            let sdpAdmin1CustodyBalanceAfter = await collateralCustody.collateralBalances(
                sdpAdmin1Address,
                dummyCoin.address
            )
            const contractBalance = await dummyCoin.balanceOf(collateralCustody.address)

            await expect(sdpAdmin1CustodyBalanceAfter).to.equal(sdpAdmin1DepositSum)
            await expect(contractBalance).to.equal(sdpAdmin1DepositSum)
        })

        it("Accepts multiple ERC20 token deposits to multiple collateral custody accounts", async () => {
            /**
             * Smart Contract accepts multiple deposits from multiple addresses
             * using the deposit function. Will pass if both contract balance
             * and both user collateral account map locations are updated properly
             * ERC20
             */
            let sdpAdmin1DepositSum = 0
            let sdpAdmin2DepositSum = 0
            for (let ii = 0; ii < sdpAdmin1Deposits.length; ii++) {
                await approveAndDeposit(dummyCoin, sdpAdmin1, collateralCustody, sdpAdmin1Deposits[ii])
                sdpAdmin1DepositSum += sdpAdmin1Deposits[ii]
                await approveAndDeposit(dummyCoin, sdpAdmin2, collateralCustody, sdpAdmin2Deposits[ii])
                sdpAdmin2DepositSum += sdpAdmin2Deposits[ii]
            }

            let sdpAdmin1CustodyBalanceAfter = await collateralCustody.collateralBalances(
                sdpAdmin1Address,
                dummyCoin.address
            )
            let sdpAdmin2CustodyBalanceAfter = await collateralCustody.collateralBalances(
                sdpAdmin2Address,
                dummyCoin.address
            )
            const contractBalance = await dummyCoin.balanceOf(collateralCustody.address)

            await expect(sdpAdmin1CustodyBalanceAfter).to.equal(sdpAdmin1DepositSum)
            await expect(sdpAdmin2CustodyBalanceAfter).to.equal(sdpAdmin2DepositSum)
            await expect(contractBalance).to.equal(sdpAdmin1DepositSum + sdpAdmin2DepositSum)
        })

        it("Reverts upon failed ERC20 token deposit and does not update user custody account", async () => {
            /**
             * makes a deposit higher than the approved amount to trigger ERC20 deposit failure
             * verifies that the user collateral account balance, and contract ERC20 balance does not change
             */
            // tramsfer allowance set to be 50 less than sdpAdmin1Deposit to trigger a failed transfer
            await dummyCoin.connect(sdpAdmin1).approve(collateralCustody.address, sdpAdmin1Deposit - 50)

            await expect(sdpAdmin1Custody.depositToken(sdpAdmin1Deposit, dummyCoin.address)).to.be.reverted

            let sdpAdmin1CustodyBalanceAfter = await collateralCustody.collateralBalances(
                sdpAdmin1Address,
                dummyCoin.address
            )
            const contractBalance = await dummyCoin.balanceOf(collateralCustody.address)
            expect(contractBalance).equal(0)
            expect(sdpAdmin1CustodyBalanceAfter).equal(0)
        })

        it("Rejects ERC20 token depostis not sent by approved SDP Admin", async () => {
            //sdpAdmin1 deposits eth to the contract using the deposit function
            await dummyCoin.connect(bob).approve(collateralCustody.address, sdpAdmin1Deposit)
            await expect(
                collateralCustody.connect(bob).depositToken(sdpAdmin1Deposit, dummyCoin.address)
            ).to.be.revertedWith("NOT_SDP_ADMIN")
            await dummyCoin.connect(sdpOperator1).approve(collateralCustody.address, sdpAdmin1Deposit)
            await expect(
                collateralCustody.connect(sdpOperator1).depositToken(sdpAdmin1Deposit, dummyCoin.address)
            ).to.be.revertedWith("NOT_SDP_ADMIN")
        })

        it("ERC20 token deposit to collateral custody contract emits an event", async () => {
            /**
             * Makes an ERC20 token deposit and checks to see that the contract emits an event
             * in the form CollateralDeposit(user address, token amount, token address) as expected
             */
            await dummyCoin.connect(sdpAdmin1).approve(collateralCustody.address, sdpAdmin1Deposit)
            await expect(sdpAdmin1Custody.depositToken(sdpAdmin1Deposit, dummyCoin.address))
                .to.emit(collateralCustody, "CollateralDeposit")
                .withArgs(sdpAdmin1Address, sdpAdmin1Deposit, dummyCoin.address)
        })
        it("Rejects token deposit of untradeable tokens, accepts deposit of tradeable tokens", async () => {
            await identity.connect(exchange).revokeRole(tokenRole, dummyCoin.address)

            const sdpAdmin1Deposit = 1000
            await dummyCoin.connect(sdpAdmin1).approve(collateralCustody.address, sdpAdmin1Deposit)

            await expect(
                sdpAdmin1Custody.depositToken(sdpAdmin1Deposit, dummyCoin.address)
            ).to.be.revertedWith("NOT_DEPOSITABLE_TOKEN")

            await identity.connect(exchange).grantRole(tokenRole, dummyCoin.address)

            await expect(sdpAdmin1Custody.depositToken(sdpAdmin1Deposit, dummyCoin.address)).to.not.be
                .reverted
        })
    })
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    describe("Role Assignment", async () => {
        it("Collateral Custody Role set correctly in setupTest()", async () => {
            /**
             * Checks that the identity.getlatestCollateralCustody() returns the address of the
             * collateral custody contract retrieved from the setupTest() function
             */
            expect(await identity.getLatestCollateralCustody()).to.be.equal(collateralCustody.address)
        })
    })

    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------

    describe("Test Token Locking", async () => {
        beforeEach(async () => {
            // approve sdp operators
            await operatorInit(sdpAdmin1, sdpOperator1, collateralCustody)
            await operatorInit(sdpAdmin2, sdpOperator2, collateralCustody)
            await operatorInit(sdpAdmin3, sdpOperator3, collateralCustody)

            //load up collateral accounts with native token
            await sdpAdmin1Custody.deposit({value: sdpAdmin1DepositEth})
            await sdpAdmin2Custody.deposit({value: sdpAdmin2DepositEth})

            expect(await ethers.provider.getBalance(collateralCustody.address)).to.equal(
                sdpAdmin1DepositEth.add(sdpAdmin2DepositEth)
            )
            // load up collateral accounts with dummyCoin
            await approveAndDeposit(dummyCoin, sdpAdmin1, collateralCustody, sdpAdmin1Deposit)
            await expect(await dummyCoin.balanceOf(collateralCustody.address)).to.be.eq(sdpAdmin1Deposit)
            await approveAndDeposit(dummyCoin, sdpAdmin2, collateralCustody, sdpAdmin2Deposit)
            await expect(await dummyCoin.balanceOf(collateralCustody.address)).to.be.eq(
                sdpAdmin2Deposit + sdpAdmin1Deposit
            )
            //set consenusMock with ROLE_CONSENSUS so it can call token lock in collateral custody contract
            await identity.updateRole(consensusRole, consensusMock.address)
        })

        it("Allows Consensus to lock tokens", async () => {
            await expect(
                consensusMock.callLockCollateral(
                    sdpOperator1Address,
                    dummyCoin.address,
                    sdpAdmin1Deposit,
                    collateralCustody.address
                )
            ).to.not.be.reverted
        })

        it("lockedBalances mapping in CollateralCustody updated by lockCollateral()", async () => {
            //test locking dummycoin
            await consensusMock.callLockCollateral(
                sdpOperator1Address,
                dummyCoin.address,
                sdpAdmin1Deposit,
                collateralCustody.address
            )
            await consensusMock.callLockCollateral(
                sdpOperator2Address,
                dummyCoin.address,
                sdpAdmin2Deposit - 50,
                collateralCustody.address
            )
            await consensusMock.callLockCollateral(
                sdpOperator2Address,
                dummyCoin.address,
                50,
                collateralCustody.address
            )
            //test locking native token
            await consensusMock.callLockCollateral(
                sdpOperator1Address,
                _za,
                sdpAdmin1DepositEth,
                collateralCustody.address
            )
            await consensusMock.callLockCollateral(
                sdpOperator2Address,
                _za,
                sdpAdmin2DepositEth.sub(50),
                collateralCustody.address
            )
            await consensusMock.callLockCollateral(sdpOperator2Address, _za, 50, collateralCustody.address)
            // dummyCoin lockedBalances checks
            expect(await collateralCustody.lockedBalances(sdpAdmin1Address, dummyCoin.address)).to.be.eq(
                sdpAdmin1Deposit
            )
            expect(await collateralCustody.lockedBalances(sdpAdmin2Address, dummyCoin.address)).to.be.eq(
                sdpAdmin2Deposit
            )
            // native token lockedBalances checks
            expect(await collateralCustody.lockedBalances(sdpAdmin1Address, _za)).to.be.eq(
                sdpAdmin1DepositEth
            )
            expect(await collateralCustody.lockedBalances(sdpAdmin2Address, _za)).to.be.eq(
                sdpAdmin2DepositEth
            )
        })
        it("dissallows non Consensus from locking tokens", async () => {
            await expect(
                collateralCustody
                    .connect(sdpAdmin1)
                    .lockCollateral(sdpAdmin1Address, dummyCoin.address, sdpAdmin1Deposit)
            ).to.be.revertedWith("SENDER_NOT_CONSENSUS")
        })
        it("Token Lock Fails if not enough unlocked tokens", async () => {
            await expect(
                consensusMock.callLockCollateral(
                    sdpOperator1Address,
                    dummyCoin.address,
                    sdpAdmin1Deposit + 1,
                    collateralCustody.address
                )
            ).to.be.revertedWith("INSUFFICIENT_UNLOCKED_TOKENS")
            await consensusMock.callLockCollateral(
                sdpOperator1Address,
                dummyCoin.address,
                sdpAdmin1Deposit,
                collateralCustody.address
            )
            await expect(
                consensusMock.callLockCollateral(
                    sdpOperator1Address,
                    dummyCoin.address,
                    1,
                    collateralCustody.address
                )
            ).to.be.revertedWith("INSUFFICIENT_UNLOCKED_TOKENS")
        })
    })

    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------

    describe("testing ERC20 withdraw", async () => {
        /**
         * withdraw() function in asset custody contract tested
         * from a starting point of funded accounts sdpAdmin2 admin and sdpAdmin2 admin
         * function is executed and the correct execution of several operations is verified
         *
         * withdraw allows the withdrawal of native tokens and ERC20 tokens
         * ERC20 token withdrawal is being tested here
         */

        beforeEach(async () => {
            //load up custody accounts
            await approveAndDeposit(dummyCoin, sdpAdmin1, collateralCustody, sdpAdmin1Deposit)
            await approveAndDeposit(dummyCoin, sdpAdmin2, collateralCustody, sdpAdmin2Deposit)

            await identity.updateRole(consensusRole, consensusMock.address)
        })

        it("sdpAdmin2 admin wallet receives tokens upon withdraw", async () => {
            let sdpAdmin2BalanceBefore = await dummyCoin.balanceOf(sdpAdmin2Address)
            await sdpAdmin2Custody.withdraw(sdpAdmin2Deposit / 2, dummyCoin.address)
            let sdpAdmin2BalanceAfter = await dummyCoin.balanceOf(sdpAdmin2Address)
            await expect(sdpAdmin2BalanceAfter.sub(sdpAdmin2BalanceBefore)).to.be.equal(sdpAdmin2Deposit / 2)
        })

        it("sdpAdmin2 admin collateral balance in custody account decrements upon withdraw", async () => {
            let sdpAdmin2BalanceBefore = await collateralCustody.collateralBalances(
                sdpAdmin2Address,
                dummyCoin.address
            )
            await sdpAdmin2Custody.withdraw(sdpAdmin2Deposit / 2, dummyCoin.address)
            let sdpAdmin2BalanceAfter = await collateralCustody.collateralBalances(
                sdpAdmin2Address,
                dummyCoin.address
            )
            expect(sdpAdmin2BalanceBefore.sub(sdpAdmin2BalanceAfter)).to.be.equal(sdpAdmin2Deposit / 2)
        })

        it("Asset Custody Contract token balance decrements upon withdrawal from different addresses", async () => {
            let assetCustodyBalanceBefore = await dummyCoin.balanceOf(collateralCustody.address)
            await sdpAdmin2Custody.withdraw(sdpAdmin2Deposit / 2, dummyCoin.address)
            let assetCustodyBalanceAfter = await dummyCoin.balanceOf(collateralCustody.address)
            expect(assetCustodyBalanceBefore.sub(assetCustodyBalanceAfter)).to.be.equal(sdpAdmin2Deposit / 2)

            await sdpAdmin1Custody.withdraw(sdpAdmin1Deposit / 2, dummyCoin.address)
            assetCustodyBalanceAfter = await dummyCoin.balanceOf(collateralCustody.address)
            expect(assetCustodyBalanceBefore.sub(assetCustodyBalanceAfter)).to.be.equal(totalDeposits / 2)
        })

        it("sdpAdmin2 admin withdrawal does not effect sdpAdmin2 admin's balance", async () => {
            let sdpAdmin1BalanceBefore = await collateralCustody.collateralBalances(
                sdpAdmin1Address,
                dummyCoin.address
            )
            await sdpAdmin2Custody.withdraw(sdpAdmin2Deposit / 2, dummyCoin.address)
            let sdpAdmin1BalanceAfter = await collateralCustody.collateralBalances(
                sdpAdmin1Address,
                dummyCoin.address
            )
            await expect(sdpAdmin1BalanceBefore).to.be.eq(sdpAdmin1BalanceAfter)
        })

        it("sdpAdmin2 admin cannot withdraw more than their total collateral balance, failed withdrawal does not alter contract", async () => {
            await sdpAdmin2Custody.withdraw(sdpAdmin2Deposit, dummyCoin.address)
            await expect(sdpAdmin2Custody.withdraw(1, dummyCoin.address)).to.be.revertedWith(
                "INSUFFICIENT_BALANCE"
            )
            expect(await dummyCoin.balanceOf(collateralCustody.address)).to.be.equal(sdpAdmin1Deposit)
            expect(
                await collateralCustody.collateralBalances(sdpAdmin2Address, dummyCoin.address)
            ).to.be.equal(0)
        })

        it("sdpAdmin2 admin cannot withdraw more than their unlocked balance, failed withdrawal does not alter contract", async () => {
            // set operator admin relationship
            await operatorInit(sdpAdmin2, sdpOperator2, collateralCustody)
            // lock tokens
            await consensusMock.callLockCollateral(
                sdpOperator2Address,
                dummyCoin.address,
                sdpAdmin2Deposit / 2,
                collateralCustody.address
            )
            await sdpAdmin2Custody.withdraw(sdpAdmin2Deposit / 2, dummyCoin.address)
            await expect(sdpAdmin2Custody.withdraw(1, dummyCoin.address)).to.be.revertedWith(
                "INSUFFICIENT_BALANCE"
            )
            expect(await dummyCoin.balanceOf(collateralCustody.address)).to.be.equal(
                totalDeposits - sdpAdmin2Deposit / 2
            )
            expect(
                await collateralCustody.collateralBalances(sdpAdmin2Address, dummyCoin.address)
            ).to.be.equal(sdpAdmin2Deposit / 2)
            expect(await collateralCustody.lockedBalances(sdpAdmin2Address, dummyCoin.address)).to.be.equal(
                sdpAdmin2Deposit / 2
            )
        })

        it("ERC20 token withdrawal emits event: CollateralWithdraw(sdp admin address, token amount, token address)", async () => {
            await expect(sdpAdmin2Custody.withdraw(sdpAdmin2Deposit, dummyCoin.address))
                .to.emit(collateralCustody, "CollateralWithdraw")
                .withArgs(sdpAdmin2Address, sdpAdmin2Deposit, dummyCoin.address)
        })
    })

    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------

    describe("testing native token withdraw", async () => {
        /**
         * withdraw() function in asset custody contract tested
         * from a starting point of funded accounts sdpAdmin2 admin and sdpAdmin2 admin
         * function is executed and the correct execution of several operations is verified
         *
         * withdraw allows the withdrawal of native tokens and ERC20 tokens
         * Native token withdrawal is being tested here
         */

        beforeEach(async () => {
            //load up custody accounts
            await sdpAdmin2Custody.deposit({value: sdpAdmin2DepositEth})
            await sdpAdmin1Custody.deposit({value: sdpAdmin1DepositEth})

            await identity.updateRole(consensusRole, consensusMock.address)
        })

        it("sdpAdmin2 admin wallet receives tokens upon withdraw", async () => {
            let sdpAdmin2BalanceBefore = await ethers.provider.getBalance(sdpAdmin2Address)
            // save tx to get txReceipt. gas fees will need to be calculated for test
            const tx = await sdpAdmin2Custody.withdraw(sdpAdmin2DepositEth.div(2), _za)
            await ethers.provider.waitForTransaction(tx.hash)
            const txReceipt = await ethers.provider.getTransactionReceipt(tx.hash)

            let sdpAdmin2BalanceAfter = await ethers.provider.getBalance(sdpAdmin2Address)
            await expect(sdpAdmin2BalanceAfter.sub(sdpAdmin2BalanceBefore)).to.be.equal(
                sdpAdmin2DepositEth.div(2).sub(txReceipt.gasUsed.mul(txReceipt.effectiveGasPrice))
            )
        })

        it("sdpAdmin2 admin collateral balance in custody account decrements upon withdraw", async () => {
            let sdpAdmin2BalanceBefore = await collateralCustody.collateralBalances(sdpAdmin2Address, _za)
            await sdpAdmin2Custody.withdraw(sdpAdmin2DepositEth.div(2), _za)
            let sdpAdmin2BalanceAfter = await collateralCustody.collateralBalances(sdpAdmin2Address, _za)
            expect(sdpAdmin2BalanceBefore.sub(sdpAdmin2BalanceAfter)).to.be.equal(sdpAdmin2DepositEth.div(2))
        })

        it("Asset Custody Contract token balance decrements upon withdrawal from different addresses", async () => {
            let assetCustodyBalanceBefore = await ethers.provider.getBalance(collateralCustody.address)
            await sdpAdmin2Custody.withdraw(sdpAdmin2DepositEth.div(2), _za)
            let assetCustodyBalanceAfter = await ethers.provider.getBalance(collateralCustody.address)
            expect(assetCustodyBalanceBefore.sub(assetCustodyBalanceAfter)).to.be.equal(
                sdpAdmin2DepositEth.div(2)
            )

            await sdpAdmin1Custody.withdraw(sdpAdmin1DepositEth.div(2), _za)
            assetCustodyBalanceAfter = await ethers.provider.getBalance(collateralCustody.address)
            expect(assetCustodyBalanceBefore.sub(assetCustodyBalanceAfter)).to.be.equal(
                totalDepositsEth.div(2)
            )
        })

        it("sdpAdmin2 admin withdrawal does not effect sdpAdmin2 admin's balance", async () => {
            let sdpAdmin1BalanceBefore = await collateralCustody.collateralBalances(sdpAdmin1Address, _za)
            await sdpAdmin2Custody.withdraw(sdpAdmin2DepositEth.div(2), _za)
            let sdpAdmin1BalanceAfter = await collateralCustody.collateralBalances(sdpAdmin1Address, _za)
            await expect(sdpAdmin1BalanceBefore).to.be.eq(sdpAdmin1BalanceAfter)
        })

        it("sdpAdmin2 admin cannot withdraw more than their total collateral balance, failed withdrawal does not alter contract", async () => {
            await sdpAdmin2Custody.withdraw(sdpAdmin2DepositEth, _za)
            await expect(sdpAdmin2Custody.withdraw(1, _za)).to.be.revertedWith("INSUFFICIENT_BALANCE")
            expect(await ethers.provider.getBalance(collateralCustody.address)).to.be.equal(
                sdpAdmin1DepositEth
            )
            expect(await collateralCustody.collateralBalances(sdpAdmin2Address, _za)).to.be.equal(0)
        })

        it("sdpAdmin2 admin cannot withdraw more than their unlocked balance, failed withdrawal does not alter contract", async () => {
            // set admin operator relationship
            await operatorInit(sdpAdmin2, sdpOperator2, collateralCustody)
            await consensusMock.callLockCollateral(
                sdpOperator2Address,
                _za,
                sdpAdmin2DepositEth.div(2),
                collateralCustody.address
            )
            await sdpAdmin2Custody.withdraw(sdpAdmin2DepositEth.div(2), _za)
            await expect(sdpAdmin2Custody.withdraw(1, _za)).to.be.revertedWith("INSUFFICIENT_BALANCE")
            expect(await ethers.provider.getBalance(collateralCustody.address)).to.be.equal(
                totalDepositsEth.sub(sdpAdmin2DepositEth.div(2))
            )
            expect(await collateralCustody.collateralBalances(sdpAdmin2Address, _za)).to.be.equal(
                sdpAdmin2DepositEth.div(2)
            )
            expect(await collateralCustody.lockedBalances(sdpAdmin2Address, _za)).to.be.equal(
                sdpAdmin2DepositEth.div(2)
            )
        })

        it("Native token withdrawal emits event: CollateralWithdraw(user address, token amount, token address)", async () => {
            await expect(sdpAdmin2Custody.withdraw(sdpAdmin2DepositEth, _za))
                .to.emit(collateralCustody, "CollateralWithdraw")
                .withArgs(sdpAdmin2Address, sdpAdmin2DepositEth, _za)
        })
    })

    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------

    describe("Test Token Unlocking", async () => {
        beforeEach(async () => {
            //The aim of this long beforeEach block is to start the accounts in a state of having locked tokens

            // approve sdp operators
            await operatorInit(sdpAdmin1, sdpOperator1, collateralCustody)
            await operatorInit(sdpAdmin2, sdpOperator2, collateralCustody)
            await operatorInit(sdpAdmin3, sdpOperator3, collateralCustody)

            //load up collateral accounts with native token
            await sdpAdmin1Custody.deposit({value: sdpAdmin1DepositEth})
            await sdpAdmin2Custody.deposit({value: sdpAdmin2DepositEth})

            expect(await ethers.provider.getBalance(collateralCustody.address)).to.equal(
                sdpAdmin1DepositEth.add(sdpAdmin2DepositEth)
            )

            // load up collateral accounts with dummyCoin
            await approveAndDeposit(dummyCoin, sdpAdmin1, collateralCustody, sdpAdmin1Deposit)
            await approveAndDeposit(dummyCoin, sdpAdmin2, collateralCustody, sdpAdmin2Deposit)

            await expect(await dummyCoin.balanceOf(collateralCustody.address)).to.be.eq(
                sdpAdmin2Deposit + sdpAdmin1Deposit
            )
            //set consenusMock with ROLE_CONSENSUS so it can call token lock in collateral custody contract
            await identity.updateRole(consensusRole, consensusMock.address)

            //lock tokens
            await consensusMock.callLockCollateral(
                sdpOperator1Address,
                dummyCoin.address,
                sdpAdmin1Deposit,
                collateralCustody.address
            )
            await consensusMock.callLockCollateral(
                sdpOperator2Address,
                dummyCoin.address,
                sdpAdmin2Deposit,
                collateralCustody.address
            )
            await consensusMock.callLockCollateral(
                sdpOperator1Address,
                _za,
                sdpAdmin1DepositEth,
                collateralCustody.address
            )
            await consensusMock
                .connect(bob)
                .callLockCollateral(sdpOperator2Address, _za, sdpAdmin2DepositEth, collateralCustody.address)
            // dummyCoin lockedBalances verify
            expect(await collateralCustody.lockedBalances(sdpAdmin1Address, dummyCoin.address)).to.be.eq(
                sdpAdmin1Deposit
            )
            expect(await collateralCustody.lockedBalances(sdpAdmin2Address, dummyCoin.address)).to.be.eq(
                sdpAdmin2Deposit
            )
            // native token lockedBalances verify
            expect(await collateralCustody.lockedBalances(sdpAdmin1Address, _za)).to.be.eq(
                sdpAdmin1DepositEth
            )
            expect(await collateralCustody.lockedBalances(sdpAdmin2Address, _za)).to.be.eq(
                sdpAdmin2DepositEth
            )
        })

        it("Allows Consensus to unlock tokens", async () => {
            await expect(
                consensusMock.callUnlockCollateral(
                    sdpOperator1Address,
                    dummyCoin.address,
                    sdpAdmin1Deposit,
                    collateralCustody.address
                )
            ).to.not.be.reverted
        })

        it("lockedBalances mapping in CollateralCustody updated by unlockCollateral()", async () => {
            //test locking dummycoin
            await consensusMock.callUnlockCollateral(
                sdpOperator1Address,
                dummyCoin.address,
                sdpAdmin1Deposit,
                collateralCustody.address
            )
            await consensusMock.callUnlockCollateral(
                sdpOperator2Address,
                dummyCoin.address,
                sdpAdmin2Deposit - 50,
                collateralCustody.address
            )
            expect(await collateralCustody.lockedBalances(sdpAdmin2Address, dummyCoin.address)).to.be.eq(50)
            await consensusMock.callUnlockCollateral(
                sdpOperator2Address,
                dummyCoin.address,
                50,
                collateralCustody.address
            )
            // dummyCoin lockedBalances checks
            expect(await collateralCustody.lockedBalances(sdpAdmin1Address, dummyCoin.address)).to.be.eq(0)
            expect(await collateralCustody.lockedBalances(sdpAdmin2Address, dummyCoin.address)).to.be.eq(0)

            //test locking native token
            await consensusMock.callUnlockCollateral(
                sdpOperator1Address,
                _za,
                sdpAdmin1DepositEth,
                collateralCustody.address
            )
            await consensusMock.callUnlockCollateral(
                sdpOperator2Address,
                _za,
                sdpAdmin2DepositEth.sub(50),
                collateralCustody.address
            )
            expect(await collateralCustody.lockedBalances(sdpAdmin2Address, _za)).to.be.eq(50)
            await consensusMock.callUnlockCollateral(sdpOperator2Address, _za, 50, collateralCustody.address)
            // native token lockedBalances checks
            expect(await collateralCustody.lockedBalances(sdpAdmin1Address, _za)).to.be.eq(0)
            expect(await collateralCustody.lockedBalances(sdpAdmin2Address, _za)).to.be.eq(0)
        })
        it("dissallows non Consensus from unlocking tokens", async () => {
            await expect(
                collateralCustody
                    .connect(sdpAdmin1)
                    .unlockCollateral(sdpAdmin1Address, dummyCoin.address, sdpAdmin1Deposit)
            ).to.be.revertedWith("SENDER_NOT_CONSENSUS")
        })
        it("Token Unlock Fails if not enough Locked tokens", async () => {
            await expect(
                consensusMock.callUnlockCollateral(
                    sdpOperator1Address,
                    dummyCoin.address,
                    sdpAdmin1Deposit + 1,
                    collateralCustody.address
                )
            ).to.be.revertedWith("INSUFFICIENT_LOCKED_TOKENS")
            await consensusMock.callUnlockCollateral(
                sdpOperator1Address,
                dummyCoin.address,
                sdpAdmin1Deposit,
                collateralCustody.address
            )
            await expect(
                consensusMock.callUnlockCollateral(
                    sdpOperator1Address,
                    dummyCoin.address,
                    1,
                    collateralCustody.address
                )
            ).to.be.revertedWith("INSUFFICIENT_LOCKED_TOKENS")
        })
    })

    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------

    describe("Testing SDP Operator approval", async () => {
        beforeEach(async () => {
            // sdpOperator1 is being used as sdp operator, sdpAdmin1 is being used as sdp admin

            // byte array of sdpAdmin1(admin) address to be signed by sdpOperator1(operator)
            sdpAdmin1AddressBytes = ethers.utils.arrayify(sdpAdmin1Address)

            signedMessage = await sdpOperator1.signMessage(sdpAdmin1AddressBytes)
            signature = signedMessage.substring(2)
            r = "0x" + signature.substring(0, 64)
            s = "0x" + signature.substring(64, 128)
            v = parseInt(signature.substring(128, 130), 16)
            // sig.r = r
            // sig.s = s
            // sig.v = v
            sig = {v, r, s}
        })

        it("SDP Admin can call approveOperator()", async () => {
            await expect(collateralCustody.connect(sdpAdmin1).approveOperator(sdpOperator1Address, sig)).to
                .not.be.reverted
        })

        it("Successful Function call results in setting SDP Admin as admin for operator in mapping", async () => {
            await sdpAdmin1Custody.approveOperator(sdpOperator1Address, sig)
            expect(await collateralCustody.callStatic.adminFor(sdpOperator1Address)).to.be.eq(
                sdpAdmin1Address
            )
        })

        it("Successful Function call does not add an entry under sdpAdmin1Address location of mapping", async () => {
            await sdpAdmin1Custody.approveOperator(sdpOperator1Address, sig)
            expect(await collateralCustody.callStatic.adminFor(sdpAdmin1Address)).to.be.eq(_za)
        })

        it("Non SDP operator calling approveOperator() will fail", async () => {
            await expect(
                collateralCustody.connect(bob).approveOperator(sdpOperator1Address, sig)
            ).to.be.revertedWith("NOT_SDP_ADMIN")
        })

        it("Function call will fail if wrong operator address reported in function call", async () => {
            await expect(
                collateralCustody.connect(sdpAdmin1).approveOperator(bobAddress, sig)
            ).to.be.revertedWith("SIGNATURE_VERIF_FAILED_OPERATOR_OR_ADMIN")
        })

        it("Function call will fail if operator signature does not match Admin address in function call", async () => {
            const bobAddressBytes = ethers.utils.arrayify(bobAddress)

            //sign bob's address with sdpOperator1 private key
            const signedMessage2 = await sdpOperator1.signMessage(bobAddressBytes)
            const signature2 = signedMessage2.substring(2)
            const r2 = "0x" + signature2.substring(0, 64)
            const s2 = "0x" + signature2.substring(64, 128)
            const v2 = parseInt(signature2.substring(128, 130), 16)
            const sig2: Signature = {v: v2, r: r2, s: s2}
            // submit bob's signed address
            await expect(sdpAdmin1Custody.approveOperator(sdpOperator1Address, sig2)).to.be.revertedWith(
                "SIGNATURE_VERIF_FAILED_OPERATOR_OR_ADMIN"
            )
            // verify that bob's signed address, with bob's address in submitted operator (signature still from sdpOperator1)
            await expect(sdpAdmin1Custody.approveOperator(bobAddress, sig2)).to.be.revertedWith(
                "SIGNATURE_VERIF_FAILED_OPERATOR_OR_ADMIN"
            )
        })

        it("Function call will fail if operator signature does not match Operator address in function call", async () => {
            // sign sdpAdmin1 address with bob's private key instead of sdpOperator1's private key
            const signedMessage2 = await bob.signMessage(sdpAdmin1AddressBytes)
            const signature2 = signedMessage2.substring(2)
            const r2 = "0x" + signature2.substring(0, 64)
            const s2 = "0x" + signature2.substring(64, 128)
            const v2 = parseInt(signature2.substring(128, 130), 16)
            const sig2: Signature = {v: v2, r: r2, s: s2}
            // submit sdpAdmin1's addres signed by bob
            await expect(sdpAdmin1Custody.approveOperator(sdpOperator1Address, sig2)).to.be.revertedWith(
                "SIGNATURE_VERIF_FAILED_OPERATOR_OR_ADMIN"
            )
            // verify that bob signing in test case worked properly and will pass if bobAddress submitted
            // this is to verify that the above test case failed for the correct reason
            await expect(sdpAdmin1Custody.approveOperator(bobAddress, sig2)).to.not.be.reverted
        })

        it("Successful Function call emits event", async () => {
            await expect(sdpAdmin1Custody.approveOperator(sdpOperator1Address, sig))
                .to.emit(collateralCustody, "OperatorApproved")
                .withArgs(sdpOperator1Address, sdpAdmin1Address)
        })
    })

    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------

    describe("Testing SDP Operator revokation", async () => {
        beforeEach(async () => {
            // Set up sdp operator and admin pairs for testing
            await operatorInit(sdpAdmin1, sdpOperator1, collateralCustody)
            await operatorInit(sdpAdmin2, sdpOperator2, collateralCustody)
            // verify setup
            expect(await collateralCustody.callStatic.adminFor(sdpOperator1Address)).to.be.eq(
                sdpAdmin1Address
            )
            expect(await collateralCustody.callStatic.adminFor(sdpOperator2Address)).to.be.eq(
                sdpAdmin2Address
            )
        })

        it("SDP Admin can call revokeOperator()", async () => {
            await expect(collateralCustody.connect(sdpAdmin1).revokeOperator(sdpOperator1Address)).to.not.be
                .reverted
        })

        it("Non SDP Admin calling revokeOperator() will fail", async () => {
            await expect(
                collateralCustody.connect(bob).revokeOperator(sdpOperator1Address)
            ).to.be.revertedWith("NOT_SDP_ADMIN")
        })

        it("Successful Function call results in clearing admins mapping entry for operator", async () => {
            await sdpAdmin1Custody.revokeOperator(sdpOperator1Address)
            expect(await collateralCustody.callStatic.adminFor(sdpOperator1Address)).to.be.eq(_za)
        })

        it("Admin attempting to clear status on another Admin's operator will fail", async () => {
            await expect(
                collateralCustody.connect(sdpAdmin1).revokeOperator(sdpOperator2Address)
            ).to.be.revertedWith("ADMIN_NOT_OPERATOR_OWNER")
            await expect(
                collateralCustody.connect(sdpAdmin2).revokeOperator(sdpOperator1Address)
            ).to.be.revertedWith("ADMIN_NOT_OPERATOR_OWNER")
        })

        it("Successful Function call emits event", async () => {
            await expect(sdpAdmin1Custody.revokeOperator(sdpOperator1Address))
                .to.emit(collateralCustody, "OperatorRevoked")
                .withArgs(sdpOperator1Address, sdpAdmin1Address)
        })
    })

    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------

    describe("Testing Slash Collateral For User", async () => {
        //Note: this version of the function slashCollateralForUser() is for slashing collateral of users
        // and auditors who do not have an operator admin relationship
        beforeEach(async () => {
            // give tokens

            // lock collateral
            //The aim of this long beforeEach block is to start the accounts in a state of having locked tokens

            // approve sdp operators
            await operatorInit(sdpAdmin1, sdpOperator1, collateralCustody)

            await sdpAdmin1Custody.deposit({value: sdpAdmin1DepositEth})

            await approveAndDeposit(protocolToken, sdpAdmin1, collateralCustody, sdpAdmin1Deposit)

            await expect(await protocolToken.balanceOf(collateralCustody.address)).to.be.eq(sdpAdmin1Deposit)
            //set consenusMock with ROLE_CONSENSUS so it can call token lock in collateral custody contract
            await identity.updateRole(consensusRole, consensusMock.address)

            //lock tokens
            await consensusMock.callLockCollateral(
                sdpOperator1Address,
                protocolToken.address,
                sdpAdmin1Deposit,
                collateralCustody.address
            )
            await consensusMock.callLockCollateral(
                sdpOperator1Address,
                _za,
                sdpAdmin1DepositEth,
                collateralCustody.address
            )
            // dummyCoin lockedBalances verify
            expect(await collateralCustody.lockedBalances(sdpAdmin1Address, protocolToken.address)).to.be.eq(
                sdpAdmin1Deposit
            )
            // native token lockedBalances verify
            expect(await collateralCustody.lockedBalances(sdpAdmin1Address, _za)).to.be.eq(
                sdpAdmin1DepositEth
            )
        })
        it("Consensus Can Call slashCollateralForUser", async () => {
            await expect(
                consensusMock.callSlashCollateralForUser(
                    sdpOperator1Address,
                    bobAddress,
                    _za,
                    sdpAdmin1DepositEth,
                    collateralAddress
                )
            ).to.not.be.reverted
        })
        it("Non Consensus Cannot Call slashCollateralForUser", async () => {
            await expect(
                collateralCustody
                    .connect(bob)
                    .slashCollateralForUser(sdpAdmin1Address, bobAddress, _za, sdpAdmin1DepositEth)
            ).to.be.revertedWith("SENDER_NOT_CONSENSUS")
        })
        it("Cannot Slash more tokens then are locked in sdp admin account", async () => {
            await sdpAdmin1Custody.deposit({value: 1})
            await expect(
                consensusMock.callSlashCollateralForUser(
                    sdpAdmin1Address,
                    bobAddress,
                    _za,
                    sdpAdmin1DepositEth.add(1),
                    collateralAddress
                )
            ).to.be.revertedWith("INSUFFICIENT_LOCKED_TOKENS")
        })
        describe("Tests outcomes of slashed collateral", async () => {
            beforeEach(async () => {
                sdpLockedBefore = await collateralCustody.callStatic.lockedBalances(
                    sdpAdmin1Address,
                    protocolToken.address
                )
                bobLockedBefore = await collateralCustody.callStatic.lockedBalances(
                    bobAddress,
                    protocolToken.address
                )
                sdpBalanceBefore = await collateralCustody.callStatic.collateralBalances(
                    sdpAdmin1Address,
                    protocolToken.address
                )
                bobBalanceBefore = await collateralCustody.callStatic.collateralBalances(
                    bobAddress,
                    protocolToken.address
                )
                await expect(
                    consensusMock.callSlashCollateralForUser(
                        sdpOperator1Address,
                        bobAddress,
                        protocolToken.address,
                        sdpAdmin1Deposit,
                        collateralAddress
                    )
                ).to.not.be.reverted
                sdpLockedAfter = await collateralCustody.callStatic.lockedBalances(
                    sdpAdmin1Address,
                    protocolToken.address
                )
                bobLockedAfter = await collateralCustody.callStatic.lockedBalances(
                    bobAddress,
                    protocolToken.address
                )
                sdpBalanceAfter = await collateralCustody.callStatic.collateralBalances(
                    sdpAdmin1Address,
                    protocolToken.address
                )
                bobBalanceAfter = await collateralCustody.callStatic.collateralBalances(
                    bobAddress,
                    protocolToken.address
                )
            })
            it("deliverer locked balance goes down", async () => {
                expect(sdpLockedBefore.sub(sdpLockedAfter)).to.be.eq(sdpAdmin1Deposit)
            })
            it("recipient locked balance unchanged", async () => {
                expect(bobLockedAfter.sub(bobLockedBefore)).to.be.eq(0)
            })
            it("deliverer loses collateral", async () => {
                expect(sdpBalanceBefore.sub(sdpBalanceAfter)).to.be.eq(sdpAdmin1Deposit)
            })
            it("recipient gains collateral", async () => {
                expect(bobBalanceAfter.sub(bobBalanceBefore)).to.be.eq(sdpAdmin1Deposit)
            })
            it("recipient can withdraw slashed tokens after", async () => {
                let bobProtocolTokenBefore = await protocolToken.balanceOf(bobAddress)
                await expect(collateralCustody.connect(bob).withdraw(sdpAdmin1Deposit, protocolToken.address))
                    .to.not.be.reverted
                let bobProtocolTokenAfter = await protocolToken.balanceOf(bobAddress)
                expect(bobProtocolTokenAfter.sub(bobProtocolTokenBefore)).to.be.eq(sdpAdmin1Deposit)
            })
        })
    })

    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------

    describe("Testing Slash Collateral", async () => {
        beforeEach(async () => {
            // give tokens

            // lock collateral
            //The aim of this long beforeEach block is to start the accounts in a state of having locked tokens

            // approve sdp operators
            await operatorInit(sdpAdmin1, sdpOperator1, collateralCustody)
            await operatorInit(sdpAdmin2, sdpOperator2, collateralCustody)

            await sdpAdmin1Custody.deposit({value: sdpAdmin1DepositEth})

            await approveAndDeposit(protocolToken, sdpAdmin1, collateralCustody, sdpAdmin1Deposit)

            await expect(await protocolToken.balanceOf(collateralCustody.address)).to.be.eq(sdpAdmin1Deposit)
            //set consenusMock with ROLE_CONSENSUS so it can call token lock in collateral custody contract
            await identity.updateRole(consensusRole, consensusMock.address)

            //lock tokens
            await consensusMock.callLockCollateral(
                sdpOperator1Address,
                protocolToken.address,
                sdpAdmin1Deposit,
                collateralCustody.address
            )
            await consensusMock.callLockCollateral(
                sdpOperator1Address,
                _za,
                sdpAdmin1DepositEth,
                collateralCustody.address
            )
            // dummyCoin lockedBalances verify
            expect(await collateralCustody.lockedBalances(sdpAdmin1Address, protocolToken.address)).to.be.eq(
                sdpAdmin1Deposit
            )
            // native token lockedBalances verify
            expect(await collateralCustody.lockedBalances(sdpAdmin1Address, _za)).to.be.eq(
                sdpAdmin1DepositEth
            )
        })
        it("Consensus Can Call slashCollateral", async () => {
            await expect(
                consensusMock.callSlashCollateral(
                    sdpOperator1Address,
                    sdpOperator2Address,
                    _za,
                    sdpAdmin1DepositEth,
                    collateralAddress
                )
            ).to.not.be.reverted
        })
        it("Non Consensus Cannot Call slashCollateral", async () => {
            await expect(
                collateralCustody
                    .connect(sdpOperator2)
                    .slashCollateral(sdpAdmin1Address, sdpOperator2Address, _za, sdpAdmin1DepositEth)
            ).to.be.revertedWith("SENDER_NOT_CONSENSUS")
        })
        it("Cannot Slash more tokens then are locked in sdp admin account", async () => {
            await sdpAdmin1Custody.deposit({value: 1})
            await expect(
                consensusMock.callSlashCollateral(
                    sdpAdmin1Address,
                    sdpOperator2Address,
                    _za,
                    sdpAdmin1DepositEth.add(1),
                    collateralAddress
                )
            ).to.be.revertedWith("INSUFFICIENT_LOCKED_TOKENS")
        })
        describe("Tests outcomes of slashed collateral", async () => {
            beforeEach(async () => {
                sdpLockedBefore = await collateralCustody.callStatic.lockedBalances(
                    sdpAdmin1Address,
                    protocolToken.address
                )
                sdp2LockedBefore = await collateralCustody.callStatic.lockedBalances(
                    sdpAdmin2Address,
                    protocolToken.address
                )
                sdpBalanceBefore = await collateralCustody.callStatic.collateralBalances(
                    sdpAdmin1Address,
                    protocolToken.address
                )
                sdp2BalanceBefore = await collateralCustody.callStatic.collateralBalances(
                    sdpAdmin2Address,
                    protocolToken.address
                )
                await expect(
                    consensusMock.callSlashCollateral(
                        sdpOperator1Address,
                        sdpOperator2Address,
                        protocolToken.address,
                        sdpAdmin1Deposit,
                        collateralAddress
                    )
                ).to.not.be.reverted
                sdpLockedAfter = await collateralCustody.callStatic.lockedBalances(
                    sdpAdmin1Address,
                    protocolToken.address
                )
                sdp2LockedAfter = await collateralCustody.callStatic.lockedBalances(
                    sdpAdmin2Address,
                    protocolToken.address
                )
                sdpBalanceAfter = await collateralCustody.callStatic.collateralBalances(
                    sdpAdmin1Address,
                    protocolToken.address
                )
                sdp2BalanceAfter = await collateralCustody.callStatic.collateralBalances(
                    sdpAdmin2Address,
                    protocolToken.address
                )
            })
            it("deliverer locked balance goes down", async () => {
                expect(sdpLockedBefore.sub(sdpLockedAfter)).to.be.eq(sdpAdmin1Deposit)
            })
            it("recipient locked balance unchanged", async () => {
                expect(sdp2LockedAfter.sub(sdp2LockedBefore)).to.be.eq(0)
            })
            it("deliverer loses collateral", async () => {
                expect(sdpBalanceBefore.sub(sdpBalanceAfter)).to.be.eq(sdpAdmin1Deposit)
            })
            it("recipient gains collateral", async () => {
                expect(sdp2BalanceAfter.sub(sdp2BalanceBefore)).to.be.eq(sdpAdmin1Deposit)
            })
            it("recipient can withdraw slashed tokens after", async () => {
                let sdpOperator2ProtocolTokenBefore = await protocolToken.balanceOf(sdpAdmin2Address)
                await expect(
                    collateralCustody.connect(sdpAdmin2).withdraw(sdpAdmin1Deposit, protocolToken.address)
                ).to.not.be.reverted
                let sdpOperator2ProtocolTokenAfter = await protocolToken.balanceOf(sdpAdmin2Address)
                expect(sdpOperator2ProtocolTokenAfter.sub(sdpOperator2ProtocolTokenBefore)).to.be.eq(
                    sdpAdmin1Deposit
                )
            })
        })
    })
})
