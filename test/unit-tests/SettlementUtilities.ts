// Copyright © 2022 TXA PTE. LTD.
import {expect} from "chai"
import {ethers, getNamedAccounts, getUnnamedAccounts} from "hardhat"
import {Address} from "hardhat-deploy/dist/types"
import {Signer, BigNumber} from "ethers"

import {MerkleTree} from "merkletreejs"
import {
    copyArray,
    deploy,
    getSigner,
    IdentityRegistry,
    Obligation,
    CollateralTransfer,
    TransferObligation,
    ExchangeReport,
    DummyCoin,
    SettlementCoordination,
    SettlementDataConsensus,
    SettlementUtilities,
    UtilsMock,
    ConsensusMock,
    AssetMock,
    CollateralMock,
    CoordinationMock,
    setupTest,
    _za,
    approveAndDeposit,
    operatorInit,
    merkleFromObs,
    range,
    merkleFromArray,
    hashACK,
    hashTrade,
    getDeployed,
    fullMerkleTree,
    Leaf,
    Trade,
    tradeAbi,
    ACK,
    ackAbi,
} from "../utils"

describe("SettlementUtilities", function () {
    let settlementUtilities: SettlementUtilities
    let utilsMock: UtilsMock
    let coordination: CoordinationMock
    let assetCustodyMock: AssetMock
    let collateralCustodyMock: CollateralMock
    let identity: IdentityRegistry
    let noRole: Address
    let chainID: number

    let alice: Signer
    let bob: Signer
    let charlie: Signer
    let sdps: Signer[] = []
    let auditor: Signer
    let exchange: Signer

    let sdpAdmin1: Signer
    let sdpAdmin2: Signer
    let sdpAdmin3: Signer
    let sdpAdmin4: Signer

    let aliceAddr: string
    let bobAddr: string
    let charlieAddr: string

    let sdpAdmin1Address: Address
    let sdpAdmin2Address: Address
    let sdpAdmin3Address: Address
    let sdpAdmin4Address: Address
    let coordAddress: Address
    let exchangeAddr: Address

    let auditorAddress: Address

    let dMCAddress: Address

    let unnamedAccounts: Address[]
    let consensus: ConsensusMock
    let dummyCoin: DummyCoin

    let assetCustodyAddress: Address
    let collateralCustodyAddress: Address

    let tradeKey: any
    let tradePrivate: string
    let tradePublic: string
    let tradeSigner: Signer
    let tradeAddress: Address

    let tradeTree: MerkleTree
    let leaves: Leaf[]
    let merkleRoot: string
    let merkleRootSol: string

    const zeroEth = ethers.utils.parseEther("0")
    const ethObligation = ethers.utils.parseEther("1")

    const merkleRootZero = "0x0000000000000000000000000000000000000000000000000000000000000000"
    let fakeMerkleRoot = "0x0000000000000000000000000000000000000000000000000000000000000000"
    let ethersMerkleRoot = "0x0000000000000000000000000000000000000000000000000000000000000000"
    let hashedLeaf: string
    let auditorFraction: number

    let bigObligations: Obligation[]
    let fakeObligations: Obligation[]
    let sig: any
    let ack: ACK
    let ackWrong: ACK
    let bytesACK: string
    let trade: Trade
    let bytesTrade: string

    before(async () => {
        const namedAccounts = await getNamedAccounts()
        unnamedAccounts = await getUnnamedAccounts()
        alice = await getSigner(namedAccounts.alice)
        bob = await getSigner(namedAccounts.bob)
        auditor = await getSigner(namedAccounts.auditor)
        exchange = await getSigner(namedAccounts.exchange)
        exchangeAddr = await exchange.getAddress()
        charlie = await getSigner(namedAccounts.charlie)
        sdps.push(
            await getSigner(namedAccounts.sdpOperator1),
            await getSigner(namedAccounts.sdpOperator2),
            await getSigner(namedAccounts.sdpOperator3)
        )

        sdpAdmin1 = await ethers.provider.getSigner(namedAccounts.sdpAdmin1)
        sdpAdmin2 = await ethers.provider.getSigner(namedAccounts.sdpAdmin2)
        sdpAdmin3 = await ethers.provider.getSigner(namedAccounts.sdpAdmin3)
        sdpAdmin4 = await ethers.provider.getSigner(namedAccounts.sdpAdmin4)

        sdpAdmin1Address = await sdpAdmin1.getAddress()
        sdpAdmin2Address = await sdpAdmin2.getAddress()
        sdpAdmin3Address = await sdpAdmin3.getAddress()
        sdpAdmin4Address = await sdpAdmin4.getAddress()
        auditorAddress = await auditor.getAddress()

        noRole = namedAccounts.noRole
        aliceAddr = await alice.getAddress()
        bobAddr = await bob.getAddress()
        charlieAddr = await charlie.getAddress()

        auditorFraction = 0.1

        tradePrivate = ethers.utils.solidityKeccak256(["address", "address"], [exchangeAddr, exchangeAddr])
        tradeKey = new ethers.utils.SigningKey(tradePrivate)
        tradePublic = tradeKey.publicKey
        tradeSigner = new ethers.Wallet(tradePrivate)
        tradeAddress = await tradeSigner.getAddress()
    })

    beforeEach(async function () {
        let contracts = await setupTest()
        let {chainId} = await ethers.provider.getNetwork()
        settlementUtilities = (await getDeployed("SettlementUtilities")) as SettlementUtilities
        chainID = chainId
        identity = contracts.identity
        coordination = contracts.coordinationMock
        coordAddress = coordination.address
        utilsMock = contracts.utilsMock
        assetCustodyMock = contracts.assetMock
        collateralCustodyMock = contracts.collateralMock
        consensus = contracts.consensusMock

        dummyCoin = contracts.dummyCoin
        dMCAddress = dummyCoin.address

        assetCustodyAddress = assetCustodyMock.address
        collateralCustodyAddress = collateralCustodyMock.address

        await identity.updateRole(await contracts.library.callStatic.ROLE_CONSENSUS(), consensus.address)
        await identity.updateRole(
            await contracts.library.callStatic.ROLE_ASSET_CUSTODY(),
            await assetCustodyAddress
        )
        await identity.updateRole(
            await contracts.library.callStatic.ROLE_COLLATERAL_CUSTODY(),
            await collateralCustodyAddress
        )
        await identity.initializeRole(await contracts.library.callStatic.ROLE_TRADE_SIGNER(), tradeAddress)

        ack = {tradeID: 100, settlementID: 0, chainID: chainID, tokenID: _za}
        ackWrong = {tradeID: 100, settlementID: 0, chainID: chainID, tokenID: _za}

        bigObligations = [
            {
                amount: 1000,
                recipient: aliceAddr,
                deliverer: bobAddr,
                token: dMCAddress,
                reallocate: false,
            },
            {
                amount: 2000,
                recipient: bobAddr,
                deliverer: charlieAddr,
                token: dMCAddress,
                reallocate: false,
            },
            {
                amount: 2000,
                recipient: sdpAdmin1Address,
                deliverer: charlieAddr,
                token: dMCAddress,
                reallocate: false,
            },
            {
                amount: 1000,
                recipient: bobAddr,
                deliverer: aliceAddr,
                token: dMCAddress,
                reallocate: false,
            },
            {
                amount: 2000,
                recipient: charlieAddr,
                deliverer: aliceAddr,
                token: dMCAddress,
                reallocate: false,
            },
            {
                amount: 1000,
                recipient: bobAddr,
                deliverer: aliceAddr,
                token: dMCAddress,
                reallocate: false,
            },
            {
                amount: 2000,
                recipient: bobAddr,
                deliverer: sdpAdmin1Address,
                token: dMCAddress,
                reallocate: false,
            },
            {
                amount: 3000,
                recipient: charlieAddr,
                deliverer: sdpAdmin1Address,
                token: dMCAddress,
                reallocate: false,
            },
            {
                amount: 4000,
                recipient: bobAddr,
                deliverer: sdpAdmin1Address,
                token: dMCAddress,
                reallocate: false,
            },
            {
                amount: 2000,
                recipient: bobAddr,
                deliverer: sdpAdmin1Address,
                token: dMCAddress,
                reallocate: false,
            },
            {
                amount: 1000,
                recipient: charlieAddr,
                deliverer: sdpAdmin1Address,
                token: dMCAddress,
                reallocate: false,
            },
        ]
    })
    it("compares obligations correctly", async () => {
        const testCompare = async (a: Obligation[], b: Obligation[], match: boolean = true) => {
            expect(await utilsMock.callStatic.compareObligations(a, b)).to.be.eq(match)
        }

        const a: Obligation[] = [
            {
                amount: ethObligation,
                recipient: aliceAddr,
                deliverer: bobAddr,
                token: ethers.constants.AddressZero,
                reallocate: false,
            },
            {
                amount: ethObligation,
                recipient: bobAddr,
                deliverer: aliceAddr,
                token: noRole,
                reallocate: false,
            },
        ]
        let b: Obligation[] = copyArray(a)

        await testCompare(a, b)

        b[1].token = ethers.constants.AddressZero

        await testCompare(a, b, false)

        b = copyArray(a)
        b[1].amount = ethers.utils.parseEther("0.99")

        await testCompare(a, b, false)

        b = [a[1], a[0]]

        await testCompare(a, b, false)

        b = copyArray(a)
        b.push({
            amount: ethers.utils.parseEther("0.5"),
            recipient: aliceAddr,
            deliverer: bobAddr,
            token: noRole,
            reallocate: false,
        })

        await testCompare(a, b, false)
    })
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    describe("Check Merkle Root Generation Function is Working", async () => {
        it("Verifies Correct Merkle Root Generated for Arbitrary large sized Leaf Array", async () => {
            const calculateLeafFromObligation = (obligation: Obligation) =>
                ethers.utils.solidityKeccak256(
                    ["uint256", "address", "address", "address", "bool"],
                    [
                        obligation.amount,
                        obligation.deliverer,
                        obligation.recipient,
                        obligation.token,
                        obligation.reallocate,
                    ]
                )

            let leaves = bigObligations.map(calculateLeafFromObligation)
            //test different sizes of merkle tree generation
            for (let ii = 1; ii < leaves.length; ii++) {
                // get merkle roots to compare
                ethersMerkleRoot = await merkleFromObs(bigObligations.slice(0, ii))
                let contractMerkleRoot = await settlementUtilities.callStatic.getMerkleRoot(
                    leaves.slice(0, ii)
                )
                expect(ethersMerkleRoot).to.be.eq(contractMerkleRoot)
                // check that the verify function is working. It accepts obligations arrays and turns them to merkle leaves.
                expect(await consensus.callStatic.verifyMerkleTree(bigObligations.slice(0, ii))).to.be.eq(
                    ethersMerkleRoot
                )
            }
        })
    })

    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------

    describe("Tests the Collateral Slashing Calculator", async () => {
        it("The Collateral Slashing Divider Function Works properly for whole number segments", async () => {
            let senders: CollateralTransfer[]
            senders = [
                {party: bobAddr, amount: 200}, // 200
                {party: aliceAddr, amount: 600}, // 800
                {party: charlieAddr, amount: 400}, // 1200
                {party: sdpAdmin1Address, amount: 300}, // 1500
            ]
            let recipients: CollateralTransfer[]
            recipients = [
                {party: bobAddr, amount: 100}, //100
                {party: aliceAddr, amount: 400}, //500
                {party: charlieAddr, amount: 700}, //1200
                {party: sdpAdmin1Address, amount: 100}, //1300
                {party: sdpAdmin2Address, amount: 200}, //1500
            ]

            let slashObligations: TransferObligation[]
            slashObligations = await utilsMock.callStatic.generateTransferObligations(senders, recipients)

            // the arrays are expected to be padded with zeros because creating and pushing to a dynamic sized array
            // in memory is not possible in solidity
            let expectedTransferAmounts = [100, 100, 300, 300, 400, 100, 200, 0, 0]
            let expectedSenders = [
                bobAddr,
                bobAddr,
                aliceAddr,
                aliceAddr,
                charlieAddr,
                sdpAdmin1Address,
                sdpAdmin1Address,
                _za,
                _za,
            ]
            let expectedRecipients = [
                bobAddr,
                aliceAddr,
                aliceAddr,
                charlieAddr,
                charlieAddr,
                sdpAdmin1Address,
                sdpAdmin2Address,
                _za,
                _za,
            ]
            for (let ii = 0; ii < expectedTransferAmounts.length; ii++) {
                expect(slashObligations[ii].sender).to.be.eq(expectedSenders[ii])
                expect(slashObligations[ii].recipient).to.be.eq(expectedRecipients[ii])
                expect(slashObligations[ii].amount).to.be.eq(expectedTransferAmounts[ii])
            }
        })

        it("The Collateral Slashing Divider Function Works properly for Rounding Errors", async () => {
            let senders: CollateralTransfer[]
            senders = [
                {party: bobAddr, amount: 199},
                {party: aliceAddr, amount: 599},
                {party: charlieAddr, amount: 399},
                {party: sdpAdmin1Address, amount: 299},
            ]
            let recipients: CollateralTransfer[]
            recipients = [
                {party: bobAddr, amount: 99},
                {party: aliceAddr, amount: 399},
                {party: charlieAddr, amount: 699},
                {party: sdpAdmin1Address, amount: 99},
                {party: sdpAdmin2Address, amount: 199},
            ]
            let slashObligations: TransferObligation[]
            slashObligations = await utilsMock.callStatic.generateTransferObligations(senders, recipients)

            // the arrays are expected to be padded with zeros because creating and pushing to a dynamic sized array
            // in memory is not possible in solidity

            let expectedTransferAmounts = [100, 100, 300, 300, 400, 100, 200, 0, 0]
            let expectedSenders = [
                bobAddr,
                bobAddr,
                aliceAddr,
                aliceAddr,
                charlieAddr,
                sdpAdmin1Address,
                sdpAdmin1Address,
                _za,
                _za,
            ]
            let expectedRecipients = [
                bobAddr,
                aliceAddr,
                aliceAddr,
                charlieAddr,
                charlieAddr,
                sdpAdmin1Address,
                sdpAdmin2Address,
                _za,
                _za,
            ]
            for (let ii = 0; ii < expectedTransferAmounts.length; ii++) {
                expect(slashObligations[ii].sender).to.be.eq(expectedSenders[ii])
                expect(slashObligations[ii].recipient).to.be.eq(expectedRecipients[ii])
                expect(slashObligations[ii].amount.toNumber()).to.be.greaterThan(
                    expectedTransferAmounts[ii] - expectedTransferAmounts.length
                )
                expect(slashObligations[ii].amount.toNumber()).to.be.lessThanOrEqual(
                    expectedTransferAmounts[ii]
                )
            }
        })
    })

    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------

    describe("Tests the Obligation Comparing Algorighthm", async () => {
        beforeEach(async () => {
            // fakeObligations = bigObligations.slice(0, bigObligations.length - 1)
        })
        it("Algorithm can account for a missing obligation", async () => {
            fakeObligations = bigObligations.slice(0, bigObligations.length - 1)
            let transferObs: CollateralTransfer[]
            transferObs = await utilsMock.callStatic.compareObligationAmounts(bigObligations, fakeObligations)
            for (let ii = 0; ii < transferObs.length; ii++) {
                if (transferObs[ii].party == bigObligations[bigObligations.length - 1].recipient) {
                    await expect(transferObs[ii].amount).to.be.eq(
                        bigObligations[bigObligations.length - 1].amount
                    )
                } else {
                    await expect(transferObs[ii].amount).to.be.eq(0)
                }
            }
        })

        it("Algorithm can account for multiple overreports and reward deliverer", async () => {
            fakeObligations = []
            bigObligations.forEach(val => fakeObligations.push(Object.assign({}, val)))

            //alice over pays by 2000 in two obs for a total of 4000
            fakeObligations[3].amount = 3000
            fakeObligations[4].amount = 4000
            let transferObs: CollateralTransfer[]
            transferObs = await utilsMock.callStatic.compareObligationAmounts(bigObligations, fakeObligations)

            for (let ii = 0; ii < transferObs.length; ii++) {
                if (transferObs[ii].party == aliceAddr) {
                    await expect(transferObs[ii].amount).to.be.eq(4000)
                } else {
                    await expect(transferObs[ii].amount).to.be.eq(0)
                }
            }
        })

        it("Algorithm can account for multiple underreports and reward recipient", async () => {
            fakeObligations = []
            bigObligations.forEach(val => fakeObligations.push(Object.assign({}, val)))

            //bob under recieved by 1000 in two obs for a total of 2000
            fakeObligations[1].amount = 1000
            fakeObligations[6].amount = 1000
            let transferObs: CollateralTransfer[]
            transferObs = await utilsMock.callStatic.compareObligationAmounts(bigObligations, fakeObligations)
            for (let ii = 0; ii < transferObs.length; ii++) {
                if (transferObs[ii].party == bobAddr) {
                    const expectedAmount = transferObs[ii].party == bobAddr ? 2000 : 0
                    await expect(transferObs[ii].amount).to.be.eq(expectedAmount)
                }
            }
        })

        it("Algorithm can account for many missing obligations and sums up all losses and gains from missreport", async () => {
            // results in multiple underreports and overreports for multiple users

            //This test tests a combination of net gains and losses from underdelivery, overdelivery, underreciept, and overreciept

            fakeObligations = bigObligations.slice(0, 0)

            let transferObs: CollateralTransfer[]
            transferObs = await utilsMock.callStatic.compareObligationAmounts(bigObligations, fakeObligations)

            await expect(transferObs[0].amount).to.be.eq(11000)
            await expect(transferObs[0].party).to.be.eq(bobAddr)
            await expect(transferObs[1].amount).to.be.eq(0)
            await expect(transferObs[1].party).to.be.eq(aliceAddr)
            await expect(transferObs[2].amount).to.be.eq(2000)
            await expect(transferObs[2].party).to.be.eq(charlieAddr)
            await expect(transferObs[3].amount).to.be.eq(0)
            await expect(transferObs[3].party).to.be.eq(sdpAdmin1Address)
        })
    })

    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------

    describe("Creates Merkle Tree", async () => {
        it("Verifies Merkle Proofs for Various Sized Ordered Merkle Trees", async () => {
            const largestTree = 1000

            for (let ii = 0; ii <= largestTree; ii += 200) {
                const tradeRange = range(ii / 10, ii)
                const tradeTree = merkleFromArray(tradeRange)
                const root = tradeTree.getHexRoot()
                for (let jj = 0; jj <= tradeRange.length; jj += 20) {
                    const hashedLeaf = ethers.utils.solidityKeccak256(["uint256"], [tradeRange[jj]])
                    const proof = tradeTree.getProof(hashedLeaf)

                    const rightProofTrue: boolean[] = new Array(proof.length)

                    const proofData = tradeTree.getHexProof(hashedLeaf)
                    for (let kk = 0; kk < proof.length; kk++) {
                        rightProofTrue[kk] = proof[kk].position == "right"
                    }

                    const [verification, leafIndex] = await settlementUtilities.callStatic.verifyOrderedProof(
                        proofData,
                        rightProofTrue,
                        root,
                        hashedLeaf
                    )
                    expect(verification).to.be.true

                    const [solidityRoot, leafIndex2] =
                        await settlementUtilities.callStatic.processOrderedProof(
                            proofData,
                            rightProofTrue,
                            hashedLeaf
                        )
                    expect(solidityRoot).to.be.eq(root)
                }
            }
        })
        it("Does Not Verify Proofs for Trees with Altered Leaves", async () => {
            const largestTree = 1000

            for (let ii = 0; ii <= largestTree; ii += 200) {
                const tradeRange = range(ii / 20, ii)
                const correctTradeTree = merkleFromArray(tradeRange)
                const root = correctTradeTree.getHexRoot()
                //alter leaf
                tradeRange[(ii * 4) / 5] = 9
                const tradeTree = merkleFromArray(tradeRange)
                for (let jj = 0; jj <= tradeRange.length; jj += 10) {
                    const hashedLeaf = ethers.utils.solidityKeccak256(["uint256"], [tradeRange[jj]])
                    const proof = tradeTree.getProof(hashedLeaf)

                    const rightProofTrue: boolean[] = new Array(proof.length)
                    const proofData = tradeTree.getHexProof(hashedLeaf)

                    for (let kk = 0; kk < proof.length; kk++) {
                        rightProofTrue[kk] = proof[kk].position == "right"
                    }
                    const [verification, leafIndex] = await settlementUtilities.callStatic.verifyOrderedProof(
                        proofData,
                        rightProofTrue,
                        root,
                        hashedLeaf
                    )
                    expect(verification).to.be.false

                    const [solidityRoot, leafIndex2] =
                        await settlementUtilities.callStatic.processOrderedProof(
                            proofData,
                            rightProofTrue,
                            hashedLeaf
                        )
                    expect(solidityRoot).to.not.be.eq(root)
                }
            }
        })
        it("Merkle Proof Processing returns correct leaf index", async () => {
            const largestTree = 1000

            for (let ii = 0; ii <= largestTree; ii += 200) {
                const tradeRange = range(0, ii)
                const tradeTree = merkleFromArray(tradeRange)
                const root = tradeTree.getHexRoot()
                for (let jj = 0; jj <= tradeRange.length; jj += 20) {
                    const hashedLeaf = ethers.utils.solidityKeccak256(["uint256"], [tradeRange[jj]])
                    const proof = tradeTree.getProof(hashedLeaf)

                    const rightProofTrue: boolean[] = new Array(proof.length)

                    const proofData = tradeTree.getHexProof(hashedLeaf)
                    for (let kk = 0; kk < proof.length; kk++) {
                        rightProofTrue[kk] = proof[kk].position == "right"
                    }
                    const [verification, leafIndex] = await settlementUtilities.callStatic.verifyOrderedProof(
                        proofData,
                        rightProofTrue,
                        root,
                        hashedLeaf
                    )
                    expect(leafIndex).to.be.eq(tradeRange[jj])

                    const [solidityRoot, leafIndex2] =
                        await settlementUtilities.callStatic.processOrderedProof(
                            proofData,
                            rightProofTrue,
                            hashedLeaf
                        )
                    expect(leafIndex2).to.be.eq(tradeRange[jj])
                }
            }
        })
    })

    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------

    describe("Settlement Utils Hashes Settlement ACK Properly", async () => {
        it("Hashes Settlement ACK", async () => {
            let hashedACKEthers = await hashACK(ack)
            let hashedACKSolidity = await utilsMock.callStatic.hashACK(ack)
            expect(hashedACKEthers).to.be.eq(hashedACKSolidity)
        })
        it("Hash of Settlement ACK is different when any entry is different", async () => {
            ackWrong.tradeID += 1
            let hashedACKEthers = await hashACK(ack)
            let hashedACKSolidity = await utilsMock.callStatic.hashACK(ackWrong)
            expect(hashedACKEthers).to.not.be.eq(hashedACKSolidity)

            ackWrong.tradeID = ack.tradeID
            ackWrong.settlementID += 1
            hashedACKEthers = await hashACK(ack)
            hashedACKSolidity = await utilsMock.callStatic.hashACK(ackWrong)
            expect(hashedACKEthers).to.not.be.eq(hashedACKSolidity)

            ackWrong.settlementID = ack.settlementID
            ackWrong.chainID += 1
            hashedACKEthers = await hashACK(ack)
            hashedACKSolidity = await utilsMock.callStatic.hashACK(ackWrong)
            expect(hashedACKEthers).to.not.be.eq(hashedACKSolidity)

            ackWrong.chainID = ack.chainID
            ackWrong.tokenID = dMCAddress
            hashedACKEthers = await hashACK(ack)
            hashedACKSolidity = await utilsMock.callStatic.hashACK(ackWrong)
            expect(hashedACKEthers).to.not.be.eq(hashedACKSolidity)
        })
    })

    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------

    describe("Signature Verification for 32 bit hashes returns correct Address", async () => {
        beforeEach(async () => {
            //hash arbitrary value for leaf to sign
            hashedLeaf = await ethers.utils.solidityKeccak256(["uint256"], [100])
            sig = await tradeKey.signDigest(hashedLeaf)
        })
        it("returns correct address", async () => {
            let returnSigner = await utilsMock.callStatic.verifySig(hashedLeaf, sig)
            expect(returnSigner).to.be.eq(tradeAddress)
        })
        it("altering any signature value returns the wrong address", async () => {
            let sig2 = sig
            sig2.v = 22
            let returnSigner = await utilsMock.callStatic.verifySig(hashedLeaf, sig2)
            expect(returnSigner).to.not.be.eq(tradeAddress)

            sig2 = sig
            sig2.r = merkleRootZero
            returnSigner = await utilsMock.callStatic.verifySig(hashedLeaf, sig2)
            expect(returnSigner).to.not.be.eq(tradeAddress)

            sig2 = sig
            sig2.s = merkleRootZero
            returnSigner = await utilsMock.callStatic.verifySig(hashedLeaf, sig2)
            expect(returnSigner).to.not.be.eq(tradeAddress)
        })
    })

    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------

    describe("Verify Merkle Root and Node Hashing for Ordered trees", async () => {
        it("Verify getOrderedNode() function for leave sets with a size of an even power of two", async () => {
            let powerOfTwo = 1

            for (let ii = 0; ii < 8; ii++) {
                powerOfTwo = powerOfTwo * 2
                ;[leaves, tradeTree] = fullMerkleTree(tradeKey, powerOfTwo, 1, chainID, _za)
                merkleRoot = tradeTree.getHexRoot()
                merkleRootSol = await settlementUtilities.callStatic.getOrderedNode(leaves.map(x => x.hash))
                expect(merkleRoot).to.be.eq(merkleRootSol)
            }
        })
        it("Verify getOrderedTree() function for various sizes of leaf sets", async () => {
            let size: number
            for (let ii = 2; ii * ii < 400; ii++) {
                size = ii * ii
                ;[leaves, tradeTree] = fullMerkleTree(tradeKey, size, 1, chainID, _za)
                merkleRoot = tradeTree.getHexRoot()
                merkleRootSol = await settlementUtilities.callStatic.getOrderedRoot(leaves.map(x => x.hash))
                expect(merkleRoot).to.be.eq(merkleRootSol)
            }
        })
    })

    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------

    describe("Tests Packing and unpacking ACK and Trade structs", async () => {
        beforeEach(async () => {
            ack.tradeID = 0x324523555
            ack.settlementID = 0x9999
            ack.chainID = 0x87519703597
            ack.tokenID = aliceAddr
            trade = {tradeID: 0x555}
        })
        it("decodeACK returns a decoded ACK", async () => {
            bytesACK = ethers.utils.defaultAbiCoder.encode(ackAbi, [ack])
            let decodedACK = await settlementUtilities.callStatic.decodeACK(bytesACK)
            expect(ack.tradeID).to.be.eq(decodedACK.tradeID)
            expect(ack.settlementID).to.be.eq(decodedACK.settlementID)
            expect(ack.chainID).to.be.eq(decodedACK.chainID)
            expect(ack.tokenID).to.be.eq(decodedACK.tokenID)
        })
        it("encodeACK returns an encoded Trade", async () => {
            bytesTrade = ethers.utils.defaultAbiCoder.encode(tradeAbi, [trade])
            let decodedTrade = await settlementUtilities.callStatic.decodeTrade(bytesTrade)
            expect(trade.tradeID).to.be.eq(decodedTrade.tradeID)
        })
    })

    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------

    describe("", async() => {
        beforeEach( async() => {
            ack.tradeID = 0x324523555
            ack.settlementID = 0x9999
            ack.chainID = 0x87519703597
            ack.tokenID = aliceAddr
            trade = {tradeID: 0x555}
        })
        it("accepts ACK, decodes properly and returns proper tradeID", async() => {
            bytesACK = ethers.utils.defaultAbiCoder.encode(ackAbi, [ack])
            const[hashedACK, decodedID] = await settlementUtilities.callStatic.decodeAndHash(bytesACK)
            expect(hashedACK).to.be.eq(hashACK(ack))
            expect(ack.tradeID).to.be.eq(decodedID)
        })

        it("accepts Trade, decodes properly and returns proper tradeID", async() => {
            bytesTrade = ethers.utils.defaultAbiCoder.encode(tradeAbi, [trade])
            const[hashedTrade, decodedID] = await settlementUtilities.callStatic.decodeAndHash(bytesTrade)
            expect(hashedTrade).to.be.eq(hashTrade(trade))
            // expect(trade.tradeID).to.be.eq(decodedID)
        })
        it("Fails if incorrect bytes length submitted", async() => {
            // various bytes of incorrect length for decoding as ACKs or Trades
            const wrongLengths = [
                "0x12341234123412341234123412341234",  //16
                "0x12341234123412341234123412341212341234123412341234123412341234", //30
                "0x123412341234123412341234123412341123412341234123412341234123412341", //34
                "0x12341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234", //64
                "0x12341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412", //126
                "0x123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123422", //130
                "0x12341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234123412341234", //256
            ]
            for (let ii = 0; ii < wrongLengths.length; ii++) {
                await expect(settlementUtilities.decodeAndHash(wrongLengths[ii])).to.be.revertedWith("INCORRECT_LEAF_DATA_LENGTH")
            }
        })
    })

})
