// Copyright © 2022 TXA PTE. LTD.
import {expect} from "chai"
import {ethers, getNamedAccounts, config, deployments, getUnnamedAccounts} from "hardhat"
import {Address} from "hardhat-deploy/dist/types"

import {getDeployed, setupTest, ExchangeEIP712, _za} from "../utils"

describe("ExchangeEIP712", function () {
    const version = "1"
    let verifierInstance: ExchangeEIP712
    let chainId: any
    let exchange: Address

    before(async () => {
        const accounts = await getNamedAccounts()
        exchange = accounts.exchange
        chainId = await ethers.provider.send("eth_chainId", [])
        await setupTest()
        verifierInstance = (await getDeployed("ExchangeEIP712")) as ExchangeEIP712
    })

    it("recovers address from EIP712 typed data signed using eth_signTypedData_v4", async () => {
        const requestTypes = [
            {
                types: {
                    AssetNetworkInput: [
                        {name: "chainId", type: "uint256"},
                        {name: "assetCustody", type: "address"},
                        {name: "beneficiaryAddress", type: "address"},
                        {name: "networkName", type: "uint256"},
                    ],
                    EIP712Domain: [
                        {name: "name", type: "string"},
                        {name: "version", type: "string"},
                        {name: "chainId", type: "uint256"},
                        {name: "verifyingContract", type: "address"},
                        {name: "salt", type: "bytes32"},
                    ],
                    SubmitLimitOrderRequestParamsInput: [
                        {name: "baseNetwork", type: "AssetNetworkInput"},
                        {name: "counterNetwork", type: "AssetNetworkInput"},
                        {name: "productId", type: "string"},
                        {name: "side", type: "uint256"},
                        {name: "fillType", type: "uint256"},
                        {name: "size", type: "string"},
                        {name: "price", type: "string"},
                    ],
                },
                primaryTypeName: "SubmitLimitOrderRequestParamsInput",
                verify: verifierInstance.verifySubmitLimitOrderRequestParamsInput,
                message: {
                    baseNetwork: {
                        chainId: chainId,
                        assetCustody: exchange,
                        beneficiaryAddress: exchange,
                        networkName: 0,
                    },
                    counterNetwork: {
                        chainId: chainId,
                        assetCustody: exchange,
                        beneficiaryAddress: exchange,
                        networkName: 0,
                    },
                    productId: "1",
                    side: 0,
                    fillType: 0,
                    size: "2.15",
                    price: "2.41",
                },
            },
            {
                types: {
                    AssetNetworkInput: [
                        {name: "chainId", type: "uint256"},
                        {name: "assetCustody", type: "address"},
                        {name: "beneficiaryAddress", type: "address"},
                        {name: "networkName", type: "uint256"},
                    ],
                    EIP712Domain: [
                        {name: "name", type: "string"},
                        {name: "version", type: "string"},
                        {name: "chainId", type: "uint256"},
                        {name: "verifyingContract", type: "address"},
                        {name: "salt", type: "bytes32"},
                    ],
                    SubmitMarketOrderRequestParamsInput: [
                        {name: "baseNetwork", type: "AssetNetworkInput"},
                        {name: "counterNetwork", type: "AssetNetworkInput"},
                        {name: "productId", type: "string"},
                        {name: "side", type: "uint256"},
                        {name: "fillType", type: "uint256"},
                        {name: "size", type: "string"},
                    ],
                },
                primaryTypeName: "SubmitMarketOrderRequestParamsInput",
                verify: verifierInstance.verifySubmitMarketOrderRequestParamsInput,
                message: {
                    baseNetwork: {
                        chainId: chainId,
                        assetCustody: exchange,
                        beneficiaryAddress: exchange,
                        networkName: 0,
                    },
                    counterNetwork: {
                        chainId: chainId,
                        assetCustody: exchange,
                        beneficiaryAddress: exchange,
                        networkName: 0,
                    },
                    productId: "1",
                    side: 0,
                    fillType: 0,
                    size: "2.15",
                },
            },
            {
                types: {
                    EIP712Domain: [
                        {name: "name", type: "string"},
                        {name: "version", type: "string"},
                        {name: "chainId", type: "uint256"},
                        {name: "verifyingContract", type: "address"},
                        {name: "salt", type: "bytes32"},
                    ],
                    UpdateOrderRequestParamsInput: [
                        {name: "id", type: "string"},
                        {name: "size", type: "string"},
                        {name: "price", type: "string"},
                        {name: "orderType", type: "uint256"},
                    ],
                },
                primaryTypeName: "UpdateOrderRequestParamsInput",
                verify: verifierInstance.verifyUpdateOrderRequestParamsInput,
                message: {
                    id: "1",
                    size: "4.3",
                    price: "2.15",
                    orderType: 0,
                },
            },
            {
                types: {
                    CancelOrderRequestParamsInput: [{name: "id", type: "string"}],
                    EIP712Domain: [
                        {name: "name", type: "string"},
                        {name: "version", type: "string"},
                        {name: "chainId", type: "uint256"},
                        {name: "verifyingContract", type: "address"},
                        {name: "salt", type: "bytes32"},
                    ],
                },
                primaryTypeName: "CancelOrderRequestParamsInput",
                verify: verifierInstance.verifyCancelOrderRequestParamsInput,
                message: {
                    id: "1",
                },
            },
        ]

        const verifierAddress = verifierInstance.address
        const verifierSalt = await verifierInstance.callStatic.salt()
        const from = exchange

        const method = "eth_signTypedData_v4"
        for (const {primaryTypeName, verify, message, types} of requestTypes) {
            const msgParams = {
                types,
                primaryType: primaryTypeName,
                domain: {
                    name: "TXA",
                    version: version,
                    chainId: chainId,
                    verifyingContract: verifierAddress,
                    salt: verifierSalt,
                },
                message,
            }
            const params = [from, msgParams]

            const signedData = await ethers.provider.send(method, params)

            const signature = signedData.substring(2)
            const r = "0x" + signature.substring(0, 64)
            const s = "0x" + signature.substring(64, 128)
            const v = parseInt(signature.substring(128, 130), 16)

            const sigMatches = await (verify as any)(from, msgParams.message, v, r, s)

            expect(sigMatches, "Signature verification fails for " + primaryTypeName).to.be.true
        }
    })
})
