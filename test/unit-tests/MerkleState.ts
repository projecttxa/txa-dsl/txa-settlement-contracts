// Copyright © 2022 TXA PTE. LTD.
import {ethers, getNamedAccounts} from "hardhat"
import {Address} from "hardhat-deploy/dist/types"
import {Signer, BigNumber} from "ethers"
import {expect} from "chai"
import {MerkleTree} from "merkletreejs"
import {
    operatorInit,
    CollateralMock,
    CollateralCustody,
    CoordinationMock,
    SettlementUtilities,
    setupTest,
    ProtocolToken,
    IdentityRegistry,
    getSigner,
    SettlementLib,
    ConsensusMock,
    approveAndDeposit,
    range,
    merkleFromArray,
    Leaf,
    hashSignLeaves,
    hashACK,
    merkleFromHashArray,
    Signature,
    Trade,
    tradeAbi,
    ACK,
    ackAbi,
    _za,
    fullMerkleTree,
    ChallengeData,
    getOrderedProof,
    getTradeLeafData,
    getDeployed,
    DisputeProfile,
} from "../utils"

describe("Merkle State", function () {
    let sdpAdmin1: Signer
    let sdpAdmin2: Signer
    let sdpAdmin3: Signer
    let sdpAdmin4: Signer
    let sdpOperator1: Signer
    let sdpOperator2: Signer
    let sdpOperator3: Signer
    let sdpOperator4: Signer

    let sdpAdmin1Address: Address
    let sdpAdmin2Address: Address
    let sdpAdmin3Address: Address
    let sdpOperator1Address: Address
    let sdpOperator2Address: Address
    let sdpOperator3Address: Address

    let exchangeAddr: Address
    let coordAddr: Address

    let settlementUtilities: SettlementUtilities
    let consensus: ConsensusMock
    let coordination: CoordinationMock
    let stakeAmount = 10000

    let exchange: Signer

    let alice: Signer
    let aliceAddr: string

    let protocolToken: ProtocolToken
    let chainID: number

    const merkleRootZero = "0x0000000000000000000000000000000000000000000000000000000000000000"

    let collateralCustody: CollateralCustody
    let collateralCustodyMock: CollateralMock
    let unlockInterval: number
    let blockInterval: string

    const hre = require("hardhat")
    let identity: IdentityRegistry
    let library: SettlementLib

    let sig: Signature
    let leaves: Leaf[]
    let leafHashes: string[]
    let currentRootID: BigNumber
    let end: number //index of end of leaf array
    let indx: number // index of arbitrary trade

    let tradeRange: number[]
    let tradeArray: Trade[]
    let tradeHashes: string[]
    let tradeTree: MerkleTree
    let merkleRoot: string
    let hashedLeaf: string
    let ackLeaf: string
    let tradeLeaf: string
    let leaf: number
    let proofData: string[]
    let rightProofTrue: boolean[]

    let challengeLeaves: Leaf[]
    let challengeTree: MerkleTree
    let challengeRoot: string
    let challengeProof: string[]
    let challengeOrder: boolean[]
    let challengeACK: ACK
    let challengeTrade: Trade
    let challengeSig: Signature
    let challengeData: ChallengeData

    let disputeProfile: DisputeProfile
    let disputer: Address

    let challengeLeaves2: Leaf[]
    let challengeTree2: MerkleTree
    let challengeRoot2: string
    let challengeProof2: string[]
    let challengeOrder2: boolean[]
    let challengeACK2: ACK
    let challengeSig2: Signature
    let challengeData2: ChallengeData

    let tradeKey: any
    let tradePrivate: string
    let tradePublic: string
    let tradeSigner: Signer
    let tradeAddress: Address
    let ack: ACK
    let trade: Trade

    before(async () => {
        const namedAccounts = await getNamedAccounts()
        aliceAddr = (await getNamedAccounts()).alice
        alice = await ethers.getSigner(aliceAddr)

        sdpAdmin1 = await ethers.provider.getSigner(namedAccounts.sdpAdmin1)
        sdpAdmin2 = await ethers.provider.getSigner(namedAccounts.sdpAdmin2)
        sdpAdmin3 = await ethers.provider.getSigner(namedAccounts.sdpAdmin3)
        sdpAdmin4 = await ethers.provider.getSigner(namedAccounts.sdpAdmin4)
        sdpOperator1 = await ethers.provider.getSigner(namedAccounts.sdpOperator1)
        sdpOperator2 = await ethers.provider.getSigner(namedAccounts.sdpOperator2)
        sdpOperator3 = await ethers.provider.getSigner(namedAccounts.sdpOperator3)
        sdpOperator4 = await ethers.provider.getSigner(namedAccounts.sdpOperator4)

        sdpAdmin2Address = ethers.utils.getAddress(await sdpAdmin2.getAddress())
        sdpAdmin1Address = ethers.utils.getAddress(await sdpAdmin1.getAddress())
        sdpAdmin3Address = ethers.utils.getAddress(await sdpAdmin3.getAddress())
        sdpOperator2Address = ethers.utils.getAddress(await sdpOperator2.getAddress())
        sdpOperator1Address = ethers.utils.getAddress(await sdpOperator1.getAddress())
        sdpOperator3Address = ethers.utils.getAddress(await sdpOperator3.getAddress())
        exchange = await getSigner(namedAccounts.exchange)
        exchangeAddr = await exchange.getAddress()

        tradePrivate = ethers.utils.solidityKeccak256(["address", "address"], [exchangeAddr, exchangeAddr])
        tradeKey = new ethers.utils.SigningKey(tradePrivate)
        tradePublic = tradeKey.publicKey
        tradeSigner = new ethers.Wallet(tradePrivate)
        tradeAddress = await tradeSigner.getAddress()
    })

    beforeEach(async () => {
        let contracts = await setupTest()
        identity = contracts.identity
        library = contracts.library
        consensus = contracts.consensusMock
        coordination = contracts.coordinationMock
        coordAddr = coordination.address
        settlementUtilities = (await getDeployed("SettlementUtilities")) as SettlementUtilities

        let {chainId} = await ethers.provider.getNetwork()
        chainID = chainId
        collateralCustody = contracts.collateralCustody
        collateralCustodyMock = contracts.collateralMock

        await identity.updateRole(
            await contracts.library.callStatic.ROLE_COLLATERAL_CUSTODY(),
            await collateralCustodyMock.address
        )
        await identity.updateRole(await contracts.library.callStatic.ROLE_CONSENSUS(), consensus.address)
        await identity.initializeRole(await contracts.library.callStatic.ROLE_TRADE_SIGNER(), tradeAddress)

        protocolToken = contracts.protocolToken
        unlockInterval = (await consensus.callStatic.rootApprovalTime()).toNumber()
        blockInterval = "0x" + (unlockInterval - 1).toString(16)
        await operatorInit(sdpAdmin1, sdpOperator1, collateralCustodyMock)
        await operatorInit(sdpAdmin2, sdpOperator2, collateralCustodyMock)
        await operatorInit(sdpAdmin3, sdpOperator3, collateralCustodyMock)
        ;[leaves, tradeTree] = fullMerkleTree(tradeKey, 100, 1, chainID, _za)
        ;[proofData, rightProofTrue] = getOrderedProof(leaves, tradeTree, 0)
        leafHashes = leaves.map(x => x.hash)
        merkleRoot = tradeTree.getHexRoot()
        ack = leaves[leaves.length - 1].data
        sig = leaves[leaves.length - 1].sig
        end = leaves.length - 1
    })

    describe("testing basic trade merkle root submission", async () => {
        beforeEach(async () => {})
        it("SDP operator can submit merkle root", async () => {
            await consensus
                .connect(sdpOperator1)
                .reportTradeRoot(merkleRoot, leafHashes, leaves[end].data, leaves[end].sig)
            const merkleState1 = await consensus.callStatic.tradeRoots(1)
            expect(merkleState1.merkleRoot).to.be.eq(merkleRoot)
        })
        it("Finalizing trade roots increments trade root block ID", async () => {
            for (let ii = 0; ii < 20; ii++) {
                ;[leaves, tradeTree] = fullMerkleTree(tradeKey, 100, ii * 100 + 1, chainID, _za)
                ;[proofData, rightProofTrue] = getOrderedProof(leaves, tradeTree, 0)
                leafHashes = leaves.map(x => x.hash)
                merkleRoot = tradeTree.getHexRoot()

                expect(await consensus.callStatic.currentRootID()).to.be.eq(ii + 1)
                await consensus
                    .connect(sdpOperator1)
                    .reportTradeRoot(merkleRoot, leafHashes, leaves[end].data, leaves[end].sig)
                await hre.network.provider.send("hardhat_mine", [blockInterval])
                await consensus.connect(sdpOperator1).finalizeTradeRoot()
            }
        })
        it("Multiple Operators can submit many merkle roots, trade Ids kept in sync", async () => {
            await consensus
                .connect(sdpOperator1)
                .reportTradeRoot(merkleRoot, leafHashes, leaves[end].data, leaves[end].sig)
            const merkleState1 = await consensus.callStatic.tradeRoots(1)
            expect(merkleState1.merkleRoot).to.be.eq(merkleRoot)

            await hre.network.provider.send("hardhat_mine", [blockInterval])
            await consensus.connect(sdpOperator1).finalizeTradeRoot()
            ;[leaves, tradeTree] = fullMerkleTree(tradeKey, 100, 101, chainID, _za)
            ;[proofData, rightProofTrue] = getOrderedProof(leaves, tradeTree, 0)
            leafHashes = leaves.map(x => x.hash)
            merkleRoot = tradeTree.getHexRoot()

            await consensus
                .connect(sdpOperator2)
                .reportTradeRoot(merkleRoot, leafHashes, leaves[end].data, leaves[end].sig)
            const merkleState2 = await consensus.callStatic.tradeRoots(2)
            expect(merkleState2.merkleRoot).to.be.eq(merkleRoot)

            await hre.network.provider.send("hardhat_mine", [blockInterval])
            await consensus.connect(sdpOperator1).finalizeTradeRoot()
            ;[leaves, tradeTree] = fullMerkleTree(tradeKey, 100, 201, chainID, _za)
            ;[proofData, rightProofTrue] = getOrderedProof(leaves, tradeTree, 0)
            leafHashes = leaves.map(x => x.hash)
            merkleRoot = tradeTree.getHexRoot()

            await consensus
                .connect(sdpOperator3)
                .reportTradeRoot(merkleRoot, leafHashes, leaves[end].data, leaves[end].sig)
            const merkleState3 = await consensus.callStatic.tradeRoots(3)
            expect(merkleState3.merkleRoot).to.be.eq(merkleRoot)
        })
        it("non SDP Operators cannot report merkle roots", async () => {
            await expect(
                consensus
                    .connect(sdpAdmin1)
                    .reportTradeRoot(merkleRoot, leafHashes, leaves[end].data, leaves[end].sig)
            ).to.be.revertedWith("NOT_APPROVED_OPERATOR")
        })

        //-----------------------------------------------------------------------------------------------------------
        //-----------------------------------------------------------------------------------------------------------
        //-----------------------------------------------------------------------------------------------------------
        //-----------------------------------------------------------------------------------------------------------

        describe("Tests basic root submission state machine", async () => {
            it("state machine starts with awaitingRoot=true", async () => {
                //check submissionState = awaitingRoot
                expect(await consensus.callStatic.submissionState()).to.be.eq(0)
            })
            it("state transitions to awaitingRoot=false after merke root submitted", async () => {
                await consensus
                    .connect(sdpOperator1)
                    .reportTradeRoot(merkleRoot, leafHashes, leaves[end].data, leaves[end].sig)
                //check submissionState = awaitingChallengeOrTimeout
                expect(await consensus.callStatic.submissionState()).to.be.eq(1)
            })
            it("finalizeTradeRoot() not callable before timer elapses", async () => {
                unlockInterval = unlockInterval - 1
                blockInterval = "0x" + (unlockInterval - 1).toString(16)
                await consensus
                    .connect(sdpOperator1)
                    .reportTradeRoot(merkleRoot, leafHashes, leaves[end].data, leaves[end].sig)
                await hre.network.provider.send("hardhat_mine", [blockInterval])
                await expect(consensus.connect(sdpOperator1).finalizeTradeRoot()).to.be.revertedWith(
                    "ROOT_APPROVAL_TIME_NOT_PASSED"
                )
            })
            it("finalizeTradeRoot() not callable when submissionState is not set to awaitingChallengeOrTimeout", async () => {
                await expect(consensus.connect(sdpOperator1).finalizeTradeRoot()).to.be.revertedWith(
                    "WRONG_ROOT_SUBMISSION_STATE"
                )
            })
            it("finalizeTradeRoot() not callable by non SDP Operator", async () => {
                await consensus
                    .connect(sdpOperator1)
                    .reportTradeRoot(merkleRoot, leafHashes, leaves[end].data, leaves[end].sig)
                await hre.network.provider.send("hardhat_mine", [blockInterval])
                await expect(consensus.connect(sdpAdmin1).finalizeTradeRoot()).to.be.revertedWith(
                    "NOT_APPROVED_OPERATOR"
                )
            })
            it("Iterate state change", async () => {
                for (let ii = 0; ii < 20; ii++) {
                    ;[leaves, tradeTree] = fullMerkleTree(tradeKey, 100, ii * 100 + 1, chainID, _za)
                    ;[proofData, rightProofTrue] = getOrderedProof(leaves, tradeTree, 0)
                    merkleRoot = tradeTree.getHexRoot()
                    leafHashes = leaves.map(x => x.hash)
                    //check submissionState = awaitingRoot
                    expect(await consensus.callStatic.submissionState()).to.be.eq(0)
                    await consensus
                        .connect(sdpOperator1)
                        .reportTradeRoot(merkleRoot, leafHashes, leaves[end].data, leaves[end].sig)
                    await hre.network.provider.send("hardhat_mine", [blockInterval])
                    //check submissionState = awaitingChallengeOrTimeout
                    expect(await consensus.callStatic.submissionState()).to.be.eq(1)
                    await consensus.connect(sdpOperator1).finalizeTradeRoot()
                }
            })
        })
    })
    //-----------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------

    describe("challengeTradeRoot() function", async () => {
        beforeEach(async () => {
            indx = 39 // index of leaf to be submitted as challenge leaf
            // add incorrect leaves to the submitted root to allow challenges
            leaves[indx - 1].data.tradeID = 1000
            leaves[indx + 9].data.tradeID = 1020
            leaves = hashSignLeaves(tradeKey, leaves)
            leafHashes = leaves.map(x => x.hash)
            tradeTree = merkleFromHashArray(leafHashes)
            merkleRoot = tradeTree.getHexRoot()
            ;[proofData, rightProofTrue] = getOrderedProof(leaves, tradeTree, 0)
            await consensus
                .connect(sdpOperator1)
                .reportTradeRoot(merkleRoot, leafHashes, leaves[end].data, leaves[end].sig)
            ;[challengeLeaves, challengeTree] = fullMerkleTree(tradeKey, 100, 1, chainID, _za)
            // make challenge root different (for now challenger is incorrect)
            challengeLeaves[20].data.tradeID = 49
            //ensure settlement ACK is different
            challengeLeaves[challengeLeaves.length - 1].data.settlementID = 34
            challengeLeaves = hashSignLeaves(tradeKey, challengeLeaves)
            challengeTree = merkleFromHashArray(challengeLeaves.map(x => x.hash))
            ;[challengeProof, challengeOrder] = getOrderedProof(challengeLeaves, challengeTree, 0)
            challengeRoot = challengeTree.getHexRoot()
            challengeACK = challengeLeaves[challengeLeaves.length - 1].data
            challengeSig = challengeLeaves[challengeLeaves.length - 1].sig
            // create challengeData struct
            challengeData = {
                challengeRoot: challengeRoot,
                challengeLeaf: ethers.utils.defaultAbiCoder.encode(ackAbi, [challengeACK]),
                challengeSig: challengeSig,
                reportedHash: leaves[end].hash,
            }
            ;[challengeLeaves2, challengeTree2] = fullMerkleTree(tradeKey, 100, 1, chainID, _za)
            // make challenge root different (for now challenger is incorrect)
            challengeLeaves2[68].data.tradeID = 29
            //ensure settlement ACK is different
            challengeLeaves2[challengeLeaves.length - 1].data.settlementID = 34
            challengeLeaves2 = hashSignLeaves(tradeKey, challengeLeaves2)
            challengeTree2 = merkleFromHashArray(challengeLeaves2.map(x => x.hash))
            ;[challengeProof2, challengeOrder2] = getOrderedProof(challengeLeaves2, challengeTree2, 0)
            challengeRoot2 = challengeTree2.getHexRoot()
            challengeACK2 = challengeLeaves2[challengeLeaves2.length - 1].data
            challengeSig2 = challengeLeaves2[challengeLeaves2.length - 1].sig
            // create challengeData struct
            challengeData2 = {
                challengeRoot: challengeRoot2,
                challengeLeaf: ethers.utils.defaultAbiCoder.encode(ackAbi, [challengeACK2]),
                challengeSig: challengeSig2,
                reportedHash: leaves[end].hash,
            }
        })
        it("Challenge sets submission state to processingChallenge", async () => {
            await consensus
                .connect(sdpOperator2)
                .challengeTradeRoot(challengeData, challengeProof, challengeOrder, proofData)
            expect(await consensus.callStatic.submissionState()).to.be.eq(2)
        })
        it("challengeTradeRoot() only callable by sdpOperator", async () => {
            await expect(
                consensus
                    .connect(sdpAdmin1)
                    .challengeTradeRoot(challengeData, challengeProof, challengeOrder, proofData)
            ).to.be.revertedWith("NOT_APPROVED_OPERATOR")
        })
        it("challengeTradeRoot() not callable after challenge period over", async () => {
            await hre.network.provider.send("hardhat_mine", [blockInterval])
            await expect(
                consensus
                    .connect(sdpOperator1)
                    .challengeTradeRoot(challengeData, challengeProof, challengeOrder, proofData)
            ).to.be.revertedWith("CHALLENGE_PERIOD_TIMED_OUT")
        })
        it("challengeTradeRoot() not callable when rootSubmissionState is not awaitingChallengeOrTimeout", async () => {
            await hre.network.provider.send("hardhat_mine", [blockInterval])
            await consensus.connect(sdpOperator1).finalizeTradeRoot()
            await expect(
                consensus
                    .connect(sdpOperator1)
                    .challengeTradeRoot(challengeData, challengeProof, challengeOrder, proofData)
            ).to.be.revertedWith("CHALLENGE_PERIOD_NOT_OPEN")
        })

        it("challengeTradeRoot() submission fails if challenge verification fails", async () => {
            challengeProof[0] = merkleRootZero
            await expect(
                consensus
                    .connect(sdpOperator1)
                    .challengeTradeRoot(challengeData, challengeProof, challengeOrder, proofData)
            ).to.be.revertedWith("MERKLE_PROOF_INVALID")
        })
        it("challengeTradeRoot() submission fails if reported verification fails", async () => {
            proofData[0] = merkleRootZero
            await expect(
                consensus
                    .connect(sdpOperator1)
                    .challengeTradeRoot(challengeData, challengeProof, challengeOrder, proofData)
            ).to.be.revertedWith("MERKLE_PROOF_INVALID")
        })
        it("successive challengeTradeRoot() calls increment the disputeTotal counter", async () => {
            // also verifies that challengeTradeRoot() callable when in processingChallenge state
            expect(await consensus.callStatic.disputeCounter()).to.be.eq(0)
            await consensus
                .connect(sdpOperator1)
                .challengeTradeRoot(challengeData, challengeProof, challengeOrder, proofData)
            expect(await consensus.callStatic.disputeCounter()).to.be.eq(1)

            await consensus
                .connect(sdpOperator2)
                .challengeTradeRoot(challengeData2, challengeProof2, challengeOrder2, proofData)
            expect(await consensus.callStatic.disputeCounter()).to.be.eq(2)
        })
        it("successive challengeTradeRoot calls create dispute profiles", async () => {
            // dispute profile starts empty
            let disputeProfile = await consensus.callStatic.disputeProfiles(challengeRoot)
            expect(disputeProfile[0]).to.be.eq(_za)
            await consensus
                .connect(sdpOperator1)
                .challengeTradeRoot(challengeData, challengeProof, challengeOrder, proofData)

            disputeProfile = await consensus.callStatic.disputeProfiles(challengeRoot)
            expect(disputeProfile[0]).to.be.eq(sdpOperator1Address)

            // dispute profile starts empty
            disputeProfile = await consensus.callStatic.disputeProfiles(challengeRoot2)
            expect(disputeProfile[0]).to.be.eq(_za)

            await consensus
                .connect(sdpOperator2)
                .challengeTradeRoot(challengeData2, challengeProof2, challengeOrder2, proofData)

            disputeProfile = await consensus.callStatic.disputeProfiles(challengeRoot2)
            expect(disputeProfile[0]).to.be.eq(sdpOperator2Address)
        })
        it("challengeTradeRoot call saves block number to dispute profile", async () => {
            // dispute profile starts empty
            let disputeProfile = await consensus.callStatic.disputeProfiles(challengeRoot)
            expect(disputeProfile[1]).to.be.eq(0)

            await consensus
                .connect(sdpOperator1)
                .challengeTradeRoot(challengeData, challengeProof, challengeOrder, proofData)

            let block = await hre.ethers.provider.getBlock("latest")
            disputeProfile = await consensus.callStatic.disputeProfiles(challengeRoot)
            expect(disputeProfile[1]).to.be.eq(block.number)

            // dispute profile starts empty
            disputeProfile = await consensus.callStatic.disputeProfiles(challengeRoot2)
            expect(disputeProfile[1]).to.be.eq(_za)
            blockInterval = "0x3"
            await hre.network.provider.send("hardhat_mine", [blockInterval])

            await consensus
                .connect(sdpOperator2)
                .challengeTradeRoot(challengeData2, challengeProof2, challengeOrder2, proofData)

            block = await hre.ethers.provider.getBlock("latest")
            disputeProfile = await consensus.callStatic.disputeProfiles(challengeRoot2)
            expect(disputeProfile[1]).to.be.eq(block.number)
        })
        it("dispute profile unresolved set to true after challengeTradeRoot() call", async () => {
            await consensus
                .connect(sdpOperator2)
                .challengeTradeRoot(challengeData, challengeProof, challengeOrder, proofData)
            expect(await consensus.callStatic.submissionState()).to.be.eq(2)

            let disputeProfile = await consensus.callStatic.disputeProfiles(challengeRoot)
            expect(disputeProfile[0]).to.be.eq(sdpOperator2Address)

            expect(disputeProfile[2]).to.be.eq(true)
        })
        it("cannot submit a challenge with a trade root with an already active challenge", async () => {
            await consensus
                .connect(sdpOperator1)
                .challengeTradeRoot(challengeData, challengeProof, challengeOrder, proofData)

            await expect(
                consensus
                    .connect(sdpOperator2)
                    .challengeTradeRoot(challengeData, challengeProof, challengeOrder, proofData)
            ).to.be.revertedWith("DISPUTE_ROOT_ALREADY_HAS_ACTIVE_CHALLENGE")
        })
        it("Can submit trade leaf as challenge", async () => {
            // get proof for specific trade index
            ;[challengeProof, challengeOrder] = getOrderedProof(challengeLeaves, challengeTree, indx)

            challengeTrade = challengeLeaves[indx - 1].data
            challengeSig = challengeLeaves[indx - 1].sig
            // get proof for specific trade index
            ;[proofData, rightProofTrue] = getOrderedProof(leaves, tradeTree, indx)
            // create challengeData struct
            challengeData = {
                challengeRoot: challengeRoot,
                challengeLeaf: ethers.utils.defaultAbiCoder.encode(tradeAbi, [challengeTrade]),
                challengeSig: challengeSig,
                reportedHash: leaves[indx - 1].hash,
            }

            await consensus
                .connect(sdpOperator2)
                .challengeTradeRoot(challengeData, challengeProof, challengeOrder, proofData)
            expect(await consensus.callStatic.submissionState()).to.be.eq(2)
        })
        it("Challenge submission fails if challenger and reporter leaf in different location of merkle tree", async () => {
            indx = 39 // index of leaf submitted for challenge
            ;[challengeLeaves, challengeTree] = fullMerkleTree(tradeKey, 100, 1, chainID, _za)
            // make challenge root different (for now challenger is incorrect)
            challengeLeaves[20].data.tradeID = 49
            challengeLeaves = hashSignLeaves(tradeKey, challengeLeaves)
            challengeTree = merkleFromHashArray(challengeLeaves.map(x => x.hash))
            // get proof for specific trade index
            ;[challengeProof, challengeOrder] = getOrderedProof(challengeLeaves, challengeTree, indx)
            challengeRoot = challengeTree.getHexRoot()
            challengeTrade = challengeLeaves[indx - 1].data
            challengeSig = challengeLeaves[indx - 1].sig
            // get proof for specific trade index plus 10
            ;[proofData, rightProofTrue] = getOrderedProof(leaves, tradeTree, indx + 10)
            leafHashes = leaves.map(x => x.hash)
            merkleRoot = tradeTree.getHexRoot()
            // create challengeData struct
            challengeData = {
                challengeRoot: challengeRoot,
                challengeLeaf: ethers.utils.defaultAbiCoder.encode(tradeAbi, [challengeTrade]),
                challengeSig: challengeSig,
                reportedHash: leaves[indx - 1].hash,
            }

            await expect(
                consensus
                    .connect(sdpOperator2)
                    .challengeTradeRoot(challengeData, challengeProof, challengeOrder, proofData)
            ).to.be.revertedWith("MERKLE_PROOF_INVALID")

            await expect(
                consensus
                    .connect(sdpOperator2)
                    .challengeTradeRoot(challengeData, challengeProof, rightProofTrue, proofData)
            ).to.be.revertedWith("MERKLE_PROOF_INVALID")
        })
        it("Cannot submit challenge if challenge root is same as reported root", async () => {
            // set challenge root to reported root to cause failure
            challengeData.challengeRoot = merkleRoot
            await expect(
                consensus
                    .connect(sdpOperator2)
                    .challengeTradeRoot(challengeData, challengeProof, challengeOrder, proofData)
            ).to.be.revertedWith("CHALLENGE_ROOT_DOES_NOT_DIFFER")
        })
        it("Cannot submit challenge if challenge leaf is same as reported leaf", async () => {
            // set challenge leaf data to same as reported leaf data to cause failure
            ;(challengeData.challengeLeaf = ethers.utils.defaultAbiCoder.encode(ackAbi, [leaves[end].data])),
                (challengeData.challengeSig = leaves[end].sig)
            await expect(
                consensus
                    .connect(sdpOperator2)
                    .challengeTradeRoot(challengeData, challengeProof, challengeOrder, proofData)
            ).to.be.revertedWith("CHALLENGE_LEAF_DOES_NOT_DIFFER")
        })
    })

    //-----------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------

    describe("Testing challengeSuccessTimeout() function", async () => {
        beforeEach(async () => {
            indx = 39 // index of leaf to be submitted as challenge leaf
            // add incorrect leaves to the submitted root to allow challenges
            leaves[indx - 1].data.tradeID = 1000
            leaves[indx + 9].data.tradeID = 1020
            leaves = hashSignLeaves(tradeKey, leaves)
            leafHashes = leaves.map(x => x.hash)
            tradeTree = merkleFromHashArray(leafHashes)
            merkleRoot = tradeTree.getHexRoot()
            ;[proofData, rightProofTrue] = getOrderedProof(leaves, tradeTree, 0)
            await consensus
                .connect(sdpOperator1)
                .reportTradeRoot(merkleRoot, leafHashes, leaves[end].data, leaves[end].sig)
            ;[challengeLeaves, challengeTree] = fullMerkleTree(tradeKey, 100, 1, chainID, _za)
            // make challenge root different (for now challenger is incorrect)
            challengeLeaves[20].data.tradeID = 49
            //ensure settlement ACK is different
            challengeLeaves[challengeLeaves.length - 1].data.settlementID = 34
            challengeLeaves = hashSignLeaves(tradeKey, challengeLeaves)
            challengeTree = merkleFromHashArray(challengeLeaves.map(x => x.hash))
            ;[challengeProof, challengeOrder] = getOrderedProof(challengeLeaves, challengeTree, 0)
            challengeRoot = challengeTree.getHexRoot()
            challengeACK = challengeLeaves[challengeLeaves.length - 1].data
            challengeSig = challengeLeaves[challengeLeaves.length - 1].sig
            // create challengeData struct
            challengeData = {
                challengeRoot: challengeRoot,
                challengeLeaf: ethers.utils.defaultAbiCoder.encode(ackAbi, [challengeACK]),
                challengeSig: challengeSig,
                reportedHash: leaves[end].hash,
            }
            await consensus
                .connect(sdpOperator2)
                .challengeTradeRoot(challengeData, challengeProof, challengeOrder, proofData)
        })
        it("challengeSuccessTimeout() callable after time elapsed, changes submission state", async () => {
            await hre.network.provider.send("hardhat_mine", [blockInterval])
            await consensus.connect(sdpOperator3).challengeSuccessTimeout(challengeData.challengeRoot)
            expect(await consensus.callStatic.submissionState()).to.be.eq(3)
        })
        it("challengeSuccessTimeout() fails while reply period still open", async () => {
            unlockInterval = unlockInterval - 1
            blockInterval = "0x" + (unlockInterval - 1).toString(16)
            await hre.network.provider.send("hardhat_mine", [blockInterval])
            await expect(
                consensus.connect(sdpOperator3).challengeSuccessTimeout(challengeData.challengeRoot)
            ).to.be.revertedWith("REPLY_PERIOD_STILL_OPEN")
        })
        it("non SDP operator cannot call challengeSuccessTimeout()", async () => {
            await hre.network.provider.send("hardhat_mine", [blockInterval])
            await expect(
                consensus.connect(alice).challengeSuccessTimeout(challengeData.challengeRoot)
            ).to.be.revertedWith("NOT_APPROVED_OPERATOR")
        })
        it("Cannot call on inactive challenge", async () => {
            //todo: when full challenge resolution is active, make another version of this test case for a challenge root
            //      that was once active
            await hre.network.provider.send("hardhat_mine", [blockInterval])
            await expect(
                consensus.connect(sdpOperator3).challengeSuccessTimeout(merkleRootZero)
            ).to.be.revertedWith("DISPUTE_PROFILE_NOT_ACTIVE")
        })
    })

    //-----------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------

    describe("Testing verifyTrade() function", async () => {
        beforeEach(async () => {
            await consensus
                .connect(sdpOperator1)
                .reportTradeRoot(merkleRoot, leafHashes, leaves[end].data, leaves[end].sig)
            indx = 49 // trade index
            ;[proofData, rightProofTrue] = getOrderedProof(leaves, tradeTree, indx + 1)
        })
        it("Verifies Trade In Simple Call", async () => {
            await expect(
                consensus.verifyTrade(1, proofData, rightProofTrue, leaves[indx].data, leaves[indx].sig)
            ).to.not.be.reverted
        })
        it("Verifies Multiple Trades", async () => {
            for (let ii = 0; ii < leaves.length; ii += 7) {
                indx = ii //trade index
                ;[proofData, rightProofTrue] = getOrderedProof(leaves, tradeTree, indx + 1)
                await expect(
                    consensus.verifyTrade(1, proofData, rightProofTrue, leaves[indx].data, leaves[indx].sig)
                ).to.not.be.reverted
            }
        })
        it("Dissallows Checking Trade under Trade Root ID that does not exist", async () => {
            await expect(
                consensus.verifyTrade(2, proofData, rightProofTrue, leaves[indx].data, leaves[indx].sig)
            ).to.be.revertedWith("TRADE_ROOT_DOES_NOT_EXIST")
        })
    })

    //-----------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------

    describe("Testing verifyACK() function", async () => {
        beforeEach(async () => {
            await consensus
                .connect(sdpOperator1)
                .reportTradeRoot(merkleRoot, leafHashes, leaves[end].data, leaves[end].sig)
        })
        it("Verifies ACK In Simple Call", async () => {
            await expect(consensus.verifyACK(1, proofData, rightProofTrue, leaves[end].data, leaves[end].sig))
                .to.not.be.reverted
        })
        it("Dissallows Checking ACK under Trade Root ID that does not exist", async () => {
            await expect(
                consensus.verifyACK(2, proofData, rightProofTrue, leaves[end].data, leaves[end].sig)
            ).to.be.revertedWith("TRADE_ROOT_DOES_NOT_EXIST")
        })
        it("Merkle Proof Fails if ack is latered", async () => {
            const alteredACK = leaves[end].data
            alteredACK.settlementID = 1029
            await expect(
                consensus.verifyACK(1, proofData, rightProofTrue, alteredACK, leaves[end].sig)
            ).to.be.revertedWith("MERKLE_PROOF_INVALID")
        })
    })

    //-----------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------

    describe("Testing Merkle Proof verification in verifyLeaf() function", async () => {
        beforeEach(async () => {
            await consensus
                .connect(sdpOperator1)
                .reportTradeRoot(merkleRoot, leafHashes, leaves[end].data, leaves[end].sig)
            currentRootID = await consensus.callStatic.currentRootID()
        })

        it("verifyLeaf() Fails if Merkle Proof Incorrect", async () => {
            leaves[20].data.tradeID = 49
            leaves = hashSignLeaves(tradeKey, leaves)
            tradeTree = merkleFromHashArray(leaves.map(x => x.hash))
            ;[proofData, rightProofTrue] = getOrderedProof(leaves, tradeTree, 0)

            //merkleRoot has been unchanged from initialization

            await expect(
                consensus
                    .connect(sdpOperator1)
                    .verifyLeaf(
                        merkleRoot,
                        proofData,
                        rightProofTrue,
                        leaves[end].dataHash,
                        currentRootID,
                        leaves[end].data.tradeID,
                        leaves[end].sig
                    )
            ).to.be.revertedWith("MERKLE_PROOF_INVALID")
        })
        it("verifyLeaf() Fails if Merkle Leaf Incorrect", async () => {
            ack.tradeID = 999
            let hashedACK = hashACK(ack)
            let sig = await tradeKey.signDigest(hashedACK)
            await expect(
                consensus
                    .connect(sdpOperator1)
                    .verifyLeaf(
                        merkleRoot,
                        proofData,
                        rightProofTrue,
                        leaves[end].dataHash,
                        currentRootID,
                        leaves[end].data.tradeID,
                        leaves[end].sig
                    )
            ).to.be.revertedWith("MERKLE_PROOF_INVALID")
        })
        it("verifyLeaf() Fails if Proof Order is Incorrect", async () => {
            rightProofTrue[2] = !rightProofTrue[2]
            await expect(
                consensus
                    .connect(sdpOperator1)
                    .verifyLeaf(
                        merkleRoot,
                        proofData,
                        rightProofTrue,
                        leaves[end].dataHash,
                        currentRootID,
                        leaves[end].data.tradeID,
                        leaves[end].sig
                    )
            ).to.be.revertedWith("MERKLE_PROOF_INVALID")
        })
        it("verifyLeaf() fails if signature is incorrect ", async () => {
            // generate signature from incorrect key
            let fakePrivate = ethers.utils.solidityKeccak256(
                ["address", "address"],
                [sdpAdmin1Address, sdpAdmin1Address]
            )
            let fakeKey = new ethers.utils.SigningKey(fakePrivate)
            leaves[end].sig = fakeKey.signDigest(leaves[end].dataHash)
            await expect(
                consensus
                    .connect(sdpOperator1)
                    .verifyLeaf(
                        merkleRoot,
                        proofData,
                        rightProofTrue,
                        leaves[end].dataHash,
                        currentRootID,
                        leaves[end].data.tradeID,
                        leaves[end].sig
                    )
            ).to.be.revertedWith("INVALID_EXCHANGE_SIGNATURE")
        })
        it("verifyLeaf() works for later trade roots where lastTradeID!=0", async () => {
            await hre.network.provider.send("hardhat_mine", [blockInterval])
            await consensus.connect(sdpOperator1).finalizeTradeRoot()
            ;[leaves, tradeTree] = fullMerkleTree(tradeKey, 100, 101, chainID, _za)
            ;[proofData, rightProofTrue] = getOrderedProof(leaves, tradeTree, 0)
            leafHashes = leaves.map(x => x.hash)
            merkleRoot = tradeTree.getHexRoot()

            await consensus
                .connect(sdpOperator2)
                .reportTradeRoot(merkleRoot, leafHashes, leaves[end].data, leaves[end].sig)

            await hre.network.provider.send("hardhat_mine", [blockInterval])
            await consensus.connect(sdpOperator1).finalizeTradeRoot()
            ;[leaves, tradeTree] = fullMerkleTree(tradeKey, 100, 201, chainID, _za)
            ;[proofData, rightProofTrue] = getOrderedProof(leaves, tradeTree, 0)
            leafHashes = leaves.map(x => x.hash)
            merkleRoot = tradeTree.getHexRoot()

            // rejects incorrectly positioned trade
            currentRootID = await consensus.callStatic.currentRootID()
            await expect(
                consensus
                    .connect(sdpOperator1)
                    .verifyLeaf(
                        merkleRoot,
                        proofData,
                        rightProofTrue,
                        leaves[end].dataHash,
                        currentRootID,
                        leaves[end].data.tradeID - 5,
                        leaves[end].sig
                    )
            ).to.be.revertedWith("END_LEAF_INDEX_INVALID")

            //accepts correctly positioned trade

            await expect(
                consensus
                    .connect(sdpOperator1)
                    .verifyLeaf(
                        merkleRoot,
                        proofData,
                        rightProofTrue,
                        leaves[end].dataHash,
                        currentRootID,
                        leaves[end].data.tradeID,
                        leaves[end].sig
                    )
            ).to.not.be.reverted

            // accepts correctly positioned trade under older trade root ID
            let tradeID = 149
            ;[leaves, tradeTree] = fullMerkleTree(tradeKey, 100, 101, chainID, _za)
            let tradeIndex = tradeID - leaves[0].data.tradeID
            ;[proofData, rightProofTrue] = getOrderedProof(leaves, tradeTree, tradeID)
            leafHashes = leaves.map(x => x.hash)
            merkleRoot = tradeTree.getHexRoot()

            await expect(
                consensus.connect(sdpOperator1).verifyLeaf(
                    merkleRoot,
                    proofData,
                    rightProofTrue,
                    leaves[tradeIndex].dataHash,
                    currentRootID.sub(1), // trade is under previous root ID
                    leaves[tradeIndex].data.tradeID,
                    leaves[tradeIndex].sig
                )
            ).to.not.be.reverted
        })
    })
    //-----------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------
    describe("Test Collateral Locking and Unlocking on Root Submission", async () => {
        beforeEach(async () => {
            // load up collateral accounts with protocolToken
            await approveAndDeposit(protocolToken, sdpAdmin1, collateralCustodyMock, stakeAmount)
            await expect(await protocolToken.balanceOf(collateralCustodyMock.address)).to.be.eq(stakeAmount)
            await approveAndDeposit(protocolToken, sdpAdmin2, collateralCustodyMock, stakeAmount - 1)
            await expect(await protocolToken.balanceOf(collateralCustodyMock.address)).to.be.eq(
                stakeAmount * 2 - 1
            )
            await identity.setStakeAmount(stakeAmount)
        })
        it("Allows Staked Operator to Submit Merkle Root", async () => {
            await consensus
                .connect(sdpOperator1)
                .reportTradeRoot(merkleRoot, leafHashes, leaves[end].data, leaves[end].sig)
            expect(
                await collateralCustodyMock.lockedBalances(sdpAdmin1Address, protocolToken.address)
            ).to.be.eq(stakeAmount)
        })
        it("Dissallows Operator with insufficient Stake to Submit Merkle Root", async () => {
            await expect(
                consensus
                    .connect(sdpOperator2)
                    .reportTradeRoot(merkleRoot, leafHashes, leaves[end].data, leaves[end].sig)
            ).to.be.revertedWith("INSUFFICIENT_UNLOCKED_TOKENS")
        })
        it("Unlocks Collateral on Merkle Root Submission regardless of root finalizer", async () => {
            await consensus
                .connect(sdpOperator1)
                .reportTradeRoot(merkleRoot, leafHashes, leaves[end].data, leaves[end].sig)
            expect(
                await collateralCustodyMock.lockedBalances(sdpAdmin1Address, protocolToken.address)
            ).to.be.eq(stakeAmount)
            await hre.network.provider.send("hardhat_mine", [blockInterval])
            await consensus.connect(sdpOperator2).finalizeTradeRoot()
            expect(
                await collateralCustodyMock.lockedBalances(sdpAdmin1Address, protocolToken.address)
            ).to.be.eq(0)
        })
    })
    //-----------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------

    describe("Merkle Root Submission Leaf Array Requirement", async () => {
        it("Merkle Root Submission Fails if a leaf is incorrect", async () => {
            // This is the only test case in this describe block that is not indirectly testing the verifyLeaf functoin
            for (let ii = 0; ii < leaves.length; ii += 13) {
                leafHashes = leaves.map(x => x.hash)
                leafHashes[ii] = merkleRootZero
                await expect(
                    consensus
                        .connect(sdpOperator1)
                        .reportTradeRoot(merkleRoot, leafHashes, leaves[end].data, leaves[end].sig)
                ).to.be.revertedWith("LEAVES_DO_NOT_HASH_TO_TRADE_ROOT")
            }
        })
    })

    //-----------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------

    describe("Merkle Root Submission stores last trade", async () => {
        it("Stores Last Trade ID", async () => {
            await consensus
                .connect(sdpOperator1)
                .reportTradeRoot(merkleRoot, leafHashes, leaves[end].data, leaves[end].sig)
            const merkleState1 = await consensus.callStatic.tradeRoots(1)
            expect(merkleState1.lastTradeID).to.be.eq(leaves[end].data.tradeID)
        })
        it("Stores Multiple Last Trade IDs", async () => {
            await consensus
                .connect(sdpOperator1)
                .reportTradeRoot(merkleRoot, leafHashes, leaves[end].data, leaves[end].sig)
            const merkleState1 = await consensus.callStatic.tradeRoots(1)
            expect(merkleState1.lastTradeID).to.be.eq(leaves[end].data.tradeID)

            await hre.network.provider.send("hardhat_mine", [blockInterval])
            await consensus.connect(sdpOperator1).finalizeTradeRoot()
            ;[leaves, tradeTree] = fullMerkleTree(tradeKey, 100, 101, chainID, _za)
            ;[proofData, rightProofTrue] = getOrderedProof(leaves, tradeTree, 0)
            leafHashes = leaves.map(x => x.hash)
            merkleRoot = tradeTree.getHexRoot()

            await consensus
                .connect(sdpOperator2)
                .reportTradeRoot(merkleRoot, leafHashes, leaves[end].data, leaves[end].sig)
            const merkleState2 = await consensus.callStatic.tradeRoots(2)
            expect(merkleState2.lastTradeID).to.be.eq(leaves[end].data.tradeID)

            await hre.network.provider.send("hardhat_mine", [blockInterval])
            await consensus.connect(sdpOperator1).finalizeTradeRoot()
            ;[leaves, tradeTree] = fullMerkleTree(tradeKey, 100, 201, chainID, _za)
            ;[proofData, rightProofTrue] = getOrderedProof(leaves, tradeTree, 0)
            leafHashes = leaves.map(x => x.hash)
            merkleRoot = tradeTree.getHexRoot()

            await consensus
                .connect(sdpOperator3)
                .reportTradeRoot(merkleRoot, leafHashes, leaves[end].data, leaves[end].sig)
            const merkleState3 = await consensus.callStatic.tradeRoots(3)
            expect(merkleState3.lastTradeID).to.be.eq(leaves[end].data.tradeID)
        })
    })

    //-----------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------

    describe("Requires last trade ID", async () => {
        it("Rejects Root Submission if Last Trade Id Tree Index is too low", async () => {
            leaves = getTradeLeafData(100, 1, chainID, _za)
            // alter ack tradeID
            leaves[leaves.length - 1].data.tradeID += 1
            leaves = hashSignLeaves(tradeKey, leaves)
            tradeTree = merkleFromHashArray(leaves.map(x => x.hash))
            ;[proofData, rightProofTrue] = getOrderedProof(leaves, tradeTree, 0)
            merkleRoot = tradeTree.getHexRoot()

            await expect(
                consensus
                    .connect(sdpOperator1)
                    .reportTradeRoot(merkleRoot, leafHashes, leaves[end].data, leaves[end].sig)
            ).to.be.revertedWith("END_LEAF_INDEX_INVALID")
        })
        it("Rejects Root Submission if Last Trade Id tree Index is too high", async () => {
            leaves = getTradeLeafData(100, 1, chainID, _za)
            // alter ack tradeID
            leaves[leaves.length - 1].data.tradeID -= 1
            leaves = hashSignLeaves(tradeKey, leaves)
            tradeTree = merkleFromHashArray(leaves.map(x => x.hash))
            ;[proofData, rightProofTrue] = getOrderedProof(leaves, tradeTree, 0)
            merkleRoot = tradeTree.getHexRoot()

            await expect(
                consensus
                    .connect(sdpOperator1)
                    .reportTradeRoot(merkleRoot, leafHashes, leaves[end].data, leaves[end].sig)
            ).to.be.revertedWith("END_LEAF_INDEX_INVALID")
        })
        it("Rejects Root Submission With Wrong Last Trade Index after a series of successful reports", async () => {
            await consensus
                .connect(sdpOperator1)
                .reportTradeRoot(merkleRoot, leafHashes, leaves[end].data, leaves[end].sig)

            await hre.network.provider.send("hardhat_mine", [blockInterval])
            await consensus.connect(sdpOperator1).finalizeTradeRoot()
            ;[leaves, tradeTree] = fullMerkleTree(tradeKey, 100, 101, chainID, _za)
            ;[proofData, rightProofTrue] = getOrderedProof(leaves, tradeTree, 0)
            leafHashes = leaves.map(x => x.hash)
            merkleRoot = tradeTree.getHexRoot()

            await consensus
                .connect(sdpOperator2)
                .reportTradeRoot(merkleRoot, leafHashes, leaves[end].data, leaves[end].sig)

            await hre.network.provider.send("hardhat_mine", [blockInterval])
            await consensus.connect(sdpOperator1).finalizeTradeRoot()

            leaves = getTradeLeafData(100, 201, chainID, _za)
            // alter ack tradeID
            leaves[leaves.length - 1].data.tradeID += 1
            leaves = hashSignLeaves(tradeKey, leaves)
            tradeTree = merkleFromHashArray(leaves.map(x => x.hash))
            ;[proofData, rightProofTrue] = getOrderedProof(leaves, tradeTree, 0)
            merkleRoot = tradeTree.getHexRoot()

            await expect(
                consensus
                    .connect(sdpOperator3)
                    .reportTradeRoot(merkleRoot, leafHashes, leaves[end].data, leaves[end].sig)
            ).to.be.revertedWith("END_LEAF_INDEX_INVALID")
        })
    })

    //-----------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------

    describe("Tests checks on verifyTradeRoot() root submission", async () => {
        it("Does not accept root with ACK not signed by exchange", async () => {
            let fakePrivate = ethers.utils.solidityKeccak256(
                ["address", "address"],
                [sdpAdmin1Address, sdpAdmin1Address]
            )
            let fakeKey = new ethers.utils.SigningKey(fakePrivate)

            ;[leaves, tradeTree] = fullMerkleTree(fakeKey, 100, 101, chainID, _za)
            ;[proofData, rightProofTrue] = getOrderedProof(leaves, tradeTree, 0)
            leafHashes = leaves.map(x => x.hash)
            merkleRoot = tradeTree.getHexRoot()

            await expect(
                consensus
                    .connect(sdpOperator1)
                    .reportTradeRoot(merkleRoot, leafHashes, leaves[end].data, leaves[end].sig)
            ).to.be.revertedWith("INVALID_EXCHANGE_SIGNATURE")
        })

        it("Does not accept root if ACK does not hash to equal the last leafHash in the array", async () => {
            // alter the ack by changing the token ID to the wrong ID
            leaves[end].data.tokenID = sdpAdmin1Address
            leaves[end].sig = tradeKey.signDigest(hashACK(leaves[end].data))

            await expect(
                consensus
                    .connect(sdpOperator1)
                    .reportTradeRoot(merkleRoot, leafHashes, leaves[end].data, leaves[end].sig)
            ).to.be.revertedWith("ACK_DOES_NOT_MATCH_LEAF")
        })
    })

    //-----------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------

    describe("Checks Chain ID in Settlement ACK", async () => {
        it("Contract Rejects Settlement ACK with Incorrect Chain ID", async () => {
            leaves[end].data.chainID = 0
            await expect(
                consensus
                    .connect(sdpOperator1)
                    .reportTradeRoot(merkleRoot, leafHashes, leaves[end].data, leaves[end].sig)
            ).to.be.revertedWith("INCORRECT_CHAIN_ID")
        })
    })
})
