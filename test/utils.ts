// Copyright © 2022 TXA PTE. LTD.
import {
  ethers,
  deployments,
  getNamedAccounts,
  getUnnamedAccounts,
  artifacts,
} from "hardhat"

import { MerkleTree } from "merkletreejs"
import { Signer, BigNumberish, Contract, BigNumber } from "ethers"
import { Address } from "hardhat-deploy/dist/types"
import { IdentityRegistry as _IdentityRegistry } from "../typechain/IdentityRegistry"
import { SettlementLib as _ExchangeLib } from "../typechain/SettlementLib"
import { ExchangeEIP712 as _ExchangeEIP712 } from "../typechain/ExchangeEIP712"
import { SettlementCoordination as _SettlementCoordination } from "../typechain/SettlementCoordination"
import { CoordinationMock as _CoordinationMock } from "../typechain/CoordinationMock"
import { ConsensusMock as _ConsensusMock } from "../typechain/ConsensusMock"
import { SettlementDataConsensus as _SettlementDataConsensus } from "../typechain/SettlementDataConsensus"
import { CollateralMock as _CollateralMock } from "../typechain/CollateralMock"
import { Artifact, Libraries } from "hardhat/types"
import { DummyCoin as _DummyCoin } from "../typechain/DummyCoin"
import { DummyCoin777 as _DummyCoin777 } from "../typechain/DummyCoin777"
import { ProtocolToken as _ProtocolToken } from "../typechain/ProtocolToken"
import { AssetCustody as _AssetCustody } from "../typechain/AssetCustody"
import { CollateralCustody as _CollateralCustody } from "../typechain/CollateralCustody"
import { AssetMock as _AssetMock } from "../typechain/AssetMock"
import { SettlementUtilities as _SettlementUtilities } from "../typechain/SettlementUtilities"
import { UtilsMock as _UtilsMock } from "../typechain/UtilsMock"

import { IntegerType } from "typechain"

export const _za = "0x0000000000000000000000000000000000000000"

export type SettlementLib = _ExchangeLib
export type ExchangeEIP712 = _ExchangeEIP712
export type SettlementCoordination = _SettlementCoordination
export type CoordinationMock = _CoordinationMock
export type ConsensusMock = _ConsensusMock
export type CollateralMock = _CollateralMock
export type SettlementDataConsensus = _SettlementDataConsensus
export type IdentityRegistry = _IdentityRegistry
export type DummyCoin = _DummyCoin
export type DummyCoin777 = _DummyCoin777
export type ProtocolToken = _ProtocolToken
export type AssetCustody = _AssetCustody
export type CollateralCustody = _CollateralCustody
export type AssetMock = _AssetMock
export type SettlementUtilities = _SettlementUtilities
export type UtilsMock = _UtilsMock

export interface Contracts {
  library: SettlementLib
  identity: IdentityRegistry
  coordination: SettlementCoordination
  coordinationMock: CoordinationMock
  consensusMock: ConsensusMock
  collateralMock: CollateralMock
  dummyCoin: DummyCoin
  dummyCoin777: DummyCoin777
  protocolToken: ProtocolToken
  consensus: SettlementDataConsensus
  assetCustody: AssetCustody
  collateralCustody: CollateralCustody
  assetMock: AssetMock
  settlementUtilities: SettlementUtilities
  utilsMock: UtilsMock
}

export const getSigner = async (address: string) => {
  return ethers.provider.getSigner(address)
}

export const getDeployed = async (contractName: string, signer?: Signer) => {
  const contract = await deployments.get(contractName)
  return await ethers.getContractAt(contract.abi, contract.address, signer)
}

export const getNamedSigners = async () => {
  let namedAccounts = await getNamedAccounts()
  let sdps: Signer[] = []
  let sdpAddrs = [
    namedAccounts.sdpAdmin1,
    namedAccounts.sdpAdmin2,
    namedAccounts.sdpAdmin3,
    namedAccounts.sdpAdmin4,
  ]
  for (let i = 0; i < sdpAddrs.length; i++) {
    sdps[i] = ethers.provider.getSigner(sdpAddrs[i])
  }
  return {
    exchange: ethers.provider.getSigner(namedAccounts.exchange),
    alice: ethers.provider.getSigner(namedAccounts.alice),
    bob: ethers.provider.getSigner(namedAccounts.bob),
    sdps: sdps,
  }
}

/**
 *  full data for leaf in merkle trees.
 *  @param data data in merkle leaf, can be trade or ack
 *  @param hash the full hash of the signed leaf
 *  @param sig exchange signature
 *  @param dataHash the hashed data that is to be signed by the exchange
 */
export type Leaf = {
  data: any
  hash: string
  sig: Signature
  dataHash: string
}

// Settlement Acknowledgement
export type ACK = {
  tradeID: number
  settlementID: number
  chainID: number
  tokenID: Address
  // coordAddress: Address; leave out for
}

export const ackAbi = [
  "tuple(uint256 tradeID,uint256 settlementID,uint256 chainID,address tokenID)",
]

export type DisputeProfile = {
  submitter: Address
}

export type ChallengeData = {
  challengeRoot: string
  challengeLeaf: string
  challengeSig: Signature
  reportedHash: string
}

// Trade
export type Trade = {
  tradeID: number
}

export const tradeAbi = ["tuple(uint256 tradeID)"]

// Ethereum Signature
export type Signature = {
  v: number
  r: string
  s: string
}

export type NamedSigners = {
  exchange: Signer
  alice: Signer
  bob: Signer
  sdps: Signer[]
}

export type Obligation = {
  amount: BigNumberish
  deliverer: string
  recipient: string
  token: string
  reallocate: boolean
}

export type CollateralTransfer = {
  party: Address
  amount: BigNumberish
}

export type TransferObligation = {
  sender: Address
  recipient: Address
  amount: BigNumber
}

export type ExchangeReport = {
  merkleRoot: string
  settlementId: BigNumber
  coordinator: Address
}

export type rootSubmission = {
  merkleRoot: String
  submitter: Address
}

export const getTraders = async (amount: number) => {
  const NAMED_OFFSET = 12
  let accounts: string[] = await getUnnamedAccounts()
  let traders: Signer[] = []
  for (let i = 0; i < amount; i++) {
    traders[i] = await ethers.provider.getSigner(accounts[i + NAMED_OFFSET])
  }

  return traders
}

export const deploy = async (
  contractName: string,
  args?: Array<any>,
  signer?: Signer
) => {
  return await (
    await ethers.getContractFactory(contractName, signer)
  ).deploy(...(args ?? []))
}

export const deployWithLibraries = async (
  contractName: string,
  libraries: Libraries,
  args?: Array<any>,
  signer?: Signer
) => {
  const cArtifact = await artifacts.readArtifact(contractName)
  const linkedBytecode = linkBytecode(cArtifact, libraries)
  const Contract = await ethers.getContractFactory(
    cArtifact.abi,
    linkedBytecode,
    signer
  )
  return await (
    await ethers.getContractFactory(cArtifact.abi, Contract.bytecode, signer)
  ).deploy(...(args ?? []))
}

export const setupTest = deployments.createFixture(
  async ({ deployments }, options) => {
    await deployments.fixture(["Test", "Tokens"])
    return {
      library: (await getDeployed("SettlementLib")) as SettlementLib,
      identity: (await getDeployed("IdentityRegistry")) as IdentityRegistry,
      coordination: (await getDeployed(
        "SettlementCoordination"
      )) as SettlementCoordination,
      coordinationMock: (await getDeployed(
        "CoordinationMock"
      )) as CoordinationMock,
      consensusMock: (await getDeployed("ConsensusMock")) as ConsensusMock,
      collateralMock: (await getDeployed("CollateralMock")) as CollateralMock,
      dummyCoin: (await getDeployed("DummyCoin")) as DummyCoin,
      dummyCoin777: (await getDeployed("DummyCoin777")) as DummyCoin777,
      protocolToken: (await getDeployed("ProtocolToken")) as ProtocolToken,
      consensus: (await getDeployed(
        "SettlementDataConsensus"
      )) as SettlementDataConsensus,
      assetCustody: (await getDeployed("AssetCustody")) as AssetCustody,
      collateralCustody: (await getDeployed(
        "CollateralCustody"
      )) as CollateralCustody,
      assetMock: (await getDeployed("AssetMock")) as AssetMock,
      settlementUtilities: (await getDeployed(
        "SettlementUtilities"
      )) as SettlementUtilities,
      utilsMock: (await getDeployed("UtilsMock")) as UtilsMock,
    }
  }
)

export function linkBytecode(artifact: Artifact, libraries: Libraries) {
  let bytecode = artifact.bytecode

  for (const [fileName, fileReferences] of Object.entries(
    artifact.linkReferences
  )) {
    for (const [libName, fixups] of Object.entries(fileReferences)) {
      const addr = libraries[libName]
      if (addr === undefined) {
        continue
      }

      for (const fixup of fixups) {
        bytecode =
          bytecode.substr(0, 2 + fixup.start * 2) +
          addr.substr(2) +
          bytecode.substr(2 + (fixup.start + fixup.length) * 2)
      }
    }
  }

  return bytecode
}

export const copyArray = (arr: Obligation[]): Obligation[] =>
  JSON.parse(JSON.stringify(arr))

/**
 * Helper Functions for tests to improve implementability and readability
 */

/**
 * Shorter implementation for depositing erc tokens
 * Works for all Asset Custody, Collateral Custody and related Mock Contracts
 *
 * @param token token contract
 * @param signer the user who is depositing tokens to their collateral or asset custody account
 * @param custody Asset Custody, Collateral Custody, or Mock contract for submitting deposit to
 * @param deposit  Amount of tokens to be deposited
 * @param collateralizeAfter Flag for collateralizing upon deposit
 */
export async function approveAndDeposit(
  token: Contract,
  signer: Signer,
  custody: Contract,
  deposit: number,
  collateralizeAfter: boolean = false
) {
  await token.connect(signer).approve(custody.address, deposit)
  // Determine if contract is AssetCustody or CollateralCustody
  if (custody.uncollateralized !== undefined) {
    await custody
      .connect(signer)
      .depositToken(deposit, token.address, collateralizeAfter)
  } else {
    await custody.connect(signer).depositToken(deposit, token.address)
  }
}
/**
 * Function to simplify assigning an SDP Operator to an SDP Admin account for tests
 * That need this to be dont but are not testing this functionality specifically
 * @param admin sdp admin, who controls the collateral
 * @param operator sdp operator who will be able to submit obligation reports using admin's collateral
 * @param contract Collateral Custody Contract where this permission assignment is submitted to
 */

export async function operatorInit(
  admin: Signer,
  operator: Signer,
  contract: Contract
) {
  let sdpAdmin1AddressBytes = ethers.utils.arrayify(await admin.getAddress())
  let signedMessage = await operator.signMessage(sdpAdmin1AddressBytes)
  let signature = signedMessage.substring(2)
  let r = "0x" + signature.substring(0, 64)
  let s = "0x" + signature.substring(64, 128)
  let v = parseInt(signature.substring(128, 130), 16)
  let sig: Signature = { v, r, s }
  await contract
    .connect(admin)
    .approveOperator(await operator.getAddress(), sig)
}

/**
 * Accepts the components of a Settlement ACK and hashes them together
 */
export function hashACK(ack: ACK) {
  const hashedACK = ethers.utils.solidityKeccak256(
    ["uint256", "uint256", "uint256", "address"],
    [ack.tradeID, ack.settlementID, ack.chainID, ack.tokenID]
  )
  return hashedACK
}

export function hashTrade(trade: Trade) {
  return ethers.utils.solidityKeccak256(["uint256"], [trade.tradeID])
}
/**
 * Gets a merkle root for an array of obligations needed for obligation reports
 * @param obligations array of obligations
 * @returns returns a merkle root
 */
export async function merkleFromObs(obligations: Obligation[]) {
  const calculateLeafFromObligation = (obligation: Obligation) =>
    ethers.utils.solidityKeccak256(
      ["uint256", "address", "address", "address", "bool"],
      [
        obligation.amount,
        obligation.deliverer,
        obligation.recipient,
        obligation.token,
        obligation.reallocate,
      ]
    )
  let merkleTree = new MerkleTree(
    obligations.map(calculateLeafFromObligation),
    ethers.utils.keccak256,
    { hashLeaves: false, sortPairs: true }
  )
  return merkleTree.getHexRoot()
}

// Outputs an array with an incremental range of numbers
export function range(start: number, end: number) {
  return Array.from(Array(end - start + 1).keys()).map(x => x + start)
}

/**
 * Returns a merkle Tree from an array of numbers. Fills all trees to be
 * balanced trees with null nodes in the unused leaves. Leaves and nodes
 * are not auto-sorted.
 *
 * The tree has a canonical order in the order of the entries in the input
 * array.
 */
export function merkleFromArray(array: number[]) {
  let arrayHash: string[] = new Array(array.length)
  for (let ii = 0; ii < array.length; ii++) {
    arrayHash[ii] = ethers.utils.solidityKeccak256(["uint256"], [array[ii]])
  }

  let merkleTree = new MerkleTree(arrayHash, ethers.utils.keccak256, {
    hashLeaves: false,
    sortPairs: false,
    sortLeaves: false,
    duplicateOdd: false,
    fillDefaultHash:
      "0x0000000000000000000000000000000000000000000000000000000000000000",
  })
  return merkleTree
}

/**
 * Returns a merkle Tree from an array of hashes. Fills all trees to be
 * balanced trees with null nodes in the unused leaves. Leaves and nodes
 * are not auto-sorted.
 *
 * The tree has a canonical order in the order of the entries in the input
 * array.
 */
export function merkleFromHashArray(array: string[]) {
  let merkleTree = new MerkleTree(array, ethers.utils.keccak256, {
    hashLeaves: false,
    sortPairs: false,
    sortLeaves: false,
    duplicateOdd: false,
    fillDefaultHash:
      "0x0000000000000000000000000000000000000000000000000000000000000000",
  })
  return merkleTree
}

/**
 *
 * @param tradeKey key used by exchange to sign trades
 * @param size amount of trades in tree including ack
 * @param firstTrade trade id of first trade in tree
 * @param proof trade ID of trade to have merkle proof presented
 * @param chainID chainID to be included in trade leaves
 * @returns array of leaves, merkle root, merkle proof for selected leaf
 */
export function fullMerkleTree(
  tradeKey: any,
  size: number,
  firstTrade: number,
  chainID: number,
  tokenID: Address
) {
  let leaves: Leaf[]

  leaves = getTradeLeafData(size, firstTrade, chainID, tokenID)
  leaves = hashSignLeaves(tradeKey, leaves)

  let tradeTree = merkleFromHashArray(leaves.map(x => x.hash))
  let merkleRoot = tradeTree.getHexRoot
  return [leaves, tradeTree] as const
}

/**
 * Function to get the trade and ack data for a trade array
 * @param size size of trade array
 * @param firstTrade tradeID of first trade
 * @param chainID chain ID
 * @returns leaves array with ack data and trade data. other fields empty
 */
export function getTradeLeafData(
  size: number,
  firstTrade: number,
  chainID: number,
  tokenID: Address
) {
  let leaves: Leaf[]
  leaves = new Array<Leaf>(size)

  let ack: ACK
  ack = {
    tradeID: firstTrade + size - 1,
    settlementID: 0,
    chainID: chainID,
    tokenID: tokenID,
  }
  // initialize leaves array. default data set to ack. trades will be filled out, ack left at end of array.
  for (let ii = 0; ii < leaves.length; ii++) {
    leaves[ii] = {
      data: ack,
      hash: "",
      sig: { v: 0, r: "", s: "" },
      dataHash: "",
    }
  }

  for (let ii = 0; ii < size - 1; ii++) {
    let trade: Trade
    trade = { tradeID: firstTrade + ii }
    leaves[ii].data = { tradeID: firstTrade + ii }
  }
  return leaves
}

/**
 *
 * @param tradeKey exchange trade signing tree
 * @param leaves leaf array. Comes in with just data, leaves with hash, dataHash and signature
 * @returns fully hashed and signed leaf array
 */
export function hashSignLeaves(tradeKey: any, leaves: Leaf[]) {
  // hash ack
  leaves[leaves.length - 1].dataHash = hashACK(leaves[leaves.length - 1].data)
  // hash trades
  for (let ii = 0; ii < leaves.length - 1; ii++) {
    leaves[ii].dataHash = ethers.utils.solidityKeccak256(
      ["uint256"],
      [leaves[ii].data.tradeID]
    )
  }
  //sign and hash all leaves
  for (let ii = 0; ii < leaves.length; ii++) {
    let sig = tradeKey.signDigest(leaves[ii].dataHash)
    leaves[ii].sig = sig
    leaves[ii].hash = ethers.utils.solidityKeccak256(
      ["uint8", "bytes32", "bytes32", "bytes32"],
      [sig.v, sig.r, sig.s, leaves[ii].dataHash]
    )
  }
  return leaves
}

/**
 * Function to request an ordered Merkle Proof in the format required by the contracts
 * @param leaves array of leaves
 * @param tradeTree merkle tree
 * @param proofRequest trade ID who's proof is being requested. Set to 0 for ACK
 */
export function getOrderedProof(
  leaves: Leaf[],
  tradeTree: MerkleTree,
  proofRequest: number
) {
  let firstTrade = leaves[0].data.tradeID
  let size = leaves.length
  let index: number
  if (proofRequest < firstTrade || proofRequest >= firstTrade + size - 1) {
    index = leaves.length - 1
  } else {
    index = proofRequest - firstTrade
  }
  let leaf = leaves[index].hash
  let proof = tradeTree.getProof(leaf)
  let proofData = tradeTree.getHexProof(leaf)
  let rightProofTrue = new Array(proof.length)
  for (let kk = 0; kk < proof.length; kk++) {
    rightProofTrue[kk] = proof[kk].position == "right"
  }
  return [proofData, rightProofTrue] as const
}
