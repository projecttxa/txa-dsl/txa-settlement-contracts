#!/bin/bash

# Replace chain id in genesis block with env var
export CHAIN_ID=${CHAIN_ID}
cd /cliquebait
J=".config.chainId = $CHAIN_ID"
contents=$(jq --arg CHAIN_ID $CHAIN_ID '.config.chainId |= ($CHAIN_ID | tonumber)' cliquebait.json)
echo -E "${contents}" > cliquebait.json

# Run cliquebait
./run.bash &

cd /app
# Call initialize on predeployed contracts to set storage
npx hardhat run --network cliquebait scripts/initialize_predeployed.ts

# Let this container stay running
sleep infinity
