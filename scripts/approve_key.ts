import { ethers, getNamedAccounts } from "hardhat"
import { IdentityRegistry } from "../typechain/IdentityRegistry"

export async function approveKey() {
  const chainId = (await ethers.provider.getNetwork()).chainId
  const { alice, bob } = await getNamedAccounts()
  const identity = (await ethers.getContractAt(
    "IdentityRegistryPreDeploy",
    "0x18e5b9d018cAb60fC73A79D560EA61dd856a808d"
  )) as IdentityRegistry
  for (const address of [alice, bob]) {
    const primaryKey = ethers.utils.solidityKeccak256(
      ["address", "address"],
      [address, address]
    )
    const primarySigner = new ethers.utils.SigningKey(primaryKey)
    const signPermission = true
    const digest = ethers.utils.solidityKeccak256(
      ["address", "uint256", "bool"],
      [address, chainId, signPermission]
    )
    const sig = primarySigner.signDigest(digest)
    await (
      await identity
        .connect(await ethers.getSigner(address))
        .approveSignature(sig.v, sig.r, sig.s, true, primarySigner.publicKey)
    ).wait()
    console.log("Approved a primary key for: " + address)
    console.log("           With public key: " + primarySigner.publicKey)
  }
}

approveKey()
  .then(() => process.exit(0))
  .catch(error => {
    console.error(error)
    process.exit(1)
  })
