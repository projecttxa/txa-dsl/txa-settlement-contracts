import { ethers, getNamedAccounts, deployments } from "hardhat"
import { AssetCustody } from "../typechain/AssetCustody"
import { DummyCoin } from "../typechain/DummyCoin"

/**
 * Requests DummyCoin owed by Bob on behalf of Alice
 * and native asset owed by Alice on behalf of Bob
 * by sending transactions to the predeployed AssetCustody
 * contract.
 *
 * MUST run report_obligations.ts before this script
 *
 * Expects SDPs to have reported for Alice and Bob's settlements.
 */
export async function requestFundsOwed() {
  const { alice, bob } = await getNamedAccounts()
  const dummyCoin = (await ethers.getContractAt(
    "DummyCoin",
    "0xa85233C63b9Ee964Add6F2cffe00Fd84eb32338f"
  )) as DummyCoin
  const assetCustody = (await ethers.getContractAt(
    "AssetCustody",
    "0x8A3cB37a7aC1acEd79034E93D409906eACb2Da6b"
  )) as AssetCustody
  console.log("alice: " + alice)
  console.log("bob: " + bob)

  await assetCustody
    .connect(await ethers.getSigner(alice))
    .callStatic.requestFundsOwed([bob], dummyCoin.address, false, false)
  await assetCustody
    .connect(await ethers.getSigner(alice))
    .requestFundsOwed([bob], dummyCoin.address, false, false)
  console.log("Alice requestd DMC owed by Bob")

  await assetCustody
    .connect(await ethers.getSigner(bob))
    .callStatic.requestFundsOwed(
      [alice],
      ethers.constants.AddressZero,
      false,
      false
    )
  await assetCustody
    .connect(await ethers.getSigner(bob))
    .requestFundsOwed([alice], ethers.constants.AddressZero, false, false)
  console.log("Bob requested native asset owed by Alice")
}

requestFundsOwed()
  .then(() => process.exit(0))
  .catch(error => {
    console.error(error)
    process.exit(1)
  })
