import { ethers, getNamedAccounts, deployments } from "hardhat"
import { AssetCustody } from "../typechain/AssetCustody"
import { DummyCoin } from "../typechain/DummyCoin"

/**
 * Requests settlement of DummyCoin on behalf of Alice
 * and settlement of the native asset on behalf of Bob
 * by sending transactions to the predeployed AssetCustody
 * contract.
 *
 * MUST run deposit_eth.ts before this script
 *
 * This script is used to run integration tests with
 * the SDP. It expects Alice to have deposited and
 * trade the native asset with Bob in exchange for
 * DummyCoin.
 */
export async function requestSettlement() {
  const { alice, bob } = await getNamedAccounts()
  const dummyCoin = (await ethers.getContractAt(
    "DummyCoin",
    "0xa85233C63b9Ee964Add6F2cffe00Fd84eb32338f"
  )) as DummyCoin
  const assetCustody = (await ethers.getContractAt(
    "AssetCustody",
    "0x8A3cB37a7aC1acEd79034E93D409906eACb2Da6b"
  )) as AssetCustody
  console.log("alice: " + alice)
  console.log("bob: " + bob)

  for (const [address, token] of [
    [alice, ethers.constants.AddressZero],
    [bob, ethers.constants.AddressZero],
  ]) {
    await assetCustody
      .connect(await ethers.getSigner(address))
      .callStatic.requestSettlement(token)
    await (
      await assetCustody
        .connect(await ethers.getSigner(address))
        .requestSettlement(token)
    ).wait()
  }
  console.log("Alice requested settlement for DMC " + dummyCoin.address)
  console.log(
    "Bob requested settlement for native asset " + ethers.constants.AddressZero
  )
}

requestSettlement()
  .then(() => process.exit(0))
  .catch(error => {
    console.error(error)
    process.exit(1)
  })
